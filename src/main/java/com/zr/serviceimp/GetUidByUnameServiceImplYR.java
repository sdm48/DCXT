package com.zr.serviceimp;

import com.zr.daoimp.LoginDaoImplYR;
import com.zr.service.GetUidByUnameServiceYR;

/**
 * 通过Uname获取uid的服务实现
 * @author YR
 *
 */
public class GetUidByUnameServiceImplYR implements GetUidByUnameServiceYR {
    //新建一个LoginDaoImplYR对象
	LoginDaoImplYR loginDaoImplYR=new LoginDaoImplYR();
	@Override
	public int getUidByUname(String uname) {
		// 调用LoginDaoImplYR的方法
		return loginDaoImplYR.getUidByUname(uname);
	}

}

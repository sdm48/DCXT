package com.zr.serviceimp;

import com.zr.daoimp.LoginDaoImplYR;
import com.zr.service.SelallcusServiceYR;

/**
 * 查询顾客总数的服务实现
 * @author YR
 *
 */
public class SelallcusServiceImplYR implements SelallcusServiceYR {
	//新建一个LoginDaoImplYR对象
	LoginDaoImplYR loginDaoImplYR =new LoginDaoImplYR();
	@Override
	public int selallcus() {
		// 调用loginDaoImplYR中的查找顾客总数方法
		return loginDaoImplYR.selallcus();
	}

}

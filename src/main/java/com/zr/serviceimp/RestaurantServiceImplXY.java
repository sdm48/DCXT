package com.zr.serviceimp;

import java.util.List;

import com.zr.dao.RestaurantDaoXY;
import com.zr.daoimp.RestaurantDaoImplXY;
import com.zr.model.RestaurantXY;
import com.zr.service.RestaurantServiceXY;

public class RestaurantServiceImplXY implements RestaurantServiceXY{
	RestaurantDaoXY rdao = new RestaurantDaoImplXY();
	
	@Override
	public RestaurantXY searchRestaurantByRid(int rid) {
		// TODO Auto-generated method stub
		return rdao.findRestaurantByRid(rid);
	}

	@Override
	public int removeRestaurantByRid(int rid) {
		// TODO Auto-generated method stub
		return rdao.deleteRestaurantByRid(rid);
	}

	@Override
	public List<RestaurantXY> selectRestsJinyong(int page, int rows) {
		// TODO Auto-generated method stub
		return rdao.selectRestaurantJinyong(page, rows);
	}

	@Override
	public int countRests() {
		// TODO Auto-generated method stub
		return rdao.countRests();
	}

	@Override
	public void jiefengRest(int rid) {
		// TODO Auto-generated method stub
		rdao.jiefengRest(rid);
	}

	
	
	

}

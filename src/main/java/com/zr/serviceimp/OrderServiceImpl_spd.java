package com.zr.serviceimp;

import java.util.List;

import com.zr.dao.OrderDao_spd;
import com.zr.daoimp.OrderDaoImpl_spd;
import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;
import com.zr.service.OrderService_spd;

public class OrderServiceImpl_spd implements OrderService_spd{
	OrderDao_spd od = new OrderDaoImpl_spd();
	@Override
	public List<Order_spd> getAllOrdersByUid(int uid) {
		return od.getAllOrdersByUid(uid);
	}
	@Override
	public Restaurant_spd getRestaurantByOid(int oid) {
		return od.getRestaurantByOid(oid);
	}
	@Override
	public boolean addJudgeTextAndTime(String judgeText, String addTime, int judgeNum, int oid) {
		return od.addJudgeTextAndTime(judgeText, addTime, judgeNum, oid);
	}
}

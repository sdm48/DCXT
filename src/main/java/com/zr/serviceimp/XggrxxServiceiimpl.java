package com.zr.serviceimp;

import com.zr.dao.XggrxxDao;
import com.zr.daoimp.XggrexxDaoimpl;
import com.zr.service.XggrxxService;

public class XggrxxServiceiimpl implements XggrxxService {
	XggrxxDao xd=new XggrexxDaoimpl();
	@Override
	public boolean changeNameByid(int uid, String cname) {
		// TODO Auto-generated method stub
		if(cname.equals("")){
			return false;
		}else{
			return xd.changeNameByid(cname, uid);
		}	
	}

	@Override
	public boolean changeCaddressByid(int uid, String Caddress) {
		// TODO Auto-generated method stub
		if(Caddress.equals("")){
			return false;
		}else{
			return xd.changeCaddressByid(uid, Caddress);
		}
	}

	@Override
	public boolean changeCphoneByid(int uid, String Cphone) {
		if(Cphone.equals("")||Cphone.equals("0")){
			return false;
		}else{
			return xd.changeCphoneByid(uid, Cphone);
		}
	}


	@Override
	public boolean changeCsexByid(int uid, String Csex) {
		// TODO Auto-generated method stub
		if(Csex.equals("")){
			return false;
		}else{
			if(Csex.equals("男")||Csex.equals("女")){return xd.changeCsexByid(uid, Csex);}
			return false;
		}
	}

	@Override
	public boolean changeUpasswordByid(int uid, String Upassword) {
		// TODO Auto-generated method stub
		if(Upassword.equals("")){
			return false;
		}else{
			return xd.changeUpasswordByid(uid, Upassword);
		}
	}

	@Override
	public boolean isnotSameName(int uid, String cname) {
		// TODO Auto-generated method stub
		return xd.isnotSameName(uid, cname);
	}

}

package com.zr.serviceimp;

import java.sql.Date;
import java.util.List;

import com.zr.dao.RestaurantDao_spd;
import com.zr.daoimp.RestaurantDaoImpl_spd;
import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;
import com.zr.service.RestaurantService_spd;

public class RestaurantServiceImpl_spd implements RestaurantService_spd{
	RestaurantDao_spd rd = new RestaurantDaoImpl_spd();
	@Override
	public List<Restaurant_spd> getAllRestaurantMessage() {
		return rd.getAllRestaurantMessage();
	}
	@Override
	public Restaurant_spd getRestaurantByRid(int rid) {
		return rd.getRestaurantByRid(rid);
	}
	@Override
	public List<Order_spd> getOrdersByUid(int uid) {
		return rd.getOrdersByUid(uid);
	}
	@Override
	public boolean collectRestaurant(int uid, int rid, String addTime) {
		return rd.collectRestaurant(uid, rid, addTime);
	}
	@Override
	public int getRestaurantCount() {
		return rd.getRestaurantCount();
	}
}

package com.zr.serviceimp;

import java.util.List;

import com.zr.daoimp.RestaurantDaoImplYR;
import com.zr.model.RestaurantYR;
import com.zr.service.SelresServiceYR;

/**
 * 查找餐馆的服务实现
 * @author YR
 *
 */
public class SelresServiceImplYR implements SelresServiceYR{

	//新建一个restaurantimplYR对象
	RestaurantDaoImplYR restaurantDaoImplYR=new RestaurantDaoImplYR();
	@Override
	public List<RestaurantYR> selresByPage(int page, int rows) {
		// 调用restaurantDaoImplYR的分页查询餐馆的方法
		return restaurantDaoImplYR.selresByPage(page, rows);
	}

	@Override
	public int selallrestaurant() {
		//  调用restaurantDaoImplYR的查询餐馆总数的方法
		return restaurantDaoImplYR.selallres();
	}

}

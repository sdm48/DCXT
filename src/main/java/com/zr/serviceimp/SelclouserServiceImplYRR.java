package com.zr.serviceimp;

import java.util.List;

import com.zr.daoimp.ClosedDaoImplYR;
import com.zr.daoimp.LoginDaoImplYR;
import com.zr.model.ClosedYR;
import com.zr.model.User;
import com.zr.service.SelclouserServiceYR;
/**
 * 查找封号对象的服务实现
 * @author YR
 *
 */
public class SelclouserServiceImplYRR implements SelclouserServiceYR{
    //新建一个LoginDaoImplYR对象
	LoginDaoImplYR loginDaoImplYR=new LoginDaoImplYR();
	//新建一个ClosedDaoImplYR对象
	ClosedDaoImplYR closedDaoImplYR=new ClosedDaoImplYR();
	@Override
	public User selclouserByUid(int uid) {
		//调用loginDaoImplYR的查找要封账户方法
		return loginDaoImplYR.selclouserByUid(uid);
	}
	@Override
	public List<ClosedYR> selcloseduserByPage(int page, int rows) {
		//调用closedDaoImplYR的根据页码查找已封账户记录的方法
		return closedDaoImplYR.selclouserByPage(page, rows);
	}
	@Override
	public int selallclouser() {
		//调用closedDaoImplYR的查找已封账户总数的方法
		return closedDaoImplYR.selallclouser();
	}

}

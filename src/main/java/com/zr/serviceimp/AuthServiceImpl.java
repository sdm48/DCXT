package com.zr.serviceimp;

import java.util.List;

import com.zr.dao.AuthDao;
import com.zr.model.Auth;
import com.zr.service.AuthService;
import com.zr.util.JDBCUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AuthServiceImpl implements AuthService{
	AuthDao  adao = new com.zr.daoimp.AuthDaoImpl();
	@Override
	public JSONArray getAuths(int aid,String uname) {
		// TODO Auto-generated method stub
		JSONArray  as = adao.getAuthsByPid(aid,uname);
		for (int i = 0; i < as.size(); i++) {
			JSONObject  j = as.getJSONObject(i);
			if(j.get("state").equals("1")){
				continue;
			}else{
				JSONArray  childrens = this.getAuths(j.getInt("id"),uname);
				j.put("children", childrens);
			}
			
		}
		
		
		return as;
	}


}
package com.zr.serviceimp;

import com.zr.daoimp.LoginDaoImplYR;
import com.zr.service.SelallbossServiceYR;

/**
 * 查询老板总数的服务实现
 * @author YR
 *
 */
public class SelallbossServiceImplYR implements SelallbossServiceYR{
	
	//新建一个LoginDaoImplYR对象
	    LoginDaoImplYR loginDaoImplYR =new LoginDaoImplYR();

		@Override
		public int selallboss() {
			// 调用loginDaoImplYR中的查找老板总数方法
			return loginDaoImplYR.selallboss();
		}
	

}

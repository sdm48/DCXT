package com.zr.serviceimp;

import java.util.ArrayList;
import java.util.List;

import com.zr.dao.DishDao_PL;
import com.zr.daoimp.DishDaoPLImpl_PL;
import com.zr.model.Dish_PL;
import com.zr.service.DishService_PL;
/**
 * 菜的服务实现
 * @author 彭浪
 *
 */
public class DishServiceImpl_PL implements DishService_PL{
	DishDao_PL ddao=new DishDaoPLImpl_PL();
	@Override
	public List<Dish_PL> getDishPLByDid(String did) {
		List<Dish_PL> list=new ArrayList<Dish_PL>();
		String[] dids=did.split(",");
		for (int i = 0; i < dids.length; i++) {
			list.add(ddao.selectOne("did",dids[i] ));
		}
		return list;
	}
	@Override
	public boolean modifyDishPL(Dish_PL d) {
		return ddao.updateDishPL(d);
	}
	
}

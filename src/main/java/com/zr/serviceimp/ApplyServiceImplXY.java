package com.zr.serviceimp;

import java.util.List;

import com.zr.dao.ApplyDaoXY;
import com.zr.daoimp.ApplyDaoImplXY;
import com.zr.model.ApplyXY;
import com.zr.service.ApplyServiceXY;

public class ApplyServiceImplXY implements ApplyServiceXY {
	ApplyDaoXY adao = new ApplyDaoImplXY();
	
	@Override
	public List<ApplyXY> selectApply(int page, int rows) {
		// TODO Auto-generated method stub
		return adao.selectApply(page, rows);
	}

	@Override
	public int countApplys() {
		// TODO Auto-generated method stub
		return adao.countApplys();
	}

	@Override
	public void passApply(int aid,String now) {
		// TODO Auto-generated method stub
		adao.passApply(aid, now);
	}

	@Override
	public void notpassApply(int aid) {
		// TODO Auto-generated method stub
		adao.notpassApply(aid);
	}

}

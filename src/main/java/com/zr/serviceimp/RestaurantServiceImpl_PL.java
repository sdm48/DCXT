package com.zr.serviceimp;

import com.zr.dao.RestaurantDao_PL;
import com.zr.daoimp.RestaurantDaoImpl_PL;
import com.zr.model.Restaurant_PL;
import com.zr.service.RestaurantService_PL;
import com.zr.util.IsBlankUtil;
import com.zr.util.ThisSystemExceptionUtil;
/**
 * 餐馆的服务实现
 * @author 彭浪
 *
 */
public class RestaurantServiceImpl_PL implements RestaurantService_PL{
	RestaurantDao_PL rdao=new RestaurantDaoImpl_PL();
	@Override
	public Restaurant_PL getRestaurantPLByUid(int uid) {
		return rdao.selectOne("uid", uid);
	}

	@Override
	public Restaurant_PL getRestaurantPLByName(String rname) {
		return rdao.selectOne("rname", rname);
	}

	@Override
	public boolean addRestaurantPL(Restaurant_PL r) {
		boolean flag=false;
		//验证参数
		String rname=IsBlankUtil.isBlank("餐馆名字不能为空",r.getRname());
		//查询角色名字是否已存在
		if(rdao.selectOne("rname",rname)!=null){
			throw new ThisSystemExceptionUtil("1007","餐馆名字已存在");
		}
		//开始注册
		r.setRname(rname);
		flag=rdao.insertRestaurantPL(r);
		return flag;
	}

	@Override
	public boolean modifyRestaurantPL(Restaurant_PL r) {
		boolean flag=false;
		//验证参数
		String rname=IsBlankUtil.isBlank("餐馆名字不能为空",r.getRname());
		String rphone=IsBlankUtil.isBlank("餐馆联系电话不能为空",r.getRphone());
		String raddress=IsBlankUtil.isBlank("餐馆详细地址不能为空",r.getRaddress());
		//查询角色名字是否已存在
		if(rdao.selectOne("rname",rname)!=null){
			throw new ThisSystemExceptionUtil("1007","餐馆名字已存在");
		}
		//开始修改
		r.setRname(rname);
		r.setRphone(rphone);
		r.setRaddress(raddress);
		flag=rdao.updateRestaurantPL(r);
		return flag;
	}

	@Override
	public boolean modifyRestaurantPLState(int rid,int isOpen) {
		boolean flag=false;
		//通过rid获取一个餐馆对象
		Restaurant_PL r=rdao.selectOne("rid", rid);
		//餐馆营业状态取反
		if(isOpen==1){
			isOpen=0;
		}else{
			isOpen=1;
		}
		r.setIsOpen(isOpen);
		flag=rdao.updateRestaurantPL(r);
		return flag;
	}

	@Override
	public boolean modifyRestaurantPLStar(int rid,int star) {
		boolean flag=false;
		//通过rid获取一个餐馆对象
		Restaurant_PL r=rdao.selectOne("rid", rid);
		//修改餐馆星级
		r.setStar(star);
		flag=rdao.updateRestaurantPL(r);
		return flag;
	}
	
}

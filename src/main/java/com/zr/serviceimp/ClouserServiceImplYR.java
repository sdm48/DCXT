package com.zr.serviceimp;

import com.zr.daoimp.ClosedDaoImplYR;
import com.zr.daoimp.LoginDaoImplYR;
import com.zr.service.ClouserServiceYR;

/**
 * 封禁账户的服务实现
 * @author YR
 *
 */
public class ClouserServiceImplYR implements ClouserServiceYR {

	//新建一个LoginDaoImplYR对象
	LoginDaoImplYR loginDaoImplYR=new LoginDaoImplYR();
	//新建一个ClosedDaoImplYR对象
	ClosedDaoImplYR closedDaoImplYR=new ClosedDaoImplYR();
	@Override
	public void coluser(int uid) {
		// 调用loginDaoImplYR的封禁账户方法
		loginDaoImplYR.clouserByUid(uid);
	}
	@Override
	public void recordclo(int mid, int uid, int urole, String cdate, String ccontent) {
		// 调用ClosedDaoImplYR的记录封号方法
		closedDaoImplYR.recordclo(mid, uid, urole, cdate, ccontent);
	}
	@Override
	public void openuser(int uid, int urole) {
		// 调用loginDaoImplYR的解封账户方法
		loginDaoImplYR.openuserByUidUrole(uid, urole);
	}
	@Override
	public void clearrecordclo(int uid) {
		// 调用ClosedDaoImplYR的清除一条记录的方法
		closedDaoImplYR.clearrecordclo(uid);
	}

}

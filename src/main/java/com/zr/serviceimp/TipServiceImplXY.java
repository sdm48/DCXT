package com.zr.serviceimp;

import java.util.List;

import com.zr.dao.TipDaoXY;
import com.zr.daoimp.TipDaoImplXY;
import com.zr.model.TipXY;
import com.zr.service.TipServiceXY;

public class TipServiceImplXY implements TipServiceXY{
	TipDaoXY tdao = new TipDaoImplXY();
	@Override
	public List<TipXY> selectTips(int page, int rows) {
		// TODO Auto-generated method stub
		return tdao.selectTip(page, rows);
	}
	@Override
	public int countTips() {
		// TODO Auto-generated method stub
		return tdao.countTips();
	}

}

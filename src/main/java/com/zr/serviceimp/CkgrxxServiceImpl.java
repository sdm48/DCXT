package com.zr.serviceimp;

import com.zr.dao.CkgrxxDao;
import com.zr.daoimp.CkgrxxDaoimpl;
import com.zr.model.Customer;
import com.zr.service.CkgrxxService;
/**
 * 
 * @author 李昱
 * 通过用户名查询个人信息的service实现
 * @return
 */
public class CkgrxxServiceImpl implements CkgrxxService{
	CkgrxxDao ckd=new CkgrxxDaoimpl();
	@Override
	public Customer getCuByUname(String uname) {
		// TODO Auto-generated method stub
		
		return ckd.getCustomerbyCname(uname);
	}
	
}

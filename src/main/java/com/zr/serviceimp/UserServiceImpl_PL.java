package com.zr.serviceimp;

import com.zr.dao.UserDao_PL;
import com.zr.daoimp.UserDaoImpl_PL;
import com.zr.model.User_PL;
import com.zr.service.UserService_PL;
/**
 * 用户的服务实现
 * @author 彭浪
 *
 */
public class UserServiceImpl_PL implements UserService_PL{
	UserDao_PL udao=new UserDaoImpl_PL();
	@Override
	public User_PL getUserPLByUname(String uname) {
		return udao.selectOne("uname", uname);
	}
	@Override
	public User_PL getUserPLByUid(int uid) {
		return udao.selectOne("uid", uid);
	}
}

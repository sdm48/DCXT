package com.zr.serviceimp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.zr.dao.OrderDao_PL;
import com.zr.daoimp.OrderDaoPLImpl_PL;
import com.zr.model.Order_PL;
import com.zr.service.OrderService_PL;
/**
 * 订单的服务实现
 * @author 彭浪
 *
 */
public class OrderServiceImpl_PL implements OrderService_PL{
	OrderDao_PL odao=new OrderDaoPLImpl_PL();

	@Override
	public List<Order_PL> getOrderPLByRid(int rid,String startTime,String endTime) {
		
		return odao.selectAll("rid", rid,startTime,endTime);
	}

	@Override
	public String[] selectStartTime(int rid) {
		
		return odao.selectStartTime(rid);
	}

	@Override
	public List<String> getWeeks(String dates, String datee) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		Date endDate = null;
		Date startDate = null;
		try {
			startDate = format.parse(dates);
			endDate = format.parse(datee);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<String> ls = new ArrayList<String>();
		Calendar beginCalendar = Calendar.getInstance();
		beginCalendar.setTime(startDate);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);
		int sday = beginCalendar.get(Calendar.DAY_OF_WEEK) - 1;
		int eday = endCalendar.get(Calendar.DAY_OF_WEEK) - 1;
		if (sday != 0) {
		    beginCalendar.add(Calendar.DAY_OF_WEEK, -sday);
		}
		if (eday != 0) {
		    endCalendar.add(Calendar.DAY_OF_WEEK, eday + 7);
		} else {
		    endCalendar.add(Calendar.DAY_OF_WEEK, eday + 1);
		}
		while (beginCalendar.before(endCalendar)) {
		    if (beginCalendar.get(Calendar.DAY_OF_WEEK) - 1 == 0) {
		        ls.add(format.format(beginCalendar.getTime()).toString());
		    }
		    beginCalendar.add(Calendar.DAY_OF_YEAR, 1);
		}
		return ls;
	}

	@Override
	public List<Order_PL> getOrderPLByRid(int rid) {
		return odao.selectAll("rid", rid,0);
	}
	
	@Override
	public List<Order_PL> getAcceptOrderPLByRid(int rid) {
		return odao.selectAll("rid", rid,1);
	}
	
	@Override
	public boolean modifyOrderPL(String orderId) {
		boolean flag=false;
		String[] orders=orderId.split(",");
		int index=0;
		if("订单序号".equals(orders[0])){
			index=1;
		}
		for (int i = index; i < orders.length; i++) {
			int oid=Integer.parseInt(orders[i]);
			Order_PL o=odao.selectone("oid", oid);
			o.setRstate(1);
			flag=odao.updateOrderPL(o);
			
		}
		
		return flag;
	}

	@Override
	public boolean replyOrderPL(String orderId,String reply) {
		boolean flag=false;
		String[] orders=orderId.split(",");
		int index=0;
		if("订单序号".equals(orders[0])){
			index=1;
		}
		for ( int i=index; i < orders.length; i++) {
			int oid=Integer.parseInt(orders[i]);
			Order_PL o=odao.selectone("oid", oid);
			o.setReply(reply);
			flag=odao.updateOrderPL(o);
			
		}
		
		return flag;
		
	}

	@Override
	public Order_PL selectone(int oid) {
		return odao.selectone("oid", oid);
	}
	
}

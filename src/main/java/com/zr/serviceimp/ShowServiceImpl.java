package com.zr.serviceimp;

import java.util.List;

import com.zr.dao.CpDao;
import com.zr.dao.UserDao;
import com.zr.daoimp.CpDaoImpl;
import com.zr.daoimp.UserDaoimpl;
import com.zr.model.Cp_xxd;
import com.zr.model.Restaurant_xxd;
import com.zr.service.ShowService;

public class ShowServiceImpl implements ShowService{
	CpDao cdao = new CpDaoImpl();
	UserDao udao = new UserDaoimpl();
	@Override
	public int getCpsCountByCurrentCp(int uid) {
		// TODO Auto-generated method stub
		return cdao.getCpsCountByUid(uid);
	}


	@Override
	public List<Cp_xxd> getRestaurant(int uid,int page,int rows) {
		// TODO Auto-generated method stub
		return cdao.getCpByUid(uid, page, rows);
	}


	@Override
	public int getUid(String uname) {
		// TODO Auto-generated method stub
		return udao.findUidByUname(uname);
	}
}

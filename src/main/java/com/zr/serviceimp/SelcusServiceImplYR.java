package com.zr.serviceimp;

import java.util.List;

import com.zr.dao.LoginDaoYR;
import com.zr.daoimp.LoginDaoImplYR;
import com.zr.model.User;
import com.zr.service.SelcusServiceYR;
/**
 * 管理员查找某页的顾客服务实现
 * @author YR
 *
 */
public class SelcusServiceImplYR implements SelcusServiceYR{
	//新建一个LoginDaoImplYR
	LoginDaoYR loginDaoYR =new LoginDaoImplYR(); 
	@Override
	public List<User> selcusByPage(int page, int rows) {
		// 服务调用DaoImpl的查找顾客方法
		return loginDaoYR.selcusByPage(page, rows);
	}

}

package com.zr.serviceimp;

import java.util.List;

import com.zr.dao.LoginDaoYR;
import com.zr.daoimp.LoginDaoImplYR;
import com.zr.model.User;
import com.zr.service.SelbossServiceYR;

/**
 * 管理员查找某页的老板服务实现
 * @author YR
 *
 */
public class SelbossServiceImplYR implements SelbossServiceYR{

	//新建一个LoginDaoImplYR
		LoginDaoYR loginDaoYR =new LoginDaoImplYR(); 
	@Override
	public List<User> selbossByPage(int page, int rows) {
		// 服务调用DaoImpl的查找顾客方法
		return loginDaoYR.selbossByPage(page, rows);
	}

}

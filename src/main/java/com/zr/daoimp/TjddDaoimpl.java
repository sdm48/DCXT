package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JSONObject;

import com.zr.dao.TjddDao;
import com.zr.model.OrderLy;
import com.zr.util.JDBCUtil;

public class TjddDaoimpl implements TjddDao{


	@Override
	public boolean tjddbygid(int gid) {
		OrderLy od=new OrderLy();
		boolean flag=false;
		StringBuffer sb=new StringBuffer();
		sb.append(" SELECT * FROM dcxt_gwc");
		sb.append(" INNER JOIN dcxt_dish");
		sb.append(" ON dcxt_dish.Did=dcxt_gwc.did");
		sb.append(" INNER JOIN dcxt_customer");
		sb.append(" ON dcxt_customer.uid=dcxt_gwc.uid");
		sb.append(" WHERE dcxt_gwc.gid=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement pps=null;
		try {
		pps=con.prepareStatement(sb.toString());
			pps.setInt(1, gid);
			ResultSet rs=pps.executeQuery();
			if(rs.next()){
				od.setDid(rs.getInt("did"));
				Date date=new Date();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String odate=sdf.format(date);
				od.setOdate(odate);
				od.setOnunber(rs.getInt("nub"));
				od.setRid(rs.getInt("Rid"));
				od.setUaddress(rs.getString("Caddress"));
				od.setUid(rs.getInt("uid"));
				od.setTotal(rs.getInt("rprice"));
				
			}
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		StringBuffer sql=new StringBuffer();
		sql.append("INSERT INTO dcxt_order(uid,rid,did,onumber,uaddress,odata,total,Ustate,Rstate) ");
		sql.append(" VALUES(?,?,?,?,?,?,?,?,?)");
		Connection con1=JDBCUtil.getConnection();
		try {
			PreparedStatement pps1=con1.prepareStatement(sql.toString());
			pps1.setInt(1, od.getUid());
			pps1.setInt(2, od.getRid());
			pps1.setInt(3, od.getDid());
			pps1.setInt(4, od.getOnunber());
			pps1.setString(5, od.getUaddress());
			pps1.setString(6, od.getOdate());
			pps1.setInt(7, od.getTotal());
			pps1.setInt(8, 1);
			pps1.setInt(9, 0);
			int rs1=pps1.executeUpdate();
			if(rs1!=0){
				flag=true;
			}
			JDBCUtil.closeJDBC(pps, con);
			JDBCUtil.closeJDBC(pps1, con1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return flag;
	}

	@Override
	public boolean jqymaisl(int gid) {

		boolean flag=false;
		StringBuffer sb=new StringBuffer();
		sb.append(" SELECT dcxt_gwc.nub,dcxt_gwc.did FROM dcxt_gwc WHERE dcxt_gwc.gid=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement pps=null;
		try {
		pps=con.prepareStatement(sb.toString());
			pps.setInt(1, gid);
			ResultSet rs=pps.executeQuery();
			if(rs.next()){
			int	nub=rs.getInt("nub");
			int	did=rs.getInt("did");
			
			StringBuffer sb1=new StringBuffer();
			
			sb1.append(" UPDATE dcxt_dish SET dcxt_dish.Dnumber=Dnumber-? WHERE Did=?");			
			Connection con1=JDBCUtil.getConnection();
			PreparedStatement pps1=null;
			try {
			pps1=con1.prepareStatement(sb1.toString());
				pps1.setInt(1, nub);
				pps1.setInt(2, did);
				int rs1=pps1.executeUpdate();
				if(rs1!=0){
					flag=true;
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		// TODO Auto-generated method stub
		return flag;
	}

	@Override
	public int sycpsl(int gid) {
		// TODO Auto-generated method stub
		int a=0;
		StringBuffer sb=new StringBuffer();
		sb.append(" SELECT dcxt_dish.Dnumber FROM dcxt_dish");
		sb.append(" INNER JOIN dcxt_gwc");
		sb.append(" ON dcxt_dish.Did=dcxt_gwc.did");
		sb.append(" WHERE gid=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement pps=null;
		try {
		pps=con.prepareStatement(sb.toString());
			pps.setInt(1, gid);
			ResultSet rs=pps.executeQuery();
			if(rs.next()){
			a=rs.getInt("Dnumber");
			}	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
					
		return a;
	}
	
	

}

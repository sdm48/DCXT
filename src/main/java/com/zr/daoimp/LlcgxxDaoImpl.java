package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import net.sf.json.JSONArray;

import com.zr.dao.LlcgxxDao;
import com.zr.model.RestaurantLy;
import com.zr.util.JDBCUtil;

public class LlcgxxDaoImpl implements LlcgxxDao{

	@Override
	public JSONArray getAllRes(int page, int rows) {
		JSONArray js=new JSONArray();

		StringBuffer sb=new StringBuffer();
		sb.append(" SELECT * FROM dcxt_restaurant where isopen=1  LIMIT ?,?");
		
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());
			psp.setInt(1, (page-1)*rows);
			psp.setInt(2, rows);
			ResultSet rs=psp.executeQuery();
			while(rs.next()){
					RestaurantLy rsly=new RestaurantLy();
					rsly.setRid(rs.getInt("Rid"));
					rsly.setRname(rs.getString("Rname"));
					rsly.setRphone(rs.getString("Rphone"));
					rsly.setRaddress(rs.getString("Raddress"));
					rsly.setUid(rs.getInt("Uid"));
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
					java.util.Date date=new java.util.Date();  
					String str=sdf.format(rs.getDate("WorkDate")); 
					rsly.setWorkData(str);
					rsly.setStar(rs.getInt("Star"));
					rsly.setIsopen("正在营业");
					js.add(rsly);
				
			}
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return js;
		
	}

	@Override
	public int getAllnub() {
		StringBuffer sb=new StringBuffer();
		sb.append(" SELECT COUNT('rid') FROM dcxt_restaurant where isopen=1");
		
		Connection con=JDBCUtil.getConnection();
		int a=0;
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());
			ResultSet rs=psp.executeQuery();
			if(rs.next()){
			 a=rs.getInt("COUNT('rid')");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return a;
	}

}


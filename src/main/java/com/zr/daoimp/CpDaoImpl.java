package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.UpdatableResultSet;
import com.zr.dao.CpDao;
import com.zr.model.Cp_xxd;
import com.zr.util.JDBCUtil;

public class CpDaoImpl implements CpDao{

	@Override
	public List<Cp_xxd> getCpByUid(int uid,int page,int rows) {
		// TODO Auto-generated method stub
		
		List<Cp_xxd> cps = new ArrayList<Cp_xxd>();
		Connection con =JDBCUtil.getConnection();
		PreparedStatement pst = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select did,dname,dnumber,sname,rprice from dcxt_dish");
		sql.append(" INNER JOIN dcxt_restaurant");
		sql.append(" on dcxt_dish.Rid = dcxt_restaurant.Rid");
		sql.append(" INNER JOIN dcxt_login");
		sql.append(" on dcxt_restaurant.Uid = dcxt_login.uid");
		sql.append(" where dcxt_login.uid = ?");
		sql.append(" limit ?,?");
		try {
			pst = con.prepareStatement(sql.toString());
			pst.setInt(1, uid);
			pst.setInt(2, (page-1)*rows);
			pst.setInt(3, rows);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Cp_xxd c = new Cp_xxd();
				c.setDid(rs.getInt("did"));
				c.setDname(rs.getString("dname"));
				c.setDnumber(rs.getInt("dnumber"));
				c.setSname(rs.getString("sname"));
				c.setRprice(rs.getInt("rprice"));
				cps.add(c);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cps;
	}

	@Override
	public int getCpsCountByUid(int uid) {
		// TODO Auto-generated method stub
		int count = 0;
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select COUNT(dcxt_dish.Did) scount FROM dcxt_dish");
		sql.append(" INNER JOIN dcxt_restaurant");
		sql.append(" on dcxt_dish.Rid = dcxt_restaurant.Rid");
		sql.append(" INNER JOIN dcxt_login");
		sql.append(" on dcxt_restaurant.Uid = dcxt_login.uid");
		sql.append(" WHERE dcxt_login.uid = ?");
		try {
			pst = con.prepareStatement(sql.toString());
			pst.setInt(1, uid);
			ResultSet rs = pst.executeQuery();
			if(rs.next()){
				count = rs.getInt("scount");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public Cp_xxd insertCp(String dname,int dnumber,String sname,int rid,int rprice,int isopen) {
		Cp_xxd cp = new Cp_xxd();
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		StringBuffer sql = new StringBuffer();
		sql.append("insert into dcxt_dish(dname,dnumber,sname,rid,rprice,isopen)");
		sql.append(" VALUES(?,?,?,?,?,?)");
		try {
			pst = con.prepareStatement(sql.toString());
			pst.setString(1, dname);
			pst.setInt(2, dnumber);
			pst.setString(3, sname);
			pst.setInt(4, rid);
			pst.setInt(5, rprice);
			pst.setInt(6, isopen);
			int i =pst.executeUpdate();
			if(i!=0){
					//Cp_xxd c = new Cp_xxd();
					cp.setDname(dname);
					cp.setDnumber(dnumber);
					cp.setSname(sname);
					cp.setRid(rid);
					cp.setRprice(rprice);
					cp.setIsopen(isopen);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cp;
	}

	@Override
	public boolean delCpBydid(int did) {
		boolean flag = false;
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		StringBuffer sql = new StringBuffer();
		sql.append("delete from dcxt_dish");
		sql.append(" where dcxt_dish.did = ?");
		try {
			pst = con.prepareStatement(sql.toString());
			pst.setInt(1, did);
			int i =pst.executeUpdate();
			if(i!=0){
				flag = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public boolean upCp(int did,String dname,int dnumber,String sname,int rid,int rprice,int isopen) {
		boolean flag= false;
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		StringBuffer sql = new StringBuffer();
		sql.append("update dcxt_dish set dname=?,dnumber=?,sname=?,rid=?,rprice=?,isopen=?");
		sql.append(" where  did=?");
		try {
			pst = con.prepareStatement(sql.toString());
			pst.setString(1, dname);
			pst.setInt(2, dnumber);
			pst.setString(3, sname);
			pst.setInt(4, rid);
			pst.setInt(5, rprice);
			pst.setInt(6, isopen);
			pst.setInt(7, did);
			int i =pst.executeUpdate();
			if(i!=0){
				flag = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

}

package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import net.sf.json.JSONArray;

import com.zr.dao.CgcpLyDao;
import com.zr.model.DishLy;
import com.zr.model.RestaurantLy;
import com.zr.util.JDBCUtil;

public class CgcpLyDaoimpl implements CgcpLyDao {

	@Override
	public JSONArray Cgcply(int page, int rows, int rid) {
		JSONArray js=new JSONArray();

		StringBuffer sb=new StringBuffer();
		sb.append(" SELECT * FROM dcxt_dish where Rid=?  LIMIT ?,? ");
		
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());
			psp.setInt(1, rid );
			psp.setInt(2, (page-1)*rows);
			psp.setInt(3, rows);
			
			ResultSet rs=psp.executeQuery();
			while(rs.next()){
			DishLy ds=new DishLy();
			ds.setDid(rs.getInt("did"));
			ds.setDname(rs.getString("dname"));
			ds.setDnumber(rs.getInt("dnumber"));
			ds.setRid(rs.getInt("rid"));
			ds.setRprice(rs.getInt("rprice"));
			ds.setSname(rs.getString("sname"));	
				if(rs.getInt("Isopen")==1){
					ds.setIsopen("正在营业");
				}else if(rs.getInt("Isopen")==0){
					ds.setIsopen("已停业");
				}else{
					ds.setIsopen("你猜");
				}		
				js.add(ds);
			}
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return js;
		

}

	@Override
	public int getAllcpByrid(int rid) {
		StringBuffer sb=new StringBuffer();
		sb.append(" SELECT COUNT('Did') FROM dcxt_dish where Rid=?");
		
		Connection con=JDBCUtil.getConnection();
		int a=0;
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());
			psp.setInt(1, rid);
			ResultSet rs=psp.executeQuery();
			if(rs.next()){
			 a=rs.getInt("COUNT('Did')");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return a;
	}
}

package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zr.dao.RestaurantDaoXY;
import com.zr.model.RestaurantXY;
import com.zr.model.TipXY;
import com.zr.util.JDBCUtil;

public class RestaurantDaoImplXY implements RestaurantDaoXY {

	
	@Override
	public RestaurantXY findRestaurantByRid(int rid) {
		// TODO Auto-generated method stub
				RestaurantXY res = new RestaurantXY();
				StringBuffer sql = new StringBuffer();
				sql.append(" SELECT * FROM dcxt_restaurant ");
				sql.append(" WHERE Rid=?");
				Connection con = JDBCUtil.getConnection();
				PreparedStatement pst = null;
				try {
					 pst =con.prepareStatement(sql.toString());
					pst.setInt(1, rid);
					ResultSet rs = pst.executeQuery();
					if(rs.next()){
						res.setIsopen(rs.getInt("IsOpen"));
						res.setRaddress(rs.getString("Raddress"));
						res.setRid(rs.getInt("Rid"));
						res.setRname(rs.getString("Rname"));
						res.setRphone(rs.getString("Rphone"));
						res.setStar(rs.getInt("Star"));
						res.setWorkdate(rs.getString("WorkDate"));
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					JDBCUtil.closeJDBC(pst, con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return res;
	}

	@Override
	public int deleteRestaurantByRid(int rid) {
		// TODO Auto-generated method stub
		int isopen = 0;
		RestaurantXY res = new RestaurantXY();
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE dcxt_restaurant");
		sql.append(" SET IsOpen=2 WHERE Rid=?");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			 pst = con.prepareStatement(sql.toString());
			 pst.setInt(1, rid);
			 isopen = pst.executeUpdate();
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isopen;
	}

	@Override
	public List<RestaurantXY> selectRestaurantJinyong(int page,int rows) {
		// TODO Auto-generated method stub
		List<RestaurantXY> rests = new ArrayList<RestaurantXY>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * from dcxt_restaurant WHERE IsOpen=2 LIMIT ?,? ");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			 pst =con.prepareStatement(sql.toString());
			pst.setInt(1, (page-1)*rows);
			pst.setInt(2, rows);
			
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				RestaurantXY res = new RestaurantXY();
				res.setIsopen(rs.getInt("IsOpen"));
				res.setRaddress(rs.getString("Raddress"));
				res.setRid(rs.getInt("Rid"));
				res.setRname(rs.getString("Rname"));
				res.setRphone(rs.getString("Rphone"));
				res.setStar(rs.getInt("Star"));
				res.setWorkdate(rs.getString("WorkDate"));
				rests.add(res);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rests;
	}

	@Override
	public int countRests() {
		// TODO Auto-generated method stub
		int count  = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT count(rid) FROM dcxt_restaurant WHERE IsOpen=2");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			pst =con.prepareStatement(sql.toString());
			
			
			ResultSet rs = pst.executeQuery();
			if(rs.next()){
				count = rs.getInt("count(rid)");
			
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return count;
	}

	@Override
	public void jiefengRest(int rid) {
		// TODO Auto-generated method stub
		
		RestaurantXY res = new RestaurantXY();
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE dcxt_restaurant");
		sql.append(" SET IsOpen=1 WHERE Rid=?");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			 pst = con.prepareStatement(sql.toString());
			 pst.setInt(1, rid);
			  pst.executeUpdate();
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	

}

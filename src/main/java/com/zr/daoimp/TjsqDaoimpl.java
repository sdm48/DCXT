package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.crypto.Data;

import com.zr.dao.TJsqDao;
import com.zr.dao.TjddDao;
import com.zr.util.JDBCUtil;
/**
 * 通过uid拿到提交表单需要申请发送给管理员
 * @author Administrator
 *
 */
public class TjsqDaoimpl implements TJsqDao{

	@Override
	public boolean tjsq(int uid) {
		boolean flag=false;
		StringBuffer sb=new StringBuffer();
		sb.append(" INSERT INTO dcxt_apply(uid,mid,Adate) VALUES (?,1,?)");
	Connection con=JDBCUtil.getConnection();
	try {
		PreparedStatement pps=con.prepareStatement(sb.toString());
		pps.setInt(1, uid);
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String odate=sdf.format(date);
		pps.setString(2, odate);
		int a=pps.executeUpdate();
		if(a>0){ 
			flag=true;
		}
		JDBCUtil.closeJDBC(pps, con);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
		
		// TODO Auto-generated method stub
		return flag;
	}

}

package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zr.dao.TipDaoXY;
import com.zr.model.TipXY;
import com.zr.util.JDBCUtil;

public class TipDaoImplXY implements TipDaoXY{

	@Override
	public List<TipXY> selectTip(int page, int rows) {
		// TODO Auto-generated method stub
		List<TipXY> tips = new ArrayList<TipXY>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * from dcxt_tip LIMIT ?,?");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			 pst =con.prepareStatement(sql.toString());
			pst.setInt(1, (page-1)*rows);
			pst.setInt(2, rows);
			
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				TipXY tip = new TipXY();
				tip.setDate(rs.getString("Date"));
				tip.setOid(rs.getInt("Oid"));
				tip.setTcontent(rs.getString("Tcontent"));
				tip.setTid(rs.getInt("Tid"));
				tips.add(tip);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tips;
	}

	@Override
	public int countTips() {
		// TODO Auto-generated method stub
		int count  = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT count(tid) FROM dcxt_tip");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			pst =con.prepareStatement(sql.toString());
			
			
			ResultSet rs = pst.executeQuery();
			if(rs.next()){
				count = rs.getInt("count(tid)");
			
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return count;
	}

}

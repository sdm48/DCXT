package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zr.dao.DishDao_PL;
import com.zr.model.Dish_PL;
import com.zr.util.JDBCUtil;
/**
 * 菜的Dao层实现
 * @author 彭浪
 *
 */
public class DishDaoPLImpl_PL implements DishDao_PL{

	@Override
	public Dish_PL selectOne(String uniqueField, Object value) {
		StringBuffer sql=new StringBuffer("select * from dcxt_dish where ");
		sql.append(uniqueField).append("=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setObject(i++, value);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return resultSetEntity(rs);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	public Dish_PL resultSetEntity(ResultSet rs) throws SQLException {
		Dish_PL d=new Dish_PL();
		d.setDid(rs.getInt("did"));
		d.setDname(rs.getString("dname"));
		d.setDnumber(rs.getInt("dnumber"));
		d.setIsOpen(rs.getInt("isOpen"));
		d.setRid(rs.getInt("rid"));
		d.setRprice(rs.getInt("rprice"));
		d.setSname(rs.getString("sname"));
		return d;
	}
	@Override
	public boolean updateDishPL(Dish_PL d) {
		boolean flag=false;
		StringBuffer sql=new StringBuffer("update dcxt_dish set ");
		sql.append("dnumber=dnumber+? ");
		sql.append("where did=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setInt(i++, d.getDnumber());
			ps.setInt(i++, d.getDid());
			int result=ps.executeUpdate();
			if(result>0){
				flag=true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}
}

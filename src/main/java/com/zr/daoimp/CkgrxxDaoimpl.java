package com.zr.daoimp;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zr.dao.CkgrxxDao;
import com.zr.model.Customer;
import com.zr.util.JDBCUtil;
/**
 * 
 * @author 李昱
 * 通过用户名查询个人信息的具体dao实现
 * @return
 */
public class CkgrxxDaoimpl implements CkgrxxDao {

	@Override
	public Customer getCustomerbyCname(String uname) {
		Customer c=new Customer();
		StringBuffer sql=new StringBuffer();
		sql.append(" SELECT * FROM dcxt_customer");
		sql.append(" INNER JOIN dcxt_login");
		sql.append(" ON dcxt_login.uid=dcxt_customer.uid");
		sql.append(" WHERE dcxt_login.uname=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement psp = null;
		try {
			String s=null;
			psp = con.prepareStatement(sql.toString());
			psp.setString(1, uname);
			ResultSet rs=psp.executeQuery();
			if(rs.next()){
				c.setCaddress(rs.getString("Caddress"));
				int a=rs.getInt("Csex");
				if(a==1){
					c.setCsex("男");
				}
				else{
					c.setCsex("女");
				}
				c.setCname(uname);	
				c.setOid(rs.getInt("Oid"));
				c.setCphone(rs.getInt("uphone"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return c;
	}

}

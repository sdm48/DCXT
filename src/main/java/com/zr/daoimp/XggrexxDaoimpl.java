package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zr.dao.XggrxxDao;
import com.zr.util.JDBCUtil;

public class XggrexxDaoimpl implements XggrxxDao {

	@Override
	public boolean changeNameByid(String cname,int uid) {
		// TODO Auto-generated method stub
		Boolean flag=false;
		StringBuffer sb=new StringBuffer();
		sb.append("update dcxt_login set uname=? where uid=?");
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());			
			psp.setString(1, cname);
			
			psp.setInt(2, uid);
			int a=psp.executeUpdate();
			if(a!=0){
				flag=true;
			}
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return flag;
	}

	@Override
	public boolean changeCaddressByid(int uid, String Caddress) {
		// TODO Auto-generated method stub
		Boolean flag=false;
		StringBuffer sb=new StringBuffer();
		sb.append("update dcxt_customer set Caddress=?  where uid=?");
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());
			psp.setString(1, Caddress);
			psp.setInt(2, uid);
			int a=psp.executeUpdate();
			if(a!=0){
				flag=true;
			}
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
	}

	@Override
	public boolean changeCphoneByid(int uid, String Cphone) {
		Boolean flag=false;
		StringBuffer sb=new StringBuffer();
		sb.append("update dcxt_login set uphone=?  where uid=?");
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());
			psp.setString(1, Cphone);
			psp.setInt(2, uid);
			int a=psp.executeUpdate();
			if(a!=0){
				flag=true;
			}
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
	}


	@Override
	public boolean changeCsexByid(int uid, String Csex) {
		Boolean flag=false;
		StringBuffer sb=new StringBuffer();
		sb.append("update dcxt_customer set Csex=?  where uid=?");
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());		
			if(Csex.equals("男")){
				psp.setInt(1, 1);
			}else{
				psp.setInt(1, 2);	
			}
			psp.setInt(2, uid);
			int a=psp.executeUpdate();
			if(a!=0){
				flag=true;
			}
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
	}


	@Override
	public boolean changeUpasswordByid(int uid, String Upassword) {
		Boolean flag=false;
		StringBuffer sb=new StringBuffer();
		sb.append("update dcxt_login set Upassword=? where uid=?");
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());
			psp.setString(1, Upassword);
			psp.setInt(2, uid);
			int a=psp.executeUpdate();
			if(a!=0){
				flag=true;
			}
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
	}

	@Override
	public boolean isnotSameName(int uid, String cname) {
		Boolean flag=true;
		StringBuffer sb=new StringBuffer();
		sb.append("SELECT * from dcxt_login WHERE uname=?");
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());
			psp.setString(1, cname);
			ResultSet rs=psp.executeQuery();
			if(rs.next()){
				flag=false;
			}
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
	}


}

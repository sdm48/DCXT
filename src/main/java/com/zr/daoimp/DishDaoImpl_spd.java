package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zr.dao.DishDao_spd;
import com.zr.model.Dish_spd;
import com.zr.util.JDBCUtil;

public class DishDaoImpl_spd implements DishDao_spd{
	@Override
	public List<Dish_spd> getAllDishs(int page, int rows) {
		List<Dish_spd> dishs = new ArrayList<Dish_spd>();
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from dcxt_dish ");
		sql.append(" limit ?,? ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, (page-1)*rows);
			ps.setInt(2, rows);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Dish_spd d = new Dish_spd();
				d.setDid(rs.getInt("did"));
				d.setdName(rs.getString("dName"));
				d.setdNumber(rs.getInt("dNumber"));
				d.setsName(rs.getString("sName"));
				d.setRid(rs.getInt("rid"));
				d.setrPrice(rs.getInt("rPrice"));
				d.setIsOPen(rs.getInt("isOpen"));
				dishs.add(d);
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dishs;
	}

	@Override
	public Dish_spd getSelectDish(int did) {
		Dish_spd selectDish = new Dish_spd();
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from dcxt_dish ");
		sql.append(" where did=? ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, did);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				selectDish.setDid(rs.getInt("did"));
				selectDish.setdName(rs.getString("dName"));
				selectDish.setdNumber(rs.getInt("dNumber"));
				selectDish.setsName(rs.getString("sName"));
				selectDish.setRid(rs.getInt("rid"));
				selectDish.setrPrice(rs.getInt("rPrice"));
				selectDish.setIsOPen(rs.getInt("isOpen"));
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return selectDish;
	}

	@Override
	public List<String> getDidSname() {
		List<String> snames = new ArrayList<String>();
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select DISTINCT sname from dcxt_dish ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String sname = rs.getString("sname");
				snames.add(sname);
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return snames;
	}

	@Override
	public List<Dish_spd> getDishsBySname(String sname) {
		List<Dish_spd> dishs = new ArrayList<Dish_spd>();
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from dcxt_dish ");
		sql.append(" where sname=? ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setString(1, sname);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Dish_spd d = new Dish_spd();
				d.setDid(rs.getInt("did"));
				d.setdName(rs.getString("dName"));
				d.setdNumber(rs.getInt("dNumber"));
				d.setsName(rs.getString("sName"));
				d.setRid(rs.getInt("rid"));
				d.setrPrice(rs.getInt("rPrice"));
				d.setIsOPen(rs.getInt("isOpen"));
				dishs.add(d);
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dishs;
	}

	@Override
	public List<Dish_spd> getDishsBySearchName(String searchName) {
		Connection con = JDBCUtil.getConnection();
		List<Dish_spd> dishs = new ArrayList<Dish_spd>();
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from dcxt_dish ");
		sql.append(" where dname LIKE ? ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setString(1, "%"+searchName+"%");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Dish_spd d = new Dish_spd();
				d.setDid(rs.getInt("did"));
				d.setdName(rs.getString("dName"));
				d.setdNumber(rs.getInt("dNumber"));
				d.setsName(rs.getString("sName"));
				d.setRid(rs.getInt("rid"));
				d.setrPrice(rs.getInt("rPrice"));
				d.setIsOPen(rs.getInt("isOpen"));
				dishs.add(d);
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dishs;
	}

	@Override
	public boolean insertOrder(int userId, int restId, int dishId, int dishNumber, String address, int totalPrice, String addTime) {
		boolean flag = false;
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into dcxt_order(uid, rid, did, onumber, uaddress, total, odata) ");
		sql.append(" VALUES(?,?,?,?,?,?,?) ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, userId);
			ps.setInt(2, restId);
			ps.setInt(3, dishId);
			ps.setInt(4, dishNumber);
			ps.setString(5, address);
			ps.setInt(6, totalPrice);
			ps.setString(7, addTime);
			int i = ps.executeUpdate();
			if(i != 0) {
				flag = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}
/*
	@Override
	public boolean insertTip(int oid, String tipText, Date tipDate) {
		boolean flag = false;
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into dcxt_tip(oid, tcontent, date) ");
		sql.append(" VALUES(?, ?, ?) ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, oid);
			ps.setString(2, tipText);
			ps.setDate(3, tipDate);
			int i = ps.executeUpdate();
			if(i != 0) {
				flag = true;
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}*/
	@Override
	public int getAllDishsNum() {
		int total = 0;
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select count(did) dcount from dcxt_dish");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				total = rs.getInt("dcount");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return total;
	}

	@Override
	public boolean insertTip(int oid, String tipText, String tipDate) {
		boolean flag = false;
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into dcxt_tip(oid, tcontent, date) ");
		sql.append(" VALUES(?, ?, ?) ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, oid);
			ps.setString(2, tipText);
			ps.setString(3, tipDate);
			int i = ps.executeUpdate();
			if(i != 0) {
				flag = true;
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}
}

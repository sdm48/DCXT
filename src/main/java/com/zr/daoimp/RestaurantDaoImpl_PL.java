package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.zr.dao.RestaurantDao_PL;
import com.zr.model.Restaurant_PL;
import com.zr.util.JDBCUtil;

/**
 * 餐馆的Dao层实现
 * @author 彭浪
 *
 */
public class RestaurantDaoImpl_PL implements RestaurantDao_PL{

	@Override
	public boolean insertRestaurantPL(Restaurant_PL r) {
		boolean flag=false;
		StringBuffer sql=new StringBuffer("insert into dcxt_restaurant ");
		sql.append("(rid,rname,rphone,raddress,uid,workdate,star,isopen) ");
		sql.append("values(?,?,?,?,?,?,?,?)");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			
			Date dt = new Date();
			SimpleDateFormat sdf = 
			new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			r.setCreateDate(sdf.format(dt));
			
			ps.setInt(i++, r.getRid());
			ps.setString(i++, r.getRname());
			ps.setString(i++, r.getRphone());
			ps.setString(i++, r.getRaddress());
			ps.setInt(i++, r.getUid());
			ps.setString(i++, r.getCreateDate());
			ps.setInt(i++, r.getStar());
			ps.setInt(i++, r.getIsOpen());
			int result=ps.executeUpdate();
			if(result>0){
				flag=true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public boolean updateRestaurantPL(Restaurant_PL r) {
		boolean flag=false;
		StringBuffer sql=new StringBuffer("update dcxt_restaurant set ");
		sql.append("rname=?,rphone=?,raddress=?,uid=?,star=?,isopen=? ");
		sql.append("where rid=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setString(i++, r.getRname());
			ps.setString(i++, r.getRphone());
			ps.setString(i++, r.getRaddress());
			ps.setInt(i++, r.getUid());
			ps.setInt(i++, r.getStar());
			ps.setInt(i++, r.getIsOpen());
			ps.setInt(i++, r.getRid());
			int result=ps.executeUpdate();
			if(result>0){
				flag=true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public Restaurant_PL selectOne(String uniqueField, Object value) {
		StringBuffer sql=new StringBuffer("select * from dcxt_restaurant where ");
		sql.append(uniqueField).append("=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setObject(i++, value);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return resultSetEntity(rs);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public Restaurant_PL resultSetEntity(ResultSet rs) throws SQLException {
		Restaurant_PL r=new Restaurant_PL();
		r.setRid(rs.getInt("rid"));
		r.setRname(rs.getString("rname"));
		r.setRphone(rs.getString("rphone"));
		r.setRaddress(rs.getString("raddress"));
		r.setUid(rs.getInt("uid"));
		r.setCreateDate(rs.getString("workdate"));
		r.setStar(rs.getInt("star"));
		r.setIsOpen(rs.getInt("isOpen"));
		return r;
	}
	
}

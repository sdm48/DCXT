
package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zr.dao.ScgwcDao;
import com.zr.util.JDBCUtil;

public class ScgwcDaoimpl implements ScgwcDao{

	@Override
	public boolean scgwcByGid(int gid) {
		// TODO Auto-generated method stub
		boolean  flag = false;
		StringBuffer  sql = new StringBuffer();
		sql.append(" DELETE FROM dcxt_gwc WHERE gid=?");

		Connection  con = JDBCUtil.getConnection();
		try {
			PreparedStatement  pst = con.prepareStatement(sql.toString());
			pst.setInt(1,gid);
			int a=pst.executeUpdate();
			if(a!=0){
				flag = true;
			}
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
		
	}

}

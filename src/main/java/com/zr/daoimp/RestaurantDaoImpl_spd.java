package com.zr.daoimp;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zr.dao.RestaurantDao_spd;
import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;
import com.zr.util.JDBCUtil;

public class RestaurantDaoImpl_spd implements RestaurantDao_spd{
	@Override
	public List<Restaurant_spd> getAllRestaurantMessage() {
		List<Restaurant_spd> rests = new ArrayList<Restaurant_spd>();
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from dcxt_restaurant ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Restaurant_spd r = new Restaurant_spd();
				r.setRid(rs.getInt("rid"));
				r.setrName(rs.getString("rname"));
				r.setrPhone(rs.getString("rphone"));
				r.setrAddress(rs.getString("raddress"));
				r.setUid(rs.getInt("uid"));
				r.setWorkDate(rs.getDate("workdate"));
				r.setStar(rs.getInt("star"));
				r.setIsOpen(rs.getInt("isopen"));
				rests.add(r);
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rests;
	}
	/*@Override
	public boolean collectRestaurant(int uid, int rid, Date addTime) {
		boolean flag = false;
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into dcxt_collect(uid, rid, addTime) ");
		sql.append(" values(?,?,?) ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, uid);
			ps.setInt(2, rid);
			ps.setDate(3, addTime);
			int i = ps.executeUpdate();
			if(i != 0) {
				flag = true;
			}
			JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}*/
	@Override
	public Restaurant_spd getRestaurantByRid(int rid) {
		Restaurant_spd r = new Restaurant_spd();
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from dcxt_restaurant ");
		sql.append(" where rid=? ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, rid);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				r.setRid(rs.getInt("rid"));
				r.setrName(rs.getString("rname"));
				r.setrPhone(rs.getString("rphone"));
				r.setrAddress(rs.getString("raddress"));
				r.setUid(rs.getInt("uid"));
				r.setWorkDate(rs.getDate("workdate"));
				r.setStar(rs.getInt("star"));
				r.setIsOpen(rs.getInt("isopen"));
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return r;
	}
	@Override
	public List<Order_spd> getOrdersByUid(int uid) {
		List<Order_spd> orders = new ArrayList<Order_spd>();
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from dcxt_order ");
		sql.append(" where uid=? ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1,uid);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Order_spd o = new Order_spd();
				o.setOid(rs.getInt("oid"));
				o.setDid(rs.getInt("did"));
				o.setRid(rs.getInt("rid"));
				o.setoNumber(rs.getInt("Onumber"));
				o.setTotal(rs.getInt("total"));
				orders.add(o);
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orders;
	}
	@Override
	public boolean collectRestaurant(int uid, int rid, String addTime) {
		boolean flag = false;
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into dcxt_collect(uid, rid, addTime) ");
		sql.append(" values(?,?,?) ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, uid);
			ps.setInt(2, rid);
			ps.setString(3, addTime);
			int i = ps.executeUpdate();
			if(i != 0) {
				flag = true;
			}
			JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}
	@Override
	public int getRestaurantCount() {
		int count = 0;
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select count(rid) scount ");
		sql.append(" from dcxt_restaurant ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				count = rs.getInt("scount");
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
}

package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zr.dao.ApplyDaoXY;
import com.zr.model.ApplyXY;
import com.zr.model.RestaurantXY;
import com.zr.model.TipXY;
import com.zr.util.JDBCUtil;

public class ApplyDaoImplXY implements ApplyDaoXY{

	@Override
	public List<ApplyXY> selectApply(int page, int rows) {
		// TODO Auto-generated method stub
		List<ApplyXY> applys = new ArrayList<ApplyXY>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * from dcxt_apply LIMIT ?,?");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			 pst =con.prepareStatement(sql.toString());
			pst.setInt(1, (page-1)*rows);
			pst.setInt(2, rows);
			
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				ApplyXY apply = new ApplyXY();
				apply.setAdate(rs.getString("Adate"));
				apply.setAid(rs.getInt("Aid"));
				apply.setAispass(rs.getInt("Aispass"));
				apply.setMid(rs.getInt("Mid"));
				apply.setPassdate(rs.getString("Passdate"));
				apply.setUid(rs.getInt("Uid"));
				applys.add(apply);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return applys;
	}

	@Override
	public int countApplys() {
		// TODO Auto-generated method stub
		int count  = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT count(aid) FROM dcxt_apply");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			pst =con.prepareStatement(sql.toString());
			
			
			ResultSet rs = pst.executeQuery();
			if(rs.next()){
				count = rs.getInt("count(aid)");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return count;
	}

	@Override
	public void passApply(int aid,String now) {
		// TODO Auto-generated method stub

		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE dcxt_apply");
		sql.append(" SET AIsPass=2,PassDate=? WHERE aid=?");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			 pst = con.prepareStatement(sql.toString());
			 pst.setString(1, now);
			 pst.setInt(2,aid );
			 pst.executeUpdate();
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	@Override
	public void notpassApply(int aid) {
		// TODO Auto-generated method stub
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE dcxt_apply");
		sql.append(" SET AIsPass=1 WHERE aid=?");
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		try {
			 pst = con.prepareStatement(sql.toString());
			 pst.setInt(1, aid);
			 pst.executeUpdate();
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

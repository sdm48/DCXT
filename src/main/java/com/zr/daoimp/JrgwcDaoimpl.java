package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.zr.dao.jrgwcDao;
import com.zr.util.JDBCUtil;

public class JrgwcDaoimpl implements jrgwcDao{

	@Override
	public boolean jrgwcByid(String did, String dname, String rprice,
			String nub, int uid) {
		StringBuffer sb=new StringBuffer("");
		boolean flag=false;
		sb.append(" insert into dcxt_gwc(did,uid,dname,rprice,nub)");
		sb.append(" VALUES (?,?,?,?,?)");
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement psp=con.prepareStatement(sb.toString());
			psp.setString(1, did);
			psp.setInt(2, uid);
			psp.setString(3, dname);
			int a1=Integer.parseInt(rprice);
			int b=Integer.parseInt(nub);
			String c=String.valueOf(a1*b);
			psp.setString(4, c);
			psp.setString(5, nub);
			
			int a=psp.executeUpdate();
			if(a!=0){
				flag=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return flag;
	}

}

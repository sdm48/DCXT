package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.sf.json.JSONArray;

import com.zr.dao.WdgwcDao;
import com.zr.model.gwc;
import com.zr.util.JDBCUtil;

public class WdgwcDaoimpl implements WdgwcDao{

	@Override
	public JSONArray getgwcbyuid(int uid) {
	
		JSONArray js=new JSONArray();
		StringBuffer sb=new StringBuffer();
		sb.append(" SELECT * FROM dcxt_gwc WHERE uid=?");
		Connection con=JDBCUtil.getConnection();
		try {
			PreparedStatement pps=con.prepareStatement(sb.toString());
			pps.setInt(1,uid);
			ResultSet rs=pps.executeQuery();
			while(rs.next()){
				gwc g=new gwc();
				g.setDname(rs.getString("dname"));
				g.setNbu(rs.getInt("nub"));
				g.setRprice(rs.getString("rprice"));
				g.setGid(rs.getInt("gid"));
				js.add(g);
			}
			JDBCUtil.closeJDBC(pps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		// TODO Auto-generated method stub
		return js;
	}

}

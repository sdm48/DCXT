

package com.zr.daoimp;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zr.dao.UserDao;
import com.zr.model.Customer;
import com.zr.util.JDBCUtil;

public class UserDaoimpl implements  UserDao{

	@Override
	public boolean findUserByName(String uname, String upswd) {
		// TODO Auto-generated method stub
		boolean  flag = false;
		StringBuffer  sql = new StringBuffer();
		sql.append(" select * from dcxt_login");
		sql.append("  where  uname = ? and upassword=?");
		Connection  con = JDBCUtil.getConnection();
		try {
			PreparedStatement  pst = con.prepareStatement(sql.toString());
			pst.setString(1,uname);
			pst.setString(2,upswd);
			ResultSet rs = pst.executeQuery();
			if(rs.next()){
				flag = true;
			}
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
}

	@Override
	public Customer getCubyName(String uname) {
		Customer c=new Customer();
		StringBuffer sql=new StringBuffer();
		sql.append(" SELECT * FROM dcxt_customer");
		sql.append(" INNER JOIN dcxt_login");
		sql.append(" ON dcxt_login.uid=dcxt_customer.uid");
		sql.append(" WHERE dcxt_login.uname=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement psp = null;
		try {
			psp = con.prepareStatement(sql.toString());
			psp.setString(1, uname);
			ResultSet rs=psp.executeQuery();
			if(rs.next()){
				c.setCaddress(rs.getString("Caddress"));
				int a=rs.getInt("Csex");
				if(a==1){
					c.setCsex("男");
				}
				else{
					c.setCsex("女");
				}
				c.setCname(uname);	
				c.setOid(rs.getInt("Oid"));
				c.setCphone(rs.getInt("uphone"));
				c.setUpassword(rs.getString("upassword"));
				c.setUid(rs.getInt("uid"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(psp, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return c;
	}

	@Override
	public int findUidByUname(String uname) {
		// TODO Auto-generated method stub
		int uid =0;
		Connection con = JDBCUtil.getConnection();
		PreparedStatement pst = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select dcxt_login.uid from dcxt_login");
		sql.append(" where dcxt_login.uname = ?");
		try {
			pst = con.prepareStatement(sql.toString());
			pst.setString(1, uname);
			ResultSet rs =pst.executeQuery();
			if(rs.next()){
				uid =rs.getInt("uid");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return uid;
	}
}


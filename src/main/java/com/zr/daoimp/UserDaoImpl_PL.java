package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zr.dao.UserDao_PL;
import com.zr.model.User_PL;
import com.zr.util.JDBCUtil;
/**
 * 用户的Dao层实现
 * @author 彭浪
 *
 */
public class UserDaoImpl_PL implements UserDao_PL{

	@Override
	public User_PL selectOne(String uniqueField, Object value) {
		StringBuffer sql=new StringBuffer("select * from dcxt_login where ");
		sql.append(uniqueField).append("=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setObject(i++, value);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return resultSetEntity(rs);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	public User_PL resultSetEntity(ResultSet rs) throws SQLException {
		User_PL u=new User_PL();
		u.setUid(rs.getInt("uid"));
		u.setUname(rs.getString("uname"));
		u.setUpassword(rs.getString("upassword"));
		u.setUphone(rs.getString("uphone"));
		u.setUrole(rs.getInt("urole"));
		return u;
	}
}

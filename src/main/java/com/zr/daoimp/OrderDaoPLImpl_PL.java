package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zr.dao.OrderDao_PL;
import com.zr.model.Order_PL;
import com.zr.util.JDBCUtil;
/**
 * 订单的Dao层实现
 * @author 彭浪
 *
 */
public class OrderDaoPLImpl_PL implements OrderDao_PL{

	@Override
	public List<Order_PL> selectAll(String uniqueField, Object value,String startTime,String endTime) {
		List<Order_PL> list=new ArrayList<Order_PL>();
		StringBuffer sql=new StringBuffer("select * from dcxt_order where ");
		sql.append(uniqueField).append("=? ");
		sql.append("and Odata between ? and ? ");
		sql.append("and Rstate=1 and Ustate=1");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setObject(i++, value);
			ps.setString(i++, startTime);
			ps.setString(i++, endTime);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(resultSetEntity(rs));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	@Override
	public String[] selectStartTime(int rid) {
		String[] date=new String[3];
		StringBuffer sql=new StringBuffer("select year(min(Odata)) as year, ");
		sql.append("month(min(Odata)) as month, ");
		sql.append("day(min(Odata)) as day ");
		sql.append("from dcxt_order where ");
		sql.append("rid=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setObject(i++, rid);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				date[0]=rs.getString("year");
				date[1]=rs.getString("month");
				date[2]=rs.getString("day");
			}
			return date;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public Order_PL resultSetEntity(ResultSet rs) throws SQLException {
		Order_PL o=new Order_PL();
		o.setDate(rs.getString("date"));
		o.setDid(rs.getString("did"));
		o.setJudge(rs.getString("judge"));
		o.setJudgeNum(rs.getInt("judgeNum"));
		o.setOdata(rs.getString("odata"));
		o.setOid(rs.getInt("oid"));
		o.setOnumber(rs.getString("onumber"));
		o.setReply(rs.getString("reply"));
		o.setRid(rs.getInt("rid"));
		o.setRstate(rs.getInt("rstate"));
		o.setTotal(rs.getInt("total"));
		o.setUaddress(rs.getString("uaddress"));
		o.setUid(rs.getInt("uid"));
		o.setUstate(rs.getInt("ustate"));
		return o;
	}

	@Override
	public List<Order_PL> selectAll(String uniqueField, Object value,int rstate) {
		List<Order_PL> list=new ArrayList<Order_PL>();
		StringBuffer sql=new StringBuffer("select * from dcxt_order where ");
		sql.append(uniqueField).append("=? ");
		sql.append("and rstate=? and ustate=1 ");
		sql.append("and reply is null");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setObject(i++, value);
			ps.setInt(i++, rstate);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(resultSetEntity(rs));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public boolean updateOrderPL(Order_PL o) {
		boolean flag=false;
		StringBuffer sql=new StringBuffer("update dcxt_order set ");
		sql.append("rstate=?,reply=? ");
		sql.append("where oid=?");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setInt(i++, o.getRstate());
			ps.setString(i++, o.getReply());
			ps.setInt(i++, o.getOid());
			int result=ps.executeUpdate();
			if(result>0){
				flag=true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public Order_PL selectone(String uniqueField, Object value) {
		StringBuffer sql=new StringBuffer("select * from dcxt_order where ");
		sql.append(uniqueField).append("=? ");
		Connection con=JDBCUtil.getConnection();
		PreparedStatement ps=null;
		try {
			ps=con.prepareStatement(sql.toString());
			int i=1;
			ps.setObject(i++, value);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return resultSetEntity(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				JDBCUtil.closeJDBC(ps, con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}

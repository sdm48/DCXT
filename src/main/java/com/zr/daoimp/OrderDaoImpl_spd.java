package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zr.dao.OrderDao_spd;
import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;
import com.zr.util.JDBCUtil;

public class OrderDaoImpl_spd implements OrderDao_spd{
	@Override
	public List<Order_spd> getAllOrdersByUid(int uid) {
		List<Order_spd> orders = new ArrayList<Order_spd>();
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from dcxt_order ");
		sql.append(" where uid=? ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, uid);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Order_spd o = new Order_spd();
				o.setOid(rs.getInt("oid"));
				o.setUid(rs.getInt("uid"));
				o.setRid(rs.getInt("rid"));
				o.setDid(rs.getInt("did"));
				o.setoNumber(rs.getInt("onumber"));
				o.setuAddress(rs.getString("uaddress"));
				o.setrState(rs.getInt("rstate"));
				o.setuState(rs.getInt("ustate"));
				o.setJudge(rs.getString("judge"));
				o.setReply(rs.getString("reply"));
				o.setTotal(rs.getInt("total"));
				o.setoDate(rs.getString("odata"));
				o.setJudgeDate(rs.getString("date"));
				o.setJudgeNum(rs.getInt("judgeNum"));
				orders.add(o);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orders;
	}

	@Override
	public Restaurant_spd getRestaurantByOid(int oid) {
		Restaurant_spd restaurant = new Restaurant_spd();
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" select * from dcxt_restaurant ");
		sql.append(" INNER JOIN dcxt_order ");
		sql.append(" on dcxt_restaurant.rid = dcxt_order.Rid ");
		sql.append(" where oid=? ");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, oid);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				restaurant.setrName(rs.getString("rname"));
			}
			JDBCUtil.closeJDBC(ps, con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return restaurant;
	}

	@Override
	public boolean addJudgeTextAndTime(String judgeText, String addTime, int judgeNum, int oid) {
		boolean flag = false;
		Connection con = JDBCUtil.getConnection();
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE dcxt_order ");
		sql.append(" set Judge=?,Date=?,JudgeNum=? ");
		sql.append("where oid=?");
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql.toString());
			ps.setString(1, judgeText);
			ps.setString(2, addTime);
			ps.setInt(3, judgeNum);
			ps.setInt(4, oid);
			int i = ps.executeUpdate();
			if(i != 0) {
				flag = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}
}

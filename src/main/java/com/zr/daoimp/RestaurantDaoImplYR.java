package com.zr.daoimp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zr.dao.RestaurantDaoYR;
import com.zr.model.RestaurantYR;
import com.zr.model.User;
import com.zr.util.JDBCUtil;

/**
 * Restaurant表的Dao层实现
 * @author YR
 *
 */
public class RestaurantDaoImplYR implements RestaurantDaoYR{

	@Override
	public List<RestaurantYR> selresByPage(int page, int rows) {
		//新建一个RestaurantYR集合
		List<RestaurantYR> lres = new ArrayList<RestaurantYR>();
		// sql语句,查询出某页的RestaurantYR
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM dcxt_restaurant");
		sql.append(" LIMIT ?,?");
		// 通过工具包建立connection
		Connection con = JDBCUtil.getConnection();
		// 声明预编译器
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(sql.toString());
			pst.setInt(1, (page - 1) * rows);
			pst.setInt(2, rows);
			ResultSet rs = pst.executeQuery();
			// 将查询出的每个RestaurantYR封装到一个RestaurantYR对象中并放入List
			while (rs.next()) {
				RestaurantYR restaurantYR = new RestaurantYR();
				restaurantYR.setRid(rs.getInt("rid"));
				restaurantYR.setRname(rs.getString("rname"));
				restaurantYR.setRphone(rs.getString("rphone"));
				restaurantYR.setRaddress(rs.getString("raddress"));
				restaurantYR.setUid(rs.getInt("uid"));
				restaurantYR.setWorkdate(rs.getString("workdate"));
				restaurantYR.setStar(rs.getInt("star"));
				restaurantYR.setIsopen(rs.getInt("isopen"));
				lres.add(restaurantYR);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lres;
	}

	@Override
	public int selallres() {
		int total=0;
		// SQL语句，查询餐馆总数
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT count(rid) FROM dcxt_restaurant");
		//通过工具包建立connection
		Connection  con = JDBCUtil.getConnection();
		//声明预编译器
		PreparedStatement pst=null;
		try {
			pst=con.prepareStatement(sql.toString());
			ResultSet rs=pst.executeQuery();
			if(rs.next()){
				total=rs.getInt("count(rid)");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JDBCUtil.closeJDBC(pst, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return total;
	}

}

package com.zr.dao;

import java.util.List;

import com.zr.model.TipXY;

public interface TipDaoXY {
	/**
	 * 查看举报
	 * @param page 页码
	 * @param rows 行数
	 * @return 举报对象集合
	 */
	public List<TipXY> selectTip(int page,int rows);
	/**
	 * 举报的总数
	 * @return 举报的数量
	 */
	public int countTips();
}

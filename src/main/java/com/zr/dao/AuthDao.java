package com.zr.dao;

import java.util.List;

import com.zr.model.Auth;

import net.sf.json.JSONArray;

public interface AuthDao {
		/**
		 * 根据父id查询下面的子功能
		 * @param pid
		 * @return
		 */
		public JSONArray  getAuthsByPid(int pid,String uname);
}

package com.zr.dao;

import java.sql.Date;
import java.util.List;

import com.zr.model.Dish_spd;

public interface DishDao_spd {
	/**
	 * 获得全部菜的个数
	 * @return
	 */
	public int getAllDishsNum();
	/**
	 * 获得全部菜
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<Dish_spd> getAllDishs(int page, int rows);
	/**
	 * 通过菜id获得被选中的菜的对象
	 * @param did
	 * @return
	 */
	public Dish_spd getSelectDish(int did);
	/**
	 * 获得菜的类名
	 * @return
	 */
	public List<String> getDidSname();
	/**
	 * 通过菜名获得该种类的菜
	 * @param sname
	 * @return
	 */
	public List<Dish_spd> getDishsBySname(String sname);
	/**
	 * 通过模糊搜索名获得菜信息
	 * @param searchName
	 * @return
	 */
	public List<Dish_spd> getDishsBySearchName(String searchName);
	/**
	 * 通过用户id，餐馆id，菜id，菜数量，地址,总价,下单时间生成订单
	 * @param userId
	 * @param restId
	 * @param dishId
	 * @param dishNumber
	 * @param address
	 * @param totalPrice
	 * @param addTime
	 * @return
	 */
	public boolean insertOrder(int userId, int restId, int dishId, int dishNumber, String address, int totalPrice, String addTime);
	/**
	 * 通过订单id，举报内容，日期生成一份举报
	 * @param oid
	 * @param tipText
	 * @param tipDate
	 * @return
	 */
	public boolean insertTip(int oid, String tipText, String tipDate);
}

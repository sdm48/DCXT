package com.zr.dao;

import java.sql.Date;
import java.util.List;

import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;

public interface RestaurantDao_spd {
	/**
	 * 得到餐馆总个数
	 * @return
	 */
	public int getRestaurantCount();
	/**
	 * 获得全部餐馆对象
	 * @return 餐馆集合
	 */
	public List<Restaurant_spd> getAllRestaurantMessage();
	/**
	 * 通过餐馆id获得餐馆对象
	 * @param rid
	 * @return
	 */
	public Restaurant_spd getRestaurantByRid(int rid);
	/**
	 * 通过用户id，餐馆id，时间生成一份收藏
	 * @param uid
	 * @param rid
	 * @param addTime
	 * @return
	 */
	public boolean collectRestaurant(int uid, int rid, String addTime);
	/**
	 * 通过用户id得到该用户全部订单
	 * @param uid
	 * @return
	 */
	public List<Order_spd> getOrdersByUid(int uid);
}

package com.zr.dao;
/**
 * 修改个人信息dao
 * @author Administrator
 *
 */
public interface XggrxxDao {
	public boolean isnotSameName(int uid,String cname);
public boolean changeNameByid(String cname,int uid);
public boolean changeCaddressByid(int uid,String Caddress);
public boolean changeCphoneByid(int uid,String Cphone);
public boolean changeCsexByid(int uid,String Csex);
public boolean changeUpasswordByid(int uid,String Upassword);
}

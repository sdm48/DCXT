package com.zr.dao;

import java.util.List;

import com.zr.model.RestaurantYR;
import com.zr.model.User;

/**
 * Restaurant表的Dao层
 * @author Administrator
 *
 */
public interface RestaurantDaoYR {
	
	/**
	 * 通过页码和每页多少行查询餐馆
	 * @param page 页码
	 * @param rows 每页多少行
	 * @return  餐馆的集合
	 */
	List<RestaurantYR> selresByPage(int page,int rows);

	/**
	 * 查找一共有多少个餐馆
	 * @return 餐馆总数
	 */
	int selallres();
}

package com.zr.dao;

import com.zr.model.Dish_PL;

/**
 * 菜的Dao层接口
 * @author 彭浪
 *
 */
public interface DishDao_PL {
	/**
	 * 通过传入一个菜的属性来查询一个菜的方法
	 * @param uniqueField 传入一个菜的属性
	 * @param value 传入一个菜相对于uniqueField的值
	 * @return 返回一个菜对象
	 */
	public Dish_PL selectOne(String uniqueField, Object value);
	/**
	 * 修改一个菜的方法
	 * @param d 传入一个菜的对象
	 * @return 返回修改是否成功
	 */
	public boolean updateDishPL(Dish_PL d);
	
}

package com.zr.dao;

import com.zr.model.Restaurant_PL;

/**
 * 餐馆的Dao层接口
 * @author 彭浪
 *
 */
public interface RestaurantDao_PL {
	/**
	 * 新增一个餐厅的方法
	 * @param u 传入一个餐厅对象
	 * @return 返回新增是否成功
	 */
	public boolean insertRestaurantPL(Restaurant_PL r);
	/**
	 * 修改一个餐厅的方法
	 * @param u 传入一个餐厅对象
	 * @return 返回修改是否成功
	 */
	public boolean updateRestaurantPL(Restaurant_PL r);
	/**
	 * 通过传入一个餐厅的属性来查询一个餐厅的方法
	 * @param uniqueField 传入一个餐厅的属性
	 * @param value 传入一个餐厅相对于uniqueField的值
	 * @return 返回一个餐厅对象
	 */
	public Restaurant_PL selectOne(String uniqueField, Object value);
}

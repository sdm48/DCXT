package com.zr.dao;

import java.util.List;

import com.zr.model.Order_PL;
/**
 * 订单的Dao层接口
 * @author 彭浪
 *
 */
public interface OrderDao_PL {
	/**
	 * 通过传入一个订单的属性来查询一个订单集合的方法
	 * @param uniqueField 传入一个订单的属性
	 * @param value 传入一个订单相对于uniqueField的值
	 * @param startTime 传入一个订单的起始时间
	 * @param endTime 传入一个订单的结束时间
	 * @return 返回一个订单对象集合
	 */
	public List<Order_PL> selectAll(String uniqueField, Object value,String startTime,String endTime);
	/**
	 * 通过传入一个餐厅的id来得到这个餐馆的最初的订单的起始时间(年月日)
	 * @param rid 传入一个餐厅的id
	 * @return 返回一个年月日的数组
	 */
	public String[] selectStartTime(int rid);
	/**
	 * 通过传入一个订单的属性来查询一个已接单或者未接单的订单集合的方法
	 * @param uniqueField 传入一个订单的属性
	 * @param value 传入一个订单相对于uniqueField的值
	 * @param rstate 传入商家是否接单
	 * @return 返回一个订单对象集合
	 */
	public List<Order_PL> selectAll(String uniqueField, Object value,int rstate);
	/**
	 * 修改一个订单的方法
	 * @param o 传入一个订单对象
	 * @return 返回修改是否成功
	 */
	public boolean updateOrderPL(Order_PL o);
	/**
	 * 通过传入一个订单的属性来查询一个订单的方法
	 * @param uniqueField 传入一个订单的属性
	 * @param value 传入一个订单相对于uniqueField的值
	 * @return 返回一个订单对象
	 */
	public Order_PL selectone(String uniqueField, Object value);
}

package com.zr.dao;

import java.util.List;

import com.zr.model.ApplyXY;

public interface ApplyDaoXY {
	/**
	 * 查看开店申请
	 * @param page 页码
	 * @param rows 行数
	 * @return
	 */
	public List<ApplyXY> selectApply(int page,int rows);
	/**
	 * 申请开店总数
	 * @return
	 */
	public int countApplys();
	/**
	 * 通过申请
	 * @param aid 申请id
	 */
	public void passApply(int aid,String now);
	/**
	 * 不通过请求
	 * @param aid 申请id
	 */
	public void notpassApply(int aid);
	
	
}

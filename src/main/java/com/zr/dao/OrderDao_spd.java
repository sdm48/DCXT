package com.zr.dao;

import java.util.List;

import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;

public interface OrderDao_spd {
	public List<Order_spd> getAllOrdersByUid(int uid);
	public Restaurant_spd getRestaurantByOid(int oid);
	public boolean addJudgeTextAndTime(String judgeText, String addTime, int judgeNum, int oid);
}

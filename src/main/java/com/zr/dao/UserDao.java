
package com.zr.dao;

import com.zr.model.Customer;


public interface UserDao {
		/**
		 * 查找用户，通过名字
		 * @param uname
		 * @return
		 */
		public  boolean   findUserByName(String uname,String upswd);
		
		/**
		 * 通过用户名查找uid
		 * @param uname 用户名
		 * @return
		 */
		public  int findUidByUname(String uname);
		
		public Customer getCubyName(String uname);
}


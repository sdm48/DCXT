package com.zr.dao;

import java.util.List;

import com.zr.model.RestaurantXY;

public interface RestaurantDaoXY {
	/**
	 * 查询餐馆通过餐馆名
	 * @param rname 餐馆名
	 * @return 餐馆信息
	 */
	public RestaurantXY findRestaurantByRid(int rid);
	
	/**
	 * 删除餐馆通过餐馆名
	 * @param rid 餐馆id
	 */
	public int deleteRestaurantByRid(int rid);
	/**
	 * 查看被禁用的餐馆
	 * @param page 页码
	 * @param rows 条数
	 * @return
	 */
	public List<RestaurantXY> selectRestaurantJinyong(int page,int rows);
	/**
	 * 查看被禁用餐馆的数量
	 * @return
	 */
	public int countRests();
	/**
	 * 解封餐馆
	 */
	public void jiefengRest(int rid);
}

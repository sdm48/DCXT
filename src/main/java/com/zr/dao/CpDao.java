package com.zr.dao;

import java.util.List;

import com.zr.model.Cp_xxd;

public interface CpDao{

	/**
	 * 根据用户ID对应下的餐馆的ID查出对应的菜品信息
	 * @param uid 用户ID
	 * @param page 
	 * @param rows
	 * @return 菜品的集合
	 */
	public List<Cp_xxd> getCpByUid(int uid,int page,int rows);
	
	/**
	 *	根据用户ID对应下的餐馆的ID获取菜品的数量
	 * @param uid 用户id
	 * @return 菜品的数量
	 */
	public int getCpsCountByUid(int uid);
	
	/**
	 * 新增菜品
	 * @param dname 新增菜品的名字
	 * @param dnumber 新增菜品的数量
	 * @param sname 新增菜品的种类
	 * @param rid 新增菜品的餐馆id
	 * @param rprice 新增菜品的价格
	 * @param isopen 新增菜品能否被选中
	 * @return 菜品信息
	 */
	public Cp_xxd insertCp(String dname,int dnumber,String sname,int rid,int rprice,int isopen);

	/**
	 * 通过菜品ID删除菜品
	 *  @param did 通过菜品ID
	 * @return 布尔值
	 */
	public boolean delCpBydid(int did);
	
	/**
	 * 通过菜品ID更新菜品信息
	 * @param did 菜品id
	 * @param dname 菜品名字
	 * @param dnumber 菜品数量
	 * @param sname 菜品种类
	 * @param rid 餐厅id
	 * @param rprice 菜品价格
	 * @param isopen 菜品能否被选中
	 * @return 布尔值
	 */
	public boolean upCp(int did,String dname,int dnumber,String sname,int rid,int rprice,int isopen);
}



package com.zr.dao;

import com.zr.model.User_PL;

/**
 * 用户的Dao层接口
 * @author 彭浪
 *
 */
public interface UserDao_PL {
	/**
	 * 通过传入一个用户的属性来查询一个用户的方法
	 * @param uniqueField 传入一个用户的属性
	 * @param value 传入一个用户相对于uniqueField的值
	 * @return 返回一个用户对象
	 */
	public User_PL selectOne(String uniqueField, Object value);
}

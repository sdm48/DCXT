package com.zr.dao;

import java.util.List;

import com.zr.model.User;

/**
 * Login表的Dao层
 * @author YR
 *
 */
public interface LoginDaoYR {
	/**
	 * 通过页码和每页多少行查询顾客
	 * @param page 页码
	 * @param rows 每页多少行
	 * @return  顾客的集合
	 */
	List<User> selcusByPage(int page,int rows);
	
	/**
	 * 查找一共有多少个注册顾客
	 * @return 顾客总数
	 */
	int selallcus();
	
	/**
	 * 通过页码和每页多少行查询老板
	 * @param page 页码
	 * @param rows 每页多少行
	 * @return  老板的集合
	 */
	List<User> selbossByPage(int page,int rows);
	

	/**
	 * 查找一共有多少个注册老板
	 * @return 老板总数
	 */
	int selallboss();
	
	/**
	 * 通过id查找用户
	 * @param uid 用户id
	 * @return User对象
	 */
	User selclouserByUid(int uid);
	
	/**
	 * 
	 * 通过id封禁某个账户
	 * @param uid  用户id
	 *
	 */
	void clouserByUid(int uid);
	
	/**
	 * 通过uname获取uid
	 * @param uname 用户登录名uname
	 * @return 用户uid
	 */
	int getUidByUname(String uname);
	
	/**
	 * 通过uid，urole解封账户
	 * @param uid 解封账户的uid
	 * @param urole 被封账户被封之前的urole
	 */
	void openuserByUidUrole(int uid,int urole);

}

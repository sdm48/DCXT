package com.zr.dao;

import com.zr.model.Customer;
/**
 * 
 * @author 李昱
 * 通过用户名查询个人信息的dao
 * @return
 */
public interface CkgrxxDao{
	public Customer getCustomerbyCname(String uname);
	
}

package com.zr.dao;

import java.util.List;

import com.zr.model.ClosedYR;
import com.zr.model.User;

/**
 * Closed表的Dao层接口
 * @author YR
 *
 */
public interface ClosedDaoYR {
	/**
	 * 执行封号操作后将操作记录在案
	 * @param mid 操作管理员的id
	 * @param uid 被封账户的id
	 * @param urole  被封账户被封之前的urole
	 * @param cdate 封号操作的时间
	 * @param ccontent 封号的原因
	 */
	void recordclo(int mid, int uid, int urole, String cdate, String ccontent);
    
	/**
	 * 通过页码和每页多少行查询已封用户记录
	 * @param page 页码
	 * @param rows 每页多少行
	 * @return  已封用户记录的集合
	 */
	List<ClosedYR> selclouserByPage(int page,int rows);
	
	/**
	 * 查询被封账户记录的总数
	 * @return
	 */
	int selallclouser();
	
	
	/**
	 * 通过uid来清除closed表中的某用户被封的记录
	 * @param uid 解封用户的uid
	 */
	void clearrecordclo(int uid);

}

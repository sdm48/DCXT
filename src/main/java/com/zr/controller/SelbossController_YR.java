package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.User;
import com.zr.service.SelbossServiceYR;
import com.zr.service.SelcusServiceYR;
import com.zr.serviceimp.SelallbossServiceImplYR;
import com.zr.serviceimp.SelallcusServiceImplYR;
import com.zr.serviceimp.SelbossServiceImplYR;
import com.zr.serviceimp.SelcusServiceImplYR;

import net.sf.json.JSONObject;

public class SelbossController_YR extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		//获取page属性
		int page=Integer.parseInt(req.getParameter("page"));
		//获取rows属性
		int rows=Integer.parseInt(req.getParameter("rows"));
		// 新建一个查找老板的服务
		SelbossServiceYR selbossServiceYR=new SelbossServiceImplYR();
		//新建一个User集合
		List<User> lisu=new ArrayList<User>();
		//通过服务查找老板
		lisu=selbossServiceYR.selbossByPage(page, rows);
		//新建一个查找老板总数的服务
		SelallbossServiceImplYR selallbossServiceImplYR=new SelallbossServiceImplYR();
	    //获得顾客总数
		int total=selallbossServiceImplYR.selallboss();
		//获取PrintWriter
		PrintWriter pw=resp.getWriter();
		//新建一个JSONObject对象
		JSONObject j=new JSONObject();
		//拼凑一个json对象格式以满足esayui数据表的需求
		j.put("total", total);
		j.put("rows", lisu);
		//回写j的格式
		pw.write(j.toString());
		pw.flush();
		pw.close();
	}

}

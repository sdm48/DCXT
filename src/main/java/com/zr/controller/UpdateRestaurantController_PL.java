package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.Restaurant_PL;
import com.zr.model.User_PL;
import com.zr.service.RestaurantService_PL;
import com.zr.service.UserService_PL;
import com.zr.serviceimp.RestaurantServiceImpl_PL;
import com.zr.serviceimp.UserServiceImpl_PL;
import com.zr.util.ThisSystemExceptionUtil;

import net.sf.json.JSONObject;

/**
 * 修改餐馆的控制器
 * @author 彭浪
 *
 */
public class UpdateRestaurantController_PL extends HttpServlet{
	//创建一个餐馆的服务对象
	RestaurantService_PL rs=new RestaurantServiceImpl_PL();
	//创建一个用户的服务对象
	UserService_PL us=new UserServiceImpl_PL();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//设置字符集
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("application/json; charset=utf-8");
		
		PrintWriter pw= resp.getWriter();
		JSONObject  jo = new JSONObject();
		try{
			//得到用户输入的餐馆名字
			String rname=req.getParameter("rname");
			//得到用户输入的餐馆联系电话
			String rphone=req.getParameter("rphone");
			//得到用户输入的餐馆详细地址
			String raddress=req.getParameter("raddress");
			//获取餐馆老板的名字
			String uname=(String) req.getSession().getAttribute("uname");
			//获取到用户(餐馆老板)的对象
			User_PL u=us.getUserPLByUname(uname);
			//得到餐馆的对象
			Restaurant_PL r=rs.getRestaurantPLByUid(u.getUid());
			
			//保存餐馆对象
			jo.put("RestaurantPL",r);
			
			//修改参数
			r.setRname(rname);
			r.setRphone(rphone);
			r.setRaddress(raddress);	
			//调用餐馆的服务方法进行更新
			boolean flag=rs.modifyRestaurantPL(r);
			if(flag){
				jo.put("message", "更新餐馆成功");
			}else{
				jo.put("message", "更新餐馆失败");
			}
			
		}catch (ThisSystemExceptionUtil e) {
			jo.put("message",e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			jo.put("message","网络繁忙请稍后再试!");
		}
		pw.write(jo.toString());
	}
}

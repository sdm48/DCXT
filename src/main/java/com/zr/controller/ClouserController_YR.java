package com.zr.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zr.serviceimp.ClouserServiceImplYR;
import com.zr.serviceimp.GetUidByUnameServiceImplYR;


/**
 * 管理员点确定，执行封禁账户操作
 * @author YR
 *
 */
public class ClouserController_YR extends  HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		//获取session对象
		HttpSession session=req.getSession();
		//获取登录进来的这位管理员的登录名uname
		String uname=session.getAttribute("uname").toString();
		//新建一个GetUidByUnameServiceImplYR对象
		GetUidByUnameServiceImplYR getUidByUnameServiceImplYR=new GetUidByUnameServiceImplYR();
		//通过管理员的uname获得uid
		int uid =getUidByUnameServiceImplYR.getUidByUname(uname);
		//获取将封账户的uid
		int clouid= Integer.parseInt(req.getParameter("clouid"));
		//获取将封账户封号之前的urole
		int clourole= Integer.parseInt(req.getParameter("clourole"));
		//获取封号的原因
		String cloreason=req.getParameter("cloreason");
		//获取当前的时间(何时执行封号操作)
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		String curtime=df.format(new Date());// new Date()为获取当前系统时间
		//新建一个ClouserServiceImplYR对象
		ClouserServiceImplYR clouserServiceImplYR=new ClouserServiceImplYR();
	    //调用ClouserServiceImplYR里的封禁账户方法
		clouserServiceImplYR.coluser(clouid);
	    //调用ClouserServiceImplYR里的记录封号行为方法
		clouserServiceImplYR.recordclo(uid, clouid, clourole, curtime, cloreason);
	}
     

}

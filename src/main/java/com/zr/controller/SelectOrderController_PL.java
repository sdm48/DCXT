package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.Dish_PL;
import com.zr.model.Order_PL;
import com.zr.model.Restaurant_PL;
import com.zr.model.User_PL;
import com.zr.service.DishService_PL;
import com.zr.service.OrderService_PL;
import com.zr.service.RestaurantService_PL;
import com.zr.service.UserService_PL;
import com.zr.serviceimp.DishServiceImpl_PL;
import com.zr.serviceimp.OrderServiceImpl_PL;
import com.zr.serviceimp.RestaurantServiceImpl_PL;
import com.zr.serviceimp.UserServiceImpl_PL;
import com.zr.util.ThisSystemExceptionUtil;

import net.sf.json.JSONObject;

/**
 * 查看已接单订单的控制器
 * @author 彭浪
 *
 */
public class SelectOrderController_PL extends HttpServlet{
	//创建一个餐馆订单的服务对象
	OrderService_PL os=new OrderServiceImpl_PL();
	//创建一个餐馆菜的服务对象
	DishService_PL ds=new DishServiceImpl_PL();
	//创建一个餐馆的服务对象
	RestaurantService_PL rs=new RestaurantServiceImpl_PL();
	//创建一个用户的服务对象
	UserService_PL us=new UserServiceImpl_PL();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//设置字符集
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("application/json; charset=utf-8");
		
		PrintWriter pw= resp.getWriter();
		JSONObject  jo = new JSONObject();
		try{
			//获取餐馆老板的名字
			String uname=(String) req.getSession().getAttribute("uname");
			//获取到用户(餐馆老板)的对象
			User_PL u=us.getUserPLByUname(uname);
			
			//得到餐馆的对象
			Restaurant_PL r=rs.getRestaurantPLByUid(u.getUid());
			List<Order_PL> order=new ArrayList<Order_PL>();
			//得到已接单的订单集合
			order=os.getAcceptOrderPLByRid(r.getRid());
			//得到选中的订单id
			String orderId=req.getParameter("orderId");
			//得到输入的回复信息
			String reply=req.getParameter("reply");
			if(orderId!=null){
				if(os.replyOrderPL(orderId,reply)){
					jo.put("message", "提交成功");
				}else{
					jo.put("message", "提交失败");
				}
			}
			
			
			List<List<String>> dishsName=new ArrayList<List<String>>();
			List<User_PL> users=new ArrayList<User_PL>();
			for (int i = 0; i < order.size(); i++) {
				//获取每个订单里的所有菜对象
				List<Dish_PL> dish=ds.getDishPLByDid(order.get(i).getDid());
				//获取每个订单里的用户对象
				User_PL user=us.getUserPLByUid(order.get(i).getUid());
				users.add(user);
				//得到每个订单里的菜品名
				List<String> dishs=new ArrayList<String>();
				for (int j = 0; j < dish.size(); j++) {
					dishs.add(dish.get(j).getDname());
				}
				dishsName.add(dishs);
				
			}
			
			jo.put("dishsName", dishsName);
			jo.put("users", users);
			jo.put("order", order);
			
		}catch (ThisSystemExceptionUtil e) {
			jo.put("message",e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			jo.put("message","网络繁忙请稍后再试!");
		}
		pw.write(jo.toString());
	}
}

package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.Dish_PL;
import com.zr.model.Order_PL;
import com.zr.model.Restaurant_PL;
import com.zr.model.User_PL;
import com.zr.service.DishService_PL;
import com.zr.service.OrderService_PL;
import com.zr.service.RestaurantService_PL;
import com.zr.service.UserService_PL;
import com.zr.serviceimp.DishServiceImpl_PL;
import com.zr.serviceimp.OrderServiceImpl_PL;
import com.zr.serviceimp.RestaurantServiceImpl_PL;
import com.zr.serviceimp.UserServiceImpl_PL;
import com.zr.util.ThisSystemExceptionUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 餐馆订单的统计报表
 * @author 彭浪
 *
 */
public class SelectCountRestaurantOrderController_PL extends HttpServlet{
	//创建一个餐馆订单的服务对象
	OrderService_PL os=new OrderServiceImpl_PL();
	//创建一个餐馆菜的服务对象
	DishService_PL ds=new DishServiceImpl_PL();
	//创建一个餐馆的服务对象
	RestaurantService_PL rs=new RestaurantServiceImpl_PL();
	//创建一个用户的服务对象
	UserService_PL us=new UserServiceImpl_PL();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//设置字符集
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("application/json; charset=utf-8");
		
		PrintWriter pw= resp.getWriter();
		JSONObject  jo = new JSONObject();
		try{
			//获取用户选择的年
			String select=req.getParameter("select");
			//获取用户选择的年
			String year=req.getParameter("year");
			//获取用户选择的月
			String month=req.getParameter("month");
			//获取用户选择的日
			String day=req.getParameter("day");
			//获取用户选择的周
			String week=req.getParameter("week");
			
		//代码逻辑放入前端网页处理，后台服务器不再处理。
//			String year=null;
//			String month=null;
//			String day=null;
//			String week=null;
//			String reg = "[\u4e00-\u9fa5]";
//			Pattern pat = Pattern.compile(reg); 
//			//通过正则表达式去除汉字
//			if(years!=null){
//				Matcher mat=pat.matcher(years);
//				year=mat.replaceAll("");
//			}else if(months!=null){
//				Matcher mat=pat.matcher(months);
//				month=mat.replaceAll("");
//			}else if(days!=null){
//				Matcher mat=pat.matcher(days);
//				day=mat.replaceAll("");
//			}else if(weeks!=null){
//				Matcher mat=pat.matcher(weeks);
//				week=mat.replaceAll("");
//			}
			
	    	
			//获取餐馆老板的名字
			String uname=(String) req.getSession().getAttribute("uname");
			//获取到用户(餐馆老板)的对象
			User_PL u=us.getUserPLByUname(uname);
			//得到餐馆的对象
			Restaurant_PL r=rs.getRestaurantPLByUid(u.getUid());
			//调用餐馆订单的服务对象的方法来得到订单最初时间
			String date[]=os.selectStartTime(r.getRid());
			//把从数据库之中获取的最早的年月日分别传入json对象
			jo.put("year", date[0]);
			jo.put("month", date[1]);
			jo.put("day", date[2]);
			
			//调用餐馆订单的服务对象的方法来得到这个餐馆的所有订单
			String startTime=null;
			String endTime=null;
			List<Order_PL> order=new ArrayList<Order_PL>();
			List<String> weeks=new ArrayList<String>();
			if(year!=null&&month==null&&day==null){
				startTime=year+"-1-1";
				endTime=year+"-12-31";
				order=os.getOrderPLByRid(r.getRid(), startTime, endTime);
				
			}else if(year!=null&&month!=null&&day==null){
				startTime=year+"-"+month+"-1";
				endTime=year+"-"+month+"-31";
				
				if("按每周查询".equals(select)){
					weeks=os.getWeeks(startTime, endTime);
					
					if(week!=null){
						//每周的日期起始与结束
						startTime=weeks.get(Integer.parseInt(week)-1);
						endTime=weeks.get(Integer.parseInt(week));
						
						order=os.getOrderPLByRid(r.getRid(), startTime, endTime);
					}else{
						
					}
				}else{
					order=os.getOrderPLByRid(r.getRid(), startTime, endTime);
				}
			}else if(year!=null&&month!=null&&day!=null){
				startTime=year+"-"+month+"-"+day;
				endTime=year+"-"+month+"-"+(Integer.parseInt(day)+1);
				order=os.getOrderPLByRid(r.getRid(), startTime, endTime);
				
			}
			List<List<String>> dishsName=new ArrayList<List<String>>();
			List<User_PL> users=new ArrayList<User_PL>();
			for (int i = 0; i < order.size(); i++) {
				//获取每个订单里的所有菜对象
				List<Dish_PL> dish=ds.getDishPLByDid(order.get(i).getDid());
				//获取每个订单里的用户对象
				User_PL user=us.getUserPLByUid(order.get(i).getUid());
				users.add(user);
				//得到每个订单里的菜品名
				List<String> dishs=new ArrayList<String>();
				for (int j = 0; j < dish.size(); j++) {
					dishs.add(dish.get(j).getDname());
				}
				dishsName.add(dishs);
				
			}
			//总销售额
			int total=0;
			for (int i = 0; i < order.size(); i++) {
				total+=order.get(i).getTotal();
			}
			
			jo.put("weekCount", (weeks.size()-1));
			jo.put("dishsName", dishsName);
			jo.put("users", users);
			jo.put("order", order);
			jo.put("total", total);
		}catch (ThisSystemExceptionUtil e) {
			jo.put("message",e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			jo.put("message","网络繁忙请稍后再试!");
		}
		pw.write(jo.toString());
	}
}

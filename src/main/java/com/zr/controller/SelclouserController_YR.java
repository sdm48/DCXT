package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zr.model.User;
import com.zr.serviceimp.SelclouserServiceImplYR;
import com.zr.serviceimp.SelclouserServiceImplYRR;

import net.sf.json.JSONObject;

/**
 * 管理员查询将要封禁的用户，查询出来的esayui表也发送这个请求来动态加载数据
 * @author YR
 *
 */

public class SelclouserController_YR extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		
		//获取一个session对象
		HttpSession session=req.getSession();
		//如果请求有发送uid这个参数，则在session中添加“clouserid”这个属性（esayui表发送的请求不带任何参数过来）
		if(req.getParameter("uid")!=null){
		   session.setAttribute("clouserid", Integer.parseInt(req.getParameter("uid")));
		}
		//新建一个SelclouserServiceImplYR对象
		//SelclouserServiceImplYR selclouserServiceImplYR=new SelclouserServiceImplYR();
		SelclouserServiceImplYRR selclouserServiceImplYRR=new SelclouserServiceImplYRR();
		//通过id获得将封的User
		//User user=selclouserServiceImplYR.selclouserByUid(Integer.parseInt(session.getAttribute("clouserid").toString()));
		User user=selclouserServiceImplYRR.selclouserByUid(Integer.parseInt(session.getAttribute("clouserid").toString()));
		//新建一个jsonobject对象
		JSONObject jl=new JSONObject();
		//新建一个User集合
		List<User> lisu=new ArrayList<User>();
		//如果获得的User存在于数据库中，则将它加入lisu，并拼凑j1对象满足esayui表的需求
		if(user.getUid()!=null){
		  lisu.add(user);
		  jl.put("total",1);
		  jl.put("rows", lisu);
		}
		//获取一个PrintWriter对象
		PrintWriter pw=resp.getWriter();
		//将j1回写到前端
		pw.write(jl.toString());
		pw.flush();
		pw.close();
		
	}

}

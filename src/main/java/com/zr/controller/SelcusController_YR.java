package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.User;
import com.zr.service.SelcusServiceYR;
import com.zr.serviceimp.SelallcusServiceImplYR;
import com.zr.serviceimp.SelcusServiceImplYR;

import net.sf.json.JSONObject;
/**
 * 管理员查询所有已注册顾客的Controller
 * @author YR
 *
 */
public class SelcusController_YR extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		//获取page属性
		int page=Integer.parseInt(req.getParameter("page"));
		//获取rows属性
		int rows=Integer.parseInt(req.getParameter("rows"));
		// 新建一个查找顾客的服务
		SelcusServiceYR selcusServiceYR=new SelcusServiceImplYR();
		//新建一个User集合
		List<User> lisu=new ArrayList<User>();
		//通过服务查找顾客
		lisu=selcusServiceYR.selcusByPage(page, rows);
		//新建一个查找顾客总数的服务
		SelallcusServiceImplYR selallcusServiceImplYR=new SelallcusServiceImplYR();
	    //获得顾客总数
		int total=selallcusServiceImplYR.selallcus();
		//获取PrintWriter
		PrintWriter pw=resp.getWriter();
		//新建一个JSONObject对象
		JSONObject j=new JSONObject();
		//拼凑一个json对象格式以满足esayui数据表的需求
		j.put("total", total);
		j.put("rows", lisu);
		//回写j的格式
		pw.write(j.toString());
		pw.flush();
		pw.close();
	}

}

package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.RestaurantXY;
import com.zr.service.RestaurantServiceXY;
import com.zr.serviceimp.RestaurantServiceImplXY;

import net.sf.json.JSONObject;

public class SelectrestjinyongControllerXY extends HttpServlet{
	RestaurantServiceXY rs = new RestaurantServiceImplXY();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		//获取页面页数，行数
		int page = Integer.parseInt(req.getParameter("page"));
		int rows = Integer.parseInt(req.getParameter("rows"));
		//被禁用餐馆的集合
		List<RestaurantXY> rests = rs.selectRestsJinyong(page, rows);
		//被禁用餐馆的数量
		int count = rs.countRests();
		//将被禁用餐馆总数，被禁用餐馆对象集合放到json
		JSONObject json = new JSONObject();
		json.put("total", count);
		json.put("rows", rests);
		PrintWriter pw = resp.getWriter();
		pw.write(json.toString());
		
		
	}
}

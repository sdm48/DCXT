
package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.zr.service.LlcgxxService;
import com.zr.serviceimp.LlcgxxServiceimpl;

/**
 * 浏览餐厅信息
 * @author 柏小毛
 *
 */
public class LlctxxController extends HttpServlet {
	LlcgxxService ls=new LlcgxxServiceimpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
	// TODO Auto-generated method stub
	JSONObject jb=new JSONObject();
	req.setCharacterEncoding("utf-8");	
	resp.setCharacterEncoding("utf-8");
	int page = Integer.parseInt(req.getParameter("page"));
	int rows = Integer.parseInt(req.getParameter("rows"));
	JSONArray ja=ls.getAllRes(page,rows);
	int total=ls.getAllnub();
	jb.put("total", total);
	jb.put("rows", ja);
	PrintWriter  pw = resp.getWriter();
	 pw.write(jb.toString());
		pw.flush();
		pw.close();
	}
}


package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.service.RestaurantServiceXY;
import com.zr.serviceimp.RestaurantServiceImplXY;

public class DeletearestControllerXY extends HttpServlet{
	RestaurantServiceXY rs = new RestaurantServiceImplXY();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		//拿到餐馆id
		int rid = Integer.parseInt(req.getParameter("rid"));
		//餐馆营业状态
		int isopen = rs.removeRestaurantByRid(rid);
		PrintWriter pw = resp.getWriter();
		pw.write(String.valueOf(isopen));
	}
}

package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.zr.model.Customer;
import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;
import com.zr.service.OrderService_spd;
import com.zr.serviceimp.OrderServiceImpl_spd;

public class OrderController_spd extends HttpServlet{
	OrderService_spd os = new OrderServiceImpl_spd();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		PrintWriter pw = resp.getWriter();
		JSONObject j = new JSONObject();
		JSONArray ja = new JSONArray();
		HttpSession session = req.getSession();
		Customer customer = (Customer) session.getAttribute("customerly");
		int uid = 0;
		if(customer != null) {
			//获得当前用户id
			uid = customer.getUid();
		}
		List<Order_spd> orders = os.getAllOrdersByUid(uid);
		for (int i=0; i<orders.size(); i++) {
			int oid = orders.get(i).getOid();
			Restaurant_spd restaurant = os.getRestaurantByOid(oid);
			String rName = restaurant.getrName();
			j.put("oid", oid);
			j.put("rName", rName);
			j.put("uid", orders.get(i).getUid());
			j.put("rid", orders.get(i).getRid());
			j.put("did", orders.get(i).getDid());
			j.put("onumber", orders.get(i).getoNumber());
			j.put("uaddress", orders.get(i).getuAddress());
			j.put("rstate", orders.get(i).getrState());
			j.put("ustate", orders.get(i).getuState());
			j.put("judge", orders.get(i).getJudge());
			j.put("reply", orders.get(i).getReply());
			j.put("total", orders.get(i).getTotal());
			j.put("odate", orders.get(i).getoDate());
			j.put("judgedate", orders.get(i).getJudgeDate());
			j.put("judgenum", orders.get(i).getJudgeNum());
			//获得下拉列表的取值
			String selectNum = req.getParameter("selectNum");
			//获得评价内容
			String judgeContent = req.getParameter("judgeContent");
			//得到订单oid
			String orderId = req.getParameter("oid");
			if(judgeContent!=null && orderId!=null && selectNum!=null) {
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String addTime = sdf.format(date);
				boolean flag = os.addJudgeTextAndTime(judgeContent, addTime, Integer.parseInt(selectNum), Integer.parseInt(orderId));
				if(flag) {
					j.put("isJudge", 1);
					ja.add(j);
					break;
				} else {
					j.put("isJudge", 0);
					ja.add(j);
					break;
				}
			}
			ja.add(j);
		}
		pw.write(ja.toString());
	}
}

package com.zr.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.serviceimp.ClouserServiceImplYR;

/**
 * 解封账户的controller
 * @author YR
 *
 */
public class OpenuserController_YR extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		//获取要解封账户的uid
		int openuid=Integer.parseInt(req.getParameter("openuid"));
	    //获取要解封账户被封之前的urole
		int urole=Integer.parseInt(req.getParameter("urole"));
        //新建一个ClouserServiceImplYR对象
		ClouserServiceImplYR clouserServiceImplYR=new ClouserServiceImplYR();
		//调用clouserServiceImplYR的解封账户方法来解封这个用户
		clouserServiceImplYR.openuser(openuid, urole);
		//调用clouserServiceImplYR的清除封号记录方法来清除closed表中此用户的封号记录
		clouserServiceImplYR.clearrecordclo(openuid);
	}

}

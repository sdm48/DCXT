package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import com.zr.model.Dish_spd;
import com.zr.service.DishServiceImpl_spd;
import com.zr.service.DishService_spd;

/**
 *根据登录用户角色得到他的功能展示
 * @author 柏小毛
 *
 */
public class SearchController_spd extends HttpServlet{
	DishService_spd ds = new DishServiceImpl_spd();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		PrintWriter pw = resp.getWriter();
		List<String> snames = ds.getDidSname();
		JSONObject j = new JSONObject();
		j.put("snames", snames);
		String sname = req.getParameter("sname");
		if(sname != null) {
			//获得不同菜类的全部菜
			List<Dish_spd> dishsBySname = ds.getDishsBySname(sname);
			//JSONObject jd = new JSONObject();
			j.put("dishsBySname", dishsBySname);
		}
		String searchName = req.getParameter("searchName");
		if(searchName != null) {
			List<Dish_spd> searchDishs = ds.getDishsBySearchName(searchName);
			for (int i=0; i<searchDishs.size(); i++) {
				j.put("searchDishs", searchDishs);
			}
		}
		pw.write(j.toString());
		pw.flush();
	}
}

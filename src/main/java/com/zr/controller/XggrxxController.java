package com.zr.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import com.zr.model.Customer;
import com.zr.service.XggrxxService;
import com.zr.serviceimp.XggrxxServiceiimpl;

/**
 * 修改个人信息
 * @author 柏小毛
 *
 */

public class XggrxxController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		HttpSession session = req.getSession();
		XggrxxService xs = new XggrxxServiceiimpl();
		Customer cs = (Customer) session.getAttribute("customerly");
		int uid = cs.getUid();
		JSONObject json = new JSONObject();
		String Cname = req.getParameter("Cname");
		String Caddress = req.getParameter("Caddress");
		String Cphone1 = req.getParameter("Cphone");
		int Cphone2 = 0;
		try {
		     Cphone2=Integer.valueOf(Cphone1);//把字符串强制转换为数字
			 //如果是数字，返回True
			} catch (Exception e) { 
			   //如果抛出异常，返回False
			}
		String Cphone=Integer.toString(Cphone2);
		String Csex = req.getParameter("Csex");
		String Upassword = req.getParameter("Upassword");
		boolean isnot = xs.isnotSameName(uid, Cname);
		if (isnot) {
			boolean cn = xs.changeNameByid(uid, Cname);
			boolean ca = xs.changeCaddressByid(uid, Caddress);
			boolean cp = xs.changeCphoneByid(uid, Cphone);
			boolean cse = xs.changeCsexByid(uid, Csex);
			boolean up = xs.changeUpasswordByid(uid, Upassword);
			StringBuffer sb = new StringBuffer("您修改了");
			if (cn) {
				sb.append("用户名,");
			}
			if (ca) {
				sb.append("地址,");
			}
			if (cp) {
				sb.append("电话号码,");
			}
			if (cse) {
				sb.append("性别,");
			}
			if (up) {
				sb.append("密码");
			}
			session.setAttribute("message", sb);
			req.getRequestDispatcher("xgcg.jsp").forward(req, resp);
		} else {
			req.getRequestDispatcher("xgsb.jsp").forward(req, resp);
			session.setAttribute("message1", "名字是重复的不得行");
		}
	}
}

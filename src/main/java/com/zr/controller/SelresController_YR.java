package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.RestaurantYR;
import com.zr.service.SelcusServiceYR;
import com.zr.serviceimp.SelcusServiceImplYR;
import com.zr.serviceimp.SelresServiceImplYR;

import net.sf.json.JSONObject;

/**
 * 管理员查询所有餐馆信息的controller
 * 
 * @author YR
 *
 */
public class SelresController_YR extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		// 获取page属性
		int page = Integer.parseInt(req.getParameter("page"));
		// 获取rows属性
		int rows = Integer.parseInt(req.getParameter("rows"));
		// 新建一个查找餐馆的服务
		SelresServiceImplYR selresServiceImplYR = new SelresServiceImplYR();
		//通过服务查找餐馆
		List<RestaurantYR> lres=selresServiceImplYR.selresByPage(page, rows);
		//获得餐馆总数
		int total=selresServiceImplYR.selallrestaurant();
		//获取PrintWriter
		PrintWriter pw = resp.getWriter();
		// 新建一个JSONObject对象
		JSONObject j = new JSONObject();
		// 拼凑一个json对象格式以满足esayui数据表的需求
		j.put("total", total);
		j.put("rows", lres);
		// 回写j的格式
		pw.write(j.toString());
		pw.flush();
		pw.close();
	}

}

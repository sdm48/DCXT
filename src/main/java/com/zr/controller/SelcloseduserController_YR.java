package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.ClosedYR;
import com.zr.serviceimp.SelclouserServiceImplYRR;

import net.sf.json.JSONObject;

/**
 * 查询所有被封账户的Controller
 * @author YR
 *
 */
public class SelcloseduserController_YR extends HttpServlet {

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		//获取页码
		int page=Integer.parseInt(req.getParameter("page"));
		//获取行数rows
		int rows=Integer.parseInt(req.getParameter("rows"));
		// 新建SelclouserServiceImplYRR对象
		SelclouserServiceImplYRR selclouserServiceImplYRR=new SelclouserServiceImplYRR();
		//获取被封账户的总数
		int total=selclouserServiceImplYRR.selallclouser();
	    //获取当前页所有被封账户记录的集合
		 List<ClosedYR> liclosed=selclouserServiceImplYRR.selcloseduserByPage(page, rows);
		//获取PrintWriter
		PrintWriter pw = resp.getWriter();
		// 新建一个JSONObject对象
		JSONObject j = new JSONObject();
		// 拼凑一个json对象格式以满足esayui数据表的需求
		j.put("total", total);
		j.put("rows", liclosed);
		// 回写j的格式
		pw.write(j.toString());
		pw.flush();
		pw.close();
	}
}

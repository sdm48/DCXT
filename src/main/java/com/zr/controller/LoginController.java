package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import com.zr.model.Customer;
import com.zr.model.Restaurant_spd;
import com.zr.service.LoginService;
import com.zr.service.RestaurantService_spd;
import com.zr.service.ShowService;
import com.zr.serviceimp.LoginServiceImpl;
import com.zr.serviceimp.RestaurantServiceImpl_spd;
import com.zr.serviceimp.ShowServiceImpl;

public class LoginController extends HttpServlet {
	LoginService ls = new LoginServiceImpl();
	RestaurantService_spd rs = new RestaurantServiceImpl_spd();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ShowService ss = new ShowServiceImpl();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject j = new JSONObject();
		PrintWriter pw = resp.getWriter();
		req.setCharacterEncoding("utf8");
		String uname = req.getParameter("uname");
		String upsdw = req.getParameter("upswd");
		String uyanzheng = req.getParameter("uyanzheng");
		HttpSession session = req.getSession();
		//得到测试的验证码
		String code = (String) session.getAttribute("code");
		List<Restaurant_spd> rests = rs.getAllRestaurantMessage();
		session.setAttribute("rests", rests);
		Customer cs = ls.getCubyName(uname);
		session.setAttribute("customerly", cs);
		boolean flag = ls.validateUser(uname, upsdw);
		if (flag && (code.equals(uyanzheng))) {
			j.put("code", 1);
			session.setAttribute("uname", uname);
		} else if (flag==true && code.equals(uyanzheng)==false){
			j.put("code", 2);
		} else {
			j.put("code", 3);
		}
		pw.write(j.toString());
	}
}


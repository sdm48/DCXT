package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.TipXY;
import com.zr.service.TipServiceXY;
import com.zr.serviceimp.TipServiceImplXY;

import net.sf.json.JSONObject;

public class SelecttipControllerXY extends HttpServlet{
	TipServiceXY ts = new TipServiceImplXY();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		//获取页面页数，行数
		int page = Integer.parseInt(req.getParameter("page"));
		int rows = Integer.parseInt(req.getParameter("rows"));
		//举报的集合
		List<TipXY> tips = ts.selectTips(page, rows);
		//举报总数
		int count = ts.countTips();
		//将举报总数，举报对象集合放到json
		JSONObject json = new JSONObject();
		json.put("total", count);
		json.put("rows", tips);
		PrintWriter pw = resp.getWriter();
		pw.write(json.toString());
	}
}

package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.zr.model.Customer;
import com.zr.model.Dish_spd;
import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;
import com.zr.service.DishServiceImpl_spd;
import com.zr.service.DishService_spd;
import com.zr.service.RestaurantService_spd;
import com.zr.serviceimp.RestaurantServiceImpl_spd;

public class TipController_spd extends HttpServlet{
	RestaurantService_spd rs = new RestaurantServiceImpl_spd();
	DishService_spd ds = new DishServiceImpl_spd();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		PrintWriter pw = resp.getWriter();
		JSONObject j = new JSONObject();
		JSONArray ja = new JSONArray();
		String page = req.getParameter("page");
		String rows = req.getParameter("rows");
		HttpSession session = req.getSession();
		int uid = 0;
		//获得当前顾客
		Customer customer = (Customer) session.getAttribute("customerly");
		if(customer != null) {
			//获得用户id
			uid = customer.getUid();
		}
		//通过用户id获得该用户的所有订单
		List<Order_spd> orders = rs.getOrdersByUid(uid);
		for (int i=0; i<orders.size(); i++) {
			//获得订单id
			int oid = orders.get(i).getOid();
			//获得订单里的菜id
			int did = orders.get(i).getDid();
			//获得餐馆id
			int rid = orders.get(i).getRid();
			Restaurant_spd restaurant_spd = rs.getRestaurantByRid(rid);
			//获得餐馆星级
			int star = restaurant_spd.getStar();
			//获得餐馆名字
			String restName = restaurant_spd.getrName();
			//获得订单中的菜个数
			int dishNum = orders.get(i).getoNumber();
			//获得订单中的菜的总价格
			int totalPrice = orders.get(i).getTotal();
			Dish_spd dish = ds.getselectDish(did);
			//获得菜名
			String dishName = dish.getdName();
			j.put("oid", oid);//订单id
			j.put("restName", restName);//餐馆名
			j.put("dishName", dishName);//菜名
			j.put("totalPrice", totalPrice);//菜总价
			j.put("dishNum", dishNum);//菜个数
			j.put("star", star);//餐馆星级
			//获得用户姓名
			String uname = customer.getCname();
			j.put("uname", uname);//用户姓名
			//获得订单id
			String Oid = req.getParameter("oid");
			//获得举报信息
			String tipText = req.getParameter("tipText");
			if(Oid != null && tipText!=null) {
				java.util.Date tempDate = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String addTime = sdf.format(tempDate);
				boolean flag = ds.insertTip(Integer.parseInt(Oid), tipText, addTime);
				if(flag) {
					j.put("isTip", 1);
					ja.add(j);
					break;
				} else {
					j.put("isTip", 0);
					ja.add(j);
					break;
				}
			}
			ja.add(j);
		}
		pw.write(ja.toString());
		List<Restaurant_spd> allRestaurants = rs.getAllRestaurantMessage();
		session.setAttribute("allRestaurants", allRestaurants);
	}
}

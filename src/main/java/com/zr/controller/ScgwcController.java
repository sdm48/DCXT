package com.zr.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.service.ScgwcService;
import com.zr.serviceimp.ScgwcServiceimpl;

/**
 * 删除购物车
 * @author 柏小毛
 *
 */
public class ScgwcController extends HttpServlet{
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
	// TODO Auto-generated method stub
	doPost(req, resp);
}
@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int gid=Integer.parseInt(req.getParameter("gid"));
		ScgwcService ss=new ScgwcServiceimpl();
		ss.scgwcByGid(gid);
		req.getRequestDispatcher("wdgwc.jsp").forward(req, resp);
	}
}

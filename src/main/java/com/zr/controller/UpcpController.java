package com.zr.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.service.UpcpService;
import com.zr.serviceimp.UpcpServiceImpl;

public class UpcpController extends HttpServlet{
	UpcpService us = new UpcpServiceImpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
				// TODO Auto-generated method stub
				doPost(req, resp);
			}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
				// TODO Auto-generated method stub
				int did = Integer.parseInt(req.getParameter("did"));
				String dname = req.getParameter("dname");
				int dnumber = Integer.parseInt(req.getParameter("dnum"));
				String sname = req.getParameter("sname");
				int rid = Integer.parseInt(req.getParameter("rid"));
				int rprice = Integer.parseInt(req.getParameter("rprice"));
				int isopen = Integer.parseInt(req.getParameter("isopen"));
				boolean flag = us.upCpBydid(did, dname, dnumber, sname, rid, rprice, isopen);
			}
	
}

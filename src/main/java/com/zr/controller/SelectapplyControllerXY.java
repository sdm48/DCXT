package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.model.ApplyXY;
import com.zr.service.ApplyServiceXY;
import com.zr.serviceimp.ApplyServiceImplXY;

import net.sf.json.JSONObject;

public class SelectapplyControllerXY extends HttpServlet{
	ApplyServiceXY  as = new ApplyServiceImplXY();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		//获取页面页数，行数
		int page = Integer.parseInt(req.getParameter("page"));
		int rows = Integer.parseInt(req.getParameter("rows")); 
		//申请开店对象集合
		List<ApplyXY> applys = (new ApplyServiceImplXY()).selectApply(page, rows);
		//申请开店总数
		int count = as.countApplys();
		//申请开店对象集合，申请开店总数放入json
		JSONObject json = new JSONObject();
		json.put("total", count);
		json.put("rows", applys);
		
		PrintWriter pw = resp.getWriter();
		pw.write(json.toString());
	}
}

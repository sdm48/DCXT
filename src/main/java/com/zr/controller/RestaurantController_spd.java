package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import com.zr.model.Restaurant_spd;
import com.zr.service.RestaurantService_spd;
import com.zr.serviceimp.RestaurantServiceImpl_spd;

public class RestaurantController_spd extends HttpServlet{
	RestaurantService_spd rs = new RestaurantServiceImpl_spd();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		//获得全部餐馆
		List<Restaurant_spd> rests = (List<Restaurant_spd>) session.getAttribute("rests");
		String page = req.getParameter("page");
		String rows = req.getParameter("rows");
		PrintWriter pw = resp.getWriter();
		JSONObject j = new JSONObject();
		String tempRid = req.getParameter("rid");
		for (int i=0; i<rests.size(); i++) {
			if(tempRid != null) {
				int rid = Integer.parseInt(tempRid);
				if(rests.get(i).getRid() == rid) {
					int uid = rests.get(i).getUid();
					java.util.Date tempDate = new java.util.Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String addTime = sdf.format(tempDate);
					boolean flag = rs.collectRestaurant(uid, rid, addTime);
					if(flag) {
						j.put("isCollect", 1);
						pw.write(j.toString());
					} else {
						j.put("isCollect", 0);
						pw.write(j.toString());
					}
				}
			}
		}
	}
}

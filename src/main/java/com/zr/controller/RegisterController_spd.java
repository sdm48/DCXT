package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

public class RegisterController_spd extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JSONObject j = new JSONObject();
		PrintWriter pw = resp.getWriter();
		String regName = req.getParameter("regName");
		String regPassword = req.getParameter("regPassword");
		String regPasswordAgain = req.getParameter("regPasswordAgain");
		if(regName=="" || regPassword=="" || regPasswordAgain=="") {
			j.put("code", 1);
		} else if(!regPassword.equals(regPasswordAgain)){
			j.put("code", 2);
		} else if(regPassword.equals(regPasswordAgain)) {
			j.put("code", 3);
		}
		pw.write(j.toString());
	}
}

package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.zr.model.Customer;
import com.zr.service.wdgwcService;
import com.zr.serviceimp.wdgwcServiceimpl;
/**
 * 我的购物车页面查看我的购物车
 * @author 柏小毛
 *
 */
public class WdgwcController extends HttpServlet {
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
	// TODO Auto-generated method stub
	doPost(req, resp);
}
@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		wdgwcService wd=new wdgwcServiceimpl();
		PrintWriter  pw = resp.getWriter();
		HttpSession session=req.getSession();
		Customer cs=(Customer) session.getAttribute("customerly");
		int uid=cs.getUid();
		JSONArray ja=wd.getGwcbyUid(uid);
		JSONObject jo=new JSONObject();
		jo.put("total", 10);
		jo.put("rows", ja);
		pw.write(jo.toString());
		pw.flush();
		pw.close();

	}
}

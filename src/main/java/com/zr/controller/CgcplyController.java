package com.zr.controller;

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zr.dao.CgcpLyDao;
import com.zr.service.CgcdService;
import com.zr.serviceimp.CgcpServiceimpl;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @柏小毛 餐馆菜品的浏览Controller;
 */
public class CgcplyController extends HttpServlet {
	CgcdService cs=new CgcpServiceimpl();
	
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
	// TODO Auto-generated method stub
	doPost(req, resp);
}
@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject jb=new JSONObject();
		req.setCharacterEncoding("utf-8");	
		resp.setCharacterEncoding("utf-8");
		HttpSession session=req.getSession();
		int rid= Integer.parseInt((String) session.getAttribute("rid"));
		int page = Integer.parseInt(req.getParameter("page"));
		int rows = Integer.parseInt(req.getParameter("rows"));
		PrintWriter  pw = resp.getWriter();
		JSONArray js=cs.Cgcply(page, rows, rid);
		int t=cs.getAllcpByrid(rid);
		jb.put("total", t);
		jb.put("rows", js);
		 pw.write(jb.toString());
			pw.flush();
			pw.close();
	}
}

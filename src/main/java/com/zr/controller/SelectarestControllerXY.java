package com.zr.controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zr.model.RestaurantXY;
import com.zr.service.RestaurantServiceXY;
import com.zr.serviceimp.RestaurantServiceImplXY;

import net.sf.json.JSONObject;

public class SelectarestControllerXY extends HttpServlet{
	RestaurantServiceXY rs = new RestaurantServiceImplXY();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		HttpSession session = req.getSession();
		//判断餐馆id是否为空，并将rid存入session中
		if(req.getParameter("rid")!=null){
			session.setAttribute("selectrid", Integer.parseInt(req.getParameter("rid")));
		}
		//得到餐馆对象
		RestaurantXY rsxy = rs.searchRestaurantByRid(Integer.parseInt(session.getAttribute("selectrid").toString()));
		//定义餐馆对象集合，放到json
		List<RestaurantXY> lxy = new ArrayList<RestaurantXY>();
		JSONObject json = new JSONObject();
		if(rsxy.getRid()!=0){
			lxy.add(rsxy);
			//一条记录
			json.put("total", 1);
			json.put("rows", lxy);
		}		
		PrintWriter pw = resp.getWriter();
		pw.write(json.toString());
	}
}

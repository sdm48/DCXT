package com.zr.controller;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zr.model.Customer;
import com.zr.service.jrgwService;
import com.zr.serviceimp.jrgwServiceImpl;

/**
 * 点击加入购物车的功能
 * @author 柏小毛
 *
 */
public class JrfwclyController extends HttpServlet {
	
		jrgwService js=new jrgwServiceImpl();
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
	// TODO Auto-generated method stub
	doPost(req, resp);
}
@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	req.setCharacterEncoding("utf-8");
		HttpSession session=req.getSession();
		String did=req.getParameter("did");
		String dname=req.getParameter("dname");
		String rprice=req.getParameter("rprice");
		String nub=req.getParameter("nub");
		Customer cs=(Customer) session.getAttribute("customerly");
		int uid=cs.getUid();
		js.jrgwcByid(did, dname, rprice, nub, uid);
		req.getRequestDispatcher("wdgwc.jsp").forward(req, resp);
	}
}

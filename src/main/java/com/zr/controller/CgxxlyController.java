package com.zr.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 
 * @author柏小毛
 *餐馆信息的浏览
 */
public class CgxxlyController extends HttpServlet {
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
	// TODO Auto-generated method stub
doPost(req, resp);
}
@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String rid=req.getParameter("rid");
		String rname=req.getParameter("rname");
		HttpSession session =req.getSession();
		session.setAttribute("rname", rname);
		session.setAttribute("rid", rid);
		req.getRequestDispatcher("cgxx.jsp").forward(req, resp);
	}
}

package com.zr.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zr.service.RestaurantServiceXY;
import com.zr.serviceimp.RestaurantServiceImplXY;

public class JiefengrestControllerXY extends HttpServlet{
	RestaurantServiceXY rs = new RestaurantServiceImplXY();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.setCharacterEncoding("utf8");
		resp.setCharacterEncoding("utf8");
		
		int rid = Integer.parseInt(req.getParameter("rid"));
		rs.jiefengRest(rid);
	}
}

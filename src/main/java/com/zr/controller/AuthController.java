package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zr.service.AuthService;
import com.zr.serviceimp.AuthServiceImpl;

import net.sf.json.JSONArray;

public class AuthController extends  HttpServlet{
	AuthService   aservice = new AuthServiceImpl();
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			// TODO Auto-generated method stub
			doPost(req, resp);
		}
		
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
				resp.setCharacterEncoding("utf8");
				PrintWriter  pw = resp.getWriter();
				int aid = Integer.parseInt(req.getParameter("pid"));
				HttpSession session=req.getSession();
				String uname=(String) session.getAttribute("uname");		
				JSONArray  as = aservice.getAuths(aid,uname);
				pw.write(as.toString());
				pw.flush();
				pw.close();
				
				
		}
}
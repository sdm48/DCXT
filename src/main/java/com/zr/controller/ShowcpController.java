package com.zr.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import com.zr.model.Cp_xxd;
import com.zr.model.Restaurant_xxd;
import com.zr.service.ShowService;
import com.zr.serviceimp.ShowServiceImpl;
import com.zr.util.PageUtil;

public class ShowcpController extends HttpServlet{
	ShowService ss = new ShowServiceImpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
				// TODO Auto-generated method stub
				doPost(req, resp);
			}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
				// TODO Auto-generated method stub
				resp.setCharacterEncoding("utf8");
				HttpSession  session = req.getSession();
				PrintWriter pw =resp.getWriter();
				int page = Integer.parseInt(req.getParameter("page"));
				int rows = Integer.parseInt(req.getParameter("rows"));
				String uname = (String) session.getAttribute("uname");
				int uid = ss.getUid(uname);
				int total = ss.getCpsCountByCurrentCp(uid);
				List<Cp_xxd> cps =ss.getRestaurant(uid,page,rows);
				JSONObject  js = new JSONObject();
				js.put("total", total);
				js.put("rows", cps);
				pw.write(js.toString());
				pw.flush();
				pw.close();
			}
}

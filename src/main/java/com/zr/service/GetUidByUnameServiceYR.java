package com.zr.service;

/**
 * 通过Uname获取uid的服务接口
 * @author YR
 *
 */
public interface GetUidByUnameServiceYR {
	/**
	 * 通过uname获取uid
	 * @param uname 用户登录名uname
	 * @return uid
	 */
	int getUidByUname(String uname);

}

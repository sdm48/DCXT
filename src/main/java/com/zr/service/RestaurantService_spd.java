package com.zr.service;

import java.sql.Date;
import java.util.List;

import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;

public interface RestaurantService_spd {
	/**
	 * 得到餐馆总个数
	 * @return
	 */
	public int getRestaurantCount();
	/**
	 * 获得所有餐馆
	 * @return
	 */
	public List<Restaurant_spd> getAllRestaurantMessage();
	/**
	 * 通过餐馆id获得餐馆对象
	 * @param rid 餐馆id
	 * @return
	 */
	public Restaurant_spd getRestaurantByRid(int rid);
	/**
	 * 通过用户id，餐馆id，当前时间生成一份收藏
	 * @param uid 用户id
	 * @param rid 餐馆id
	 * @param addTime 当前时间
	 * @return
	 */
	public boolean collectRestaurant(int uid, int rid, String addTime);
	/**
	 * 通过用户id得到该用户全部订单
	 * @param uid 用户id
	 * @return
	 */
	public List<Order_spd> getOrdersByUid(int uid);
}

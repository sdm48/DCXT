package com.zr.service;

import net.sf.json.JSONArray;

public interface AuthService {
		/**
		 * 根据权限id获取当前有哪些功能
		 * @param aid  
		 * @return  tree 要求的json数据
		 * 
		 */
	public  JSONArray  getAuths(int aid,String uname);
}

package com.zr.service;

import java.util.List;

import com.zr.model.Order_spd;
import com.zr.model.Restaurant_spd;

public interface OrderService_spd {
	/**
	 * 通过用户id获得全部订单
	 * @param uid 用户id
	 * @return
	 */
	public List<Order_spd> getAllOrdersByUid(int uid);
	/**
	 * 通过订单id获得餐馆对象
	 * @param oid 订单id
	 * @return
	 */
	public Restaurant_spd getRestaurantByOid(int oid);
	/**
	 * 通过评价内容，当前时间，订单id更改订单的评价
	 * @param judgeText 评价内容
	 * @param addTime 当前时间
	 * @param oid 订单id
	 * @return
	 */
	public boolean addJudgeTextAndTime(String judgeText, String addTime, int judgeNum, int oid);
}

package com.zr.service;

import java.sql.Date;
import java.util.List;

import com.zr.model.Dish_spd;

public interface DishService_spd {
	/**
	 * 获得全部菜的个数
	 * @return
	 */
	public int getAllDishsNum();
	/**
	 * 获得全部菜
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<Dish_spd> getAllDishs(int page, int rows);
	/**
	 * 通过菜id获得被选中的菜
	 * @param did
	 * @return
	 */
	public Dish_spd getselectDish(int did);
	/**
	 * 获得菜的类名
	 * @return
	 */
	public List<String> getDidSname();
	/**
	 * 通过菜名获得该种类的菜
	 * @param sname
	 * @return
	 */
	public List<Dish_spd> getDishsBySname(String sname);
	/**
	 * 通过模糊搜索名获得菜信息
	 * @param searchName
	 * @return
	 */
	public List<Dish_spd> getDishsBySearchName(String searchName);
	/**
	 * 通过用户id，餐馆id，菜id，菜数量，地址,总价,下单时间生成订单
	 * @param userId 用户id
	 * @param restId 餐馆id
	 * @param dishId 菜id
	 * @param dishNumber 菜数量
	 * @param address 地址
	 * @param totalPrice 总价
	 * @param addTime
	 * @return
	 */
	public boolean insertOrder(int userId, int restId, int dishId, int dishNumber, String address, int totalPrice, String addTime);
	/**
	 * 通过订单id，举报内容，当前日期生成一份举报
	 * @param oid 订单id
	 * @param tipText 举报内容
	 * @param tipDate 当前日期
	 * @return
	 */
	public boolean insertTip(int oid, String tipText, String tipDate);
}

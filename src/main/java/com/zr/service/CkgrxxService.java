package com.zr.service;

import com.zr.model.Customer;
/**
 * 
 * @author 李昱
 * 通过用户名查询个人信息的service
 * @return
 */
public interface CkgrxxService {
	public Customer getCuByUname(String uname);
}

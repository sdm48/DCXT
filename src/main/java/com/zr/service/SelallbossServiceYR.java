package com.zr.service;

/**
 * 查询已注册老板的总数的接口
 * @author YR
 *
 */
public interface SelallbossServiceYR {
	/**
	 * 查询老板总数
	 * @return 老板总数
	 */
	int selallboss();

}

package com.zr.service;

import java.util.List;

import com.zr.model.ClosedYR;
import com.zr.model.User;

/**
 * 查询将封用户的服务接口
 * @author YR
 *
 */
public interface SelclouserServiceYR {
	/**
	 * 通过用户id查找用户
	 * @param uid ID
	 * @return User对象
	 */
	User selclouserByUid(int uid);
	
	/**
	 * 通过页码和每页多少行查询已封用户
	 * @param page 页码
	 * @param rows 每页多少行
	 * @return 已封账户记录的集合
	 */
	List<ClosedYR> selcloseduserByPage(int page,int rows);

	/**
	 * 查找已封账户的总数
	 * @return 已封账户的总数
	 */
	int selallclouser();
}

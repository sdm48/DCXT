package com.zr.service;

import java.util.List;

import com.zr.model.Cp_xxd;
import com.zr.model.Restaurant_xxd;

public interface ShowService {
	
	/**
	 * 根据用户id查询当前餐馆的菜品信息
	 * @param uid 用户ID
	 * @param page
	 * @param rows
	 * @return 菜品的集合
	 */
	public List<Cp_xxd> getRestaurant(int uid,int page,int rows);
	
	/**
	 * 根据用户id获取当前餐馆下的菜品数量
	 * @param uid 用户id
	 * @return 菜品数量
	 */
	public int getCpsCountByCurrentCp(int uid);
	
	/**
	 * 获取uid
	 * @param uname 用户名
	 * @return
	 */
	public int getUid(String uname);
}

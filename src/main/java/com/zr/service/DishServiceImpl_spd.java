package com.zr.service;

import java.sql.Date;
import java.util.List;

import com.zr.dao.DishDao_spd;
import com.zr.daoimp.DishDaoImpl_spd;
import com.zr.model.Dish_spd;
import com.zr.service.DishService_spd;

public class DishServiceImpl_spd implements DishService_spd{
	DishDao_spd dd = new DishDaoImpl_spd();
	@Override
	public List<Dish_spd> getAllDishs(int page, int rows) {
		return dd.getAllDishs(page, rows);
	}
	@Override
	public Dish_spd getselectDish(int did) {
		return dd.getSelectDish(did);
	}
	@Override
	public List<String> getDidSname() {
		return dd.getDidSname();
	}
	@Override
	public List<Dish_spd> getDishsBySname(String sname) {
		return dd.getDishsBySname(sname);
	}
	@Override
	public List<Dish_spd> getDishsBySearchName(String searchName) {
		return dd.getDishsBySearchName(searchName);
	}
	@Override
	public boolean insertOrder(int userId, int restId, int dishId, int dishNumber, String address, int totalPrice, String addTime) {
		return dd.insertOrder(userId, restId, dishId, dishNumber, address, totalPrice, addTime);
	}
	/*@Override
	public boolean insertTip(int oid, String tipText, Date tipDate) {
		return dd.insertTip(oid, tipText, tipDate);
	}*/
	@Override
	public int getAllDishsNum() {
		return dd.getAllDishsNum();
	}
	@Override
	public boolean insertTip(int oid, String tipText, String tipDate) {
		return dd.insertTip(oid, tipText, tipDate);
	}
}

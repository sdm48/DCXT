package com.zr.service;

import java.util.List;

import com.zr.model.Order_PL;

/**
 * 订单的服务接口
 * @author 彭浪
 *
 */
public interface OrderService_PL {
	/**
	 * 通过传入一个餐馆的id来得到这个餐馆的所有订单
	 * @param rid 传入一个餐馆的id
	 * @param startTime 传入一个订单起始时间
	 * @param endTime 传入一个订单的结束时间
	 * @return 返回一个订单集合
	 */
	public List<Order_PL> getOrderPLByRid(int rid,String startTime,String endTime);
	/**
	 * 通过传入一个餐厅的id来得到这个餐馆的最初的订单的起始时间(年月日)
	 * @param rid 传入一个餐厅的id
	 * @return 返回一个年月日的数组
	 */
	public String[] selectStartTime(int rid);
	/**
	 * 通过传入一个起始时间与结束时间来获取周数的方法(周数为返回的时间集合的长度的一半)
	 * @param dates 起始时间
	 * @param datee 结束时间
	 * @return 返回一个字符串的时间集合，以俩个为一组 例如：第一周：2015-01-04(下标为0)到2015-01-11(下标为1)
	 */
	public List<String> getWeeks(String dates, String datee);
	/**
	 * 通过传入一个餐馆的id来得到这个餐馆的所有未接单的订单
	 * @param rid 传入一个餐馆的id
	 * @return 返回一个订单集合
	 */
	public List<Order_PL> getOrderPLByRid(int rid);
	/**
	 * 通过传入一个餐馆的id来得到这个餐馆的所有已接单的订单
	 * @param rid 传入一个餐馆的id
	 * @return 返回一个订单集合
	 */
	public List<Order_PL> getAcceptOrderPLByRid(int rid);
	/**
	 * 修改一个未接单订单为已接单的订单的方法
	 * @param orderId 传入一个订单的id的字符串
	 * @return 返回修改是否成功
	 */
	public boolean modifyOrderPL(String orderId);
	/**
	 * 商家回复一个订单的方法
	 * @param orderId 传入一个订单id的字符串
	 * @param reply 传入一个回复信息
	 * @return 返回修改是否成功
	 */
	public boolean replyOrderPL(String orderId,String reply);
	/**
	 * 通过传入一个订单的id来查询一个订单的方法
	 * @param o 传入一个订单id
	 * @return 返回一个订单对象
	 */
	public Order_PL selectone(int oid);
}

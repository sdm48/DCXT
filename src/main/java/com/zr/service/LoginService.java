
package com.zr.service;

import com.zr.model.Customer;

public interface LoginService {
		
			/**
			 * 根据用户名，判断是否有该用户
			 * @param uname
			 * @return
			 */
			public  boolean   validateUser(String uname,String upswd);
			
			/**
			 * 获取uid
			 * @param uname 用户名
			 * @return
			 */
			public int getUid(String uname);

			public Customer getCubyName(String uname);
}


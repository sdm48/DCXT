package com.zr.service;

import com.zr.model.User_PL;

/**
 * 用户的服务接口
 * @author 彭浪
 *
 */
public interface UserService_PL {
	/**
	 * 通过传入一个用户的名字来得到用户对象
	 * @param uid 传入一个用户的名字
	 * @return 返回一个用户对象
	 */
	public User_PL getUserPLByUname(String uname);
	/**
	 * 通过传入一个用户id来得到用户对象
	 * @param uid 传入一个用户id
	 * @return 返回一个用户对象
	 */
	public User_PL getUserPLByUid(int uid);
}

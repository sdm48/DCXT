package com.zr.service;

import java.util.List;

import com.zr.model.TipXY;

public interface TipServiceXY {
	/**
	 * 查看举报
	 * @param page 页码
	 * @param rows 行数
	 * @return
	 */
	public List<TipXY> selectTips(int page,int rows);
	/**
	 * 举报总数
	 * @return
	 */
	public int countTips();
}

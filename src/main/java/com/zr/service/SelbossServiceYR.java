package com.zr.service;

import java.util.List;

import com.zr.model.User;

/**
 * 管理员查找已注册老板的服务
 * @author YR
 *
 */
public interface SelbossServiceYR {
	
	/**
	 * 通过页码和每页多少行查询老板
	 * @param page 页码
	 * @param rows 每页多少行
	 * @return  老板的集合
	 */
	List<User> selbossByPage(int page,int rows);

}

package com.zr.service;

import java.util.List;

import com.zr.model.Dish_PL;

/**
 * 菜的服务接口
 * @author 彭浪
 *
 */
public interface DishService_PL {
	/**
	 * 通过传入一个菜的id来得到这个菜的对象
	 * @param did 传入一个菜的id的字符串
	 * @return 返回一个菜的对象的集合
	 */
	public List<Dish_PL> getDishPLByDid(String did);
	/**
	 * 修改一个菜的方法
	 * @param d 传入一个菜的对象
	 * @return 返回修改是否成功
	 */
	public boolean modifyDishPL(Dish_PL d);
}

package com.zr.service;

import java.util.List;

import com.zr.model.RestaurantXY;

public interface RestaurantServiceXY {
	/**
	 * 查询餐馆
	 * @param rid
	 * @return
	 */
	public RestaurantXY searchRestaurantByRid(int rid);
	/**
	 * 删除餐馆
	 * @param rid 餐馆id
	 */
	public int removeRestaurantByRid(int rid);
	/**
	 * 查看被禁用的餐馆
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<RestaurantXY> selectRestsJinyong(int page,int rows);
	/**
	 * 被禁用餐馆的数量
	 * @return
	 */
	public int countRests();
	/**
	 * 解封餐馆
	 */
	public void jiefengRest(int rid);
}

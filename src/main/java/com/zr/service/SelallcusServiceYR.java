package com.zr.service;
/**
 * 查询已注册顾客的总数的接口
 * @author YR
 *
 */
public interface SelallcusServiceYR {
	/**
	 * 查询顾客总数
	 * @return 顾客总数
	 */
	int selallcus();

}

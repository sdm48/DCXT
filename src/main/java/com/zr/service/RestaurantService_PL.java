package com.zr.service;

import com.zr.model.Restaurant_PL;

/**
 * 餐馆的服务接口
 * @author 彭浪
 *
 */
public interface RestaurantService_PL {
	/**
	 * 通过传入一个餐馆老板的id来得到他所拥有的餐厅对象
	 * @param uid 传入一个餐馆的老板的id
	 * @return 返回一个餐馆对象
	 */
	public Restaurant_PL getRestaurantPLByUid(int uid);
	/**
	 * 通过传入一个餐馆的名字来得到一个对应的餐厅对象
	 * @param Name 传入一个餐馆的名字
	 * @return 返回一个餐馆对象
	 */
	public Restaurant_PL getRestaurantPLByName(String rname);
	/**
	 * 新增一个餐馆对象
	 * @param r 传入一个餐馆的对象
	 * @return 返回新增是否成功
	 */
	public boolean addRestaurantPL(Restaurant_PL r);
	/**
	 * 修改一个餐馆对象
	 * @param r 传入一个餐馆的对象
	 * @return 返回修改是否成功
	 */
	public boolean modifyRestaurantPL(Restaurant_PL r);
	/**
	 * 修改一个餐馆的营业状态(1为营业,0为关门),使其取反。
	 * @param rid 传入一个餐馆的id
	 * @param isOpen 传入一个餐馆的当前状态
	 * @return 返回修改是否成功
	 */
	public boolean modifyRestaurantPLState(int rid,int isOpen);
	/**
	 * 修改一个餐馆的星级
	 * @param rid 传入一个餐馆的id
	 * @param star 传入一个1-5的数字
	 * @return 返回修改是否成功
	 */
	public boolean modifyRestaurantPLStar(int rid,int star);
	
}

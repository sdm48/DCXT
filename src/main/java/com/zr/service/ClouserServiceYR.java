package com.zr.service;

/**
 * 封禁或解封账户的服务接口
 * @author YR
 *
 */
public interface ClouserServiceYR {
	/**
	 * 通过uid封禁账户
	 * @param uid 用户uid
	 */
	void coluser(int uid);
	
	/**
	 * 执行封号操作后将操作记录在案
	 * @param mid 操作管理员的id
	 * @param uid 被封账户的id
	 * @param urole  被封账户被封之前的urole
	 * @param cdate 封号操作的时间
	 * @param ccontent 封号的原因
	 */
	void recordclo(int mid,int uid,int urole,String cdate,String ccontent);
    
	
	/**
	 * 通过uid，urole解封账户
	 * @param uid  解封账户的uid
	 * @param urole 被封账户被封之前的urole
	 */
	void openuser(int uid,int urole);
	
	
	/**
	 * 通过uid将解封的角色从封号记录中清除
	 * @param uid 解封角色的uid
	 */
	void clearrecordclo(int uid);


}

package com.zr.service;

import net.sf.json.JSONArray;

public interface LlcgxxService {
	/**
	 * 拿到每页的餐馆json数组
	 * @param page
	 * @param rows
	 * @return
	 */
	public JSONArray getAllRes(	int page,int rows);
	/**
	 * 拿到所有餐馆的总数用来分页
	 * @return
	 */
	public int getAllnub();
}

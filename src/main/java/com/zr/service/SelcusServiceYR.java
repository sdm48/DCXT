package com.zr.service;

import java.util.List;

import com.zr.model.User;

/**
 * 管理员查找已注册顾客的服务
 * @author YR
 *
 */
public interface SelcusServiceYR {
	/**
	 * 通过页码和每页多少行查询顾客
	 * @param page 页码
	 * @param rows 每页多少行
	 * @return  顾客的集合
	 */
	List<User> selcusByPage(int page,int rows);

}

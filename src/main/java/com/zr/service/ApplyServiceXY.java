package com.zr.service;

import java.util.List;

import com.zr.model.ApplyXY;

public interface ApplyServiceXY {
	/**
	 * 查看开店申请
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<ApplyXY> selectApply(int page,int rows);
	/**
	 * 开店申请总数
	 * @return
	 */
	public int countApplys();
	/**
	 * 通过申请
	 */
	public void passApply(int aid,String now);
	/**
	 * 不通过请求
	 * @param aid
	 */
	public void notpassApply(int aid);
}

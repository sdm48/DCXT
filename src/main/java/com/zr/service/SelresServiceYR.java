package com.zr.service;

import java.util.List;

import com.zr.model.RestaurantYR;
import com.zr.model.User;

/**
 * 查询餐馆的服务接口
 * @author YR
 *
 */
public interface SelresServiceYR {
	/**
	 * 通过页码和每页多少行查询餐馆
	 * @param page 页码
	 * @param rows 每页多少行
	 * @return  餐馆的集合
	 */
	List<RestaurantYR> selresByPage(int page,int rows);
	
	/**
	 * 查找餐馆的总数
	 * @return 餐馆的总数
	 */
	int selallrestaurant();

}

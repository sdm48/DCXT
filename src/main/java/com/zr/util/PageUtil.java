package com.zr.util;

public class PageUtil {
		/**
		 * 
		 * @param scount  总数
		 * @param currentPage  当前页
		 * @param pageSize  分页条数
		 * @return
		 */
	 public static  String  getPagList(int scount,int currentPage,int pageSize){
		 		//求页码数
	    	int totalPage =  scount%pageSize == 0?scount/pageSize:scount/pageSize+1;
	    	StringBuffer   str = new StringBuffer();
	    	str.append("<li><a href='selectStus?page=1'>首页</a></li>");
	    	if(currentPage==1){
	    		str.append("<li class='disabled'><a href='#'>上一页</a></li>");
	    	}
	    	else{
	    		str.append("<li ><a href='selectStus?page="+(currentPage-1)+"'>上一页</a></li>");
	    	}
	    	for (int i = currentPage-3; i <=currentPage+3; i++) {
				if(i<1||i>totalPage){
					continue;
				}
				if(i==currentPage){
					str.append("<li class='active'><a href='#'>"+i+"</a></li>");
				}else{
					str.append("<li ><a href='selectStus?page="+i+"'>"+i+"</a></li>");
				}
			}
	    	if(currentPage==totalPage){
	    		str.append("<li class='disabled'><a href='#'>下一页</a></li>");
	    	}
	    	else{
	    		str.append("<li ><a href='selectStus?page="+(currentPage+1)+"'>下一页</a></li>");
	    	}
	    	str.append("<li><a href='selectStus?page="+totalPage+"'>尾页</a></li>");
	    	return str.toString();
	    }
}

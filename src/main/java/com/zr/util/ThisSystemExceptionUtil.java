package com.zr.util;

/**
 * 
 * 本系统异常
 * @author 彭浪
 *
 */
public class ThisSystemExceptionUtil extends RuntimeException{
	private String code;
	public ThisSystemExceptionUtil(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	/**
	 * 设置一个系统异常
	 * @param code 传入这个异常的编号
	 * @param message 传入这个异常的异常信息
	 */
	public ThisSystemExceptionUtil(String code,String message) {
		super(message);
		this.code=code;
		// TODO Auto-generated constructor stub
	}
	/**
	 * 设置一个系统异常
	 * @param message 传入这个异常的异常信息
	 */
	public ThisSystemExceptionUtil(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ThisSystemExceptionUtil(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

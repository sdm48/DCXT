
package com.zr.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCUtil {
	 //1.数据库地址  (根据不同的数据标准是不一样)
	  private  final  static String DBURL = "jdbc:mysql://172.18.20.48:3306/dcxt?useUnicode=true&characterEncoding=UTF8";
	  //2.设置用户和密码
	  private  final  static String  USERNAME = "root";
	  private  final  static String  PASSWORD = "123";
	  //3.设置驱动名称 (根据不同的数据标准是不一样)
	  private  final  static String  DBDRIVER = "com.mysql.jdbc.Driver";
	  /**
	   * 获取数据库连接
	   * @return  返回数据库连接
	   */
	  public  static  Connection  getConnection(){
		  Connection  con = null;
		  try {
			Class.forName(DBDRIVER);
			con  =  DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return  con;
			
	  }
	  
	  public static void  closeJDBC(Statement st,Connection  con) throws SQLException{
		    if(st!=null){
		    	st.close();
		    }
		    if(con!=null){
		    	con.close();
		    }
	  }
	  public static void  closeJDBC(Connection  con) throws SQLException{
		    if(con!=null){
		    	con.close();
		    }
	  }
}


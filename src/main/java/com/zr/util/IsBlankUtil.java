package com.zr.util;
/**
 * 工具方法
 * @author 彭浪
 *
 */
public class IsBlankUtil {
	/**
	 * 判断用户输入是否为null或空白字符串
	 * @param message 传入一个异常信息
	 * @param s 传入要验证的值
	 * @return 返回之前传入的要验证的值
	 */
	public static String isBlank(String message,String s){
		if(s==null||(s=s.trim()).length()==0){
			throw new ThisSystemExceptionUtil("1000",message);
		}
		return s;
	}
}

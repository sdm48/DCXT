package com.zr.model;

public class RestaurantLy {
	private int Rid;
	private String Rname;
	private String Rphone;
	private String Raddress;
	private int Uid;
	private String WorkData;
	private int Star;
	private String Isopen;
	public int getRid() {
		return Rid;
	}
	public void setRid(int rid) {
		Rid = rid;
	}
	public String getRname() {
		return Rname;
	}
	public void setRname(String rname) {
		Rname = rname;
	}
	public String getRphone() {
		return Rphone;
	}
	public void setRphone(String rphone) {
		Rphone = rphone;
	}
	public String getRaddress() {
		return Raddress;
	}
	public void setRaddress(String raddress) {
		Raddress = raddress;
	}
	public int getUid() {
		return Uid;
	}
	public void setUid(int uid) {
		Uid = uid;
	}
	public String getWorkData() {
		return WorkData;
	}
	public void setWorkData(String workData) {
		WorkData = workData;
	}
	public int getStar() {
		return Star;
	}
	public void setStar(int star) {
		Star = star;
	}
	public String getIsopen() {
		return Isopen;
	}
	public void setIsopen(String isopen) {
		Isopen = isopen;
	}

}

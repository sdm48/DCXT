package com.zr.model;

public class Dish_spd {
	private int did;
	private String dName;
	private int dNumber;
	private String sName;
	private int rid;
	private int rPrice;
	private int isOPen;
	public int getDid() {
		return did;
	}
	public void setDid(int did) {
		this.did = did;
	}
	public String getdName() {
		return dName;
	}
	public void setdName(String dName) {
		this.dName = dName;
	}
	public int getdNumber() {
		return dNumber;
	}
	public void setdNumber(int dNumber) {
		this.dNumber = dNumber;
	}
	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public int getrPrice() {
		return rPrice;
	}
	public void setrPrice(int rPrice) {
		this.rPrice = rPrice;
	}
	public int getIsOPen() {
		return isOPen;
	}
	public void setIsOPen(int isOPen) {
		this.isOPen = isOPen;
	}
}

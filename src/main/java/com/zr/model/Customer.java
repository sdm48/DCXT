package com.zr.model;

public class Customer {
	private String Cname;
	private int Cphone;
	private int Uid;
	private String Csex;
	private String Caddress;
	private int Oid;
	private String Upassword;
	public int getOid() {
		return Oid;
	}


	public String getUpassword() {
		return Upassword;
	}


	public void setUpassword(String upassword) {
		Upassword = upassword;
	}


	public void setOid(int oid) {
		Oid = oid;
	}
	public String getCname() {
		return Cname;
	}
	public void setCname(String cname) {
		Cname = cname;
	}
	public int getCphone() {
		return Cphone;
	}
	public void setCphone(int cphone) {
		Cphone = cphone;
	}
	public int getUid() {
		return Uid;
	}
	public void setUid(int uid) {
		Uid = uid;
	}
	public String getCsex() {
		return Csex;
	}
	public void setCsex(String csex) {
		Csex = csex;
	}
	public String getCaddress() {
		return Caddress;
	}
	public void setCaddress(String caddress) {
		Caddress = caddress;
	}
	
}

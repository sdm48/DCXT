package com.zr.model;

public class DishLy {
	private int Did;
	private String Dname;
	private int Dnumber;
	private String Sname;
	private int Rid;
	private int Rprice;
	private String Isopen;
	public int getDid() {
		return Did;
	}
	public void setDid(int did) {
		Did = did;
	}
	public String getDname() {
		return Dname;
	}
	public void setDname(String dname) {
		Dname = dname;
	}
	public int getDnumber() {
		return Dnumber;
	}
	public void setDnumber(int dnumber) {
		Dnumber = dnumber;
	}
	public String getSname() {
		return Sname;
	}
	public void setSname(String sname) {
		Sname = sname;
	}
	public int getRid() {
		return Rid;
	}
	public void setRid(int rid) {
		Rid = rid;
	}
	public int getRprice() {
		return Rprice;
	}
	public void setRprice(int rprice) {
		Rprice = rprice;
	}
	public String getIsopen() {
		return Isopen;
	}
	public void setIsopen(String isopen) {
		Isopen = isopen;
	}
}

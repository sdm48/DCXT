package com.zr.model;

/**
 * 餐馆的餐馆信息
 * @author 彭浪
 *
 */
public class Restaurant_PL {
	//餐馆id
	private int rid;
	//餐馆名字
	private String rname;
	//餐馆联系电话
	private String rphone;
	//餐馆地址
	private String raddress;
	//餐馆的老板id
	private int uid;
	//餐馆的创建时间
	private String createDate;
	//餐馆的星级
	private int star;
	//餐馆的状态（1为营业，0为关门）
	private int isOpen;
	
	public Restaurant_PL() {
		
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	public String getRphone() {
		return rphone;
	}
	public void setRphone(String rphone) {
		this.rphone = rphone;
	}
	public String getRaddress() {
		return raddress;
	}
	public void setRaddress(String raddress) {
		this.raddress = raddress;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public int getStar() {
		return star;
	}
	public void setStar(int star) {
		this.star = star;
	}
	public int getIsOpen() {
		return isOpen;
	}
	public void setIsOpen(int isOpen) {
		this.isOpen = isOpen;
	}
}

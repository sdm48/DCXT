package com.zr.model;

public class User {
	private String uname;
	private String upasw;
	private String uid;
	private String uphone;
	private String urole;
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getUpasw() {
		return upasw;
	}
	public void setUpasw(String upasw) {
		this.upasw = upasw;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUphone() {
		return uphone;
	}
	public void setUphone(String uphone) {
		this.uphone = uphone;
	}
	public String getUrole() {
		return urole;
	}
	public void setUrole(String urole) {
		this.urole = urole;
	}
}

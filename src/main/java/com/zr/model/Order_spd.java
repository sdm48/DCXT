package com.zr.model;

public class Order_spd {
	private int oid;
	private int uid;
	private int rid;
	private int did;
	private int oNumber;
	private String uAddress;
	private int rState;
	private int uState;
	private String judge;
	private String reply;
	private int total;
	private String oDate;
	private String judgeDate;
	private int judgeNum;
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public int getDid() {
		return did;
	}
	public void setDid(int did) {
		this.did = did;
	}
	public int getoNumber() {
		return oNumber;
	}
	public void setoNumber(int oNumber) {
		this.oNumber = oNumber;
	}
	public String getuAddress() {
		return uAddress;
	}
	public void setuAddress(String uAddress) {
		this.uAddress = uAddress;
	}
	public int getrState() {
		return rState;
	}
	public void setrState(int rState) {
		this.rState = rState;
	}
	public int getuState() {
		return uState;
	}
	public void setuState(int uState) {
		this.uState = uState;
	}
	public String getJudge() {
		return judge;
	}
	public void setJudge(String judge) {
		this.judge = judge;
	}
	public String getReply() {
		return reply;
	}
	public void setReply(String reply) {
		this.reply = reply;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getoDate() {
		return oDate;
	}
	public void setoDate(String oDate) {
		this.oDate = oDate;
	}
	public String getJudgeDate() {
		return judgeDate;
	}
	public void setJudgeDate(String judgeDate) {
		this.judgeDate = judgeDate;
	}
	public int getJudgeNum() {
		return judgeNum;
	}
	public void setJudgeNum(int judgeNum) {
		this.judgeNum = judgeNum;
	}
}

package com.zr.model;

public class Auth {
		private  int aid;
		private String aname;
		private String astate;
		private String aurl;
		public int getAid() {
			return aid;
		}
		public void setAid(int aid) {
			this.aid = aid;
		}
		public String getAname() {
			return aname;
		}
		public void setAname(String aname) {
			this.aname = aname;
		}
		public String getAstate() {
			return astate;
		}
		public void setAstate(String astate) {
			this.astate = astate;
		}
		public String getAurl() {
			return aurl;
		}
		public void setAurl(String aurl) {
			this.aurl = aurl;
		}
		
}
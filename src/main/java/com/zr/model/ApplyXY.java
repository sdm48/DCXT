package com.zr.model;

public class ApplyXY {
	private int aid;
	private int uid;
	private int aispass;
	private int mid;
	private String adate;
	private String passdate;
	
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public int getAispass() {
		return aispass;
	}
	public void setAispass(int aispass) {
		this.aispass = aispass;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getAdate() {
		return adate;
	}
	public void setAdate(String adate) {
		this.adate = adate;
	}
	public String getPassdate() {
		return passdate;
	}
	public void setPassdate(String passdate) {
		this.passdate = passdate;
	}
}

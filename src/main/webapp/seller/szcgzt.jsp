<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>设置餐馆营业状态</title>
<link rel="stylesheet" type="text/css"
	href="../themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="../themes/color.css">
<script type="text/javascript"
	src="../js/jquery.min.js"></script>
<script type="text/javascript"
	src="../js/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
	$(function(){
		$.ajax({
			//配置参数
			//请求方式
			type:'post',
			//需要发起的请求
			url:'updateRestaurantStatePL',
			//需要带什么数据过去
			data:{"state":0},
			//定义返回的数据类型
			dataType:"json",
			//请求成功时调用的函数
			success:function(data){ 
				if(data.state==1){
					$("div").text(
							"目前餐馆为营业状态"
							);
					$("input[type='submit']").val("关闭餐馆");
				}else{
					$("div").text(
							"目前餐馆为关门状态"
							);
					$("input[type='submit']").val("开始营业");
				}
			}
		});
		
		$("input[type='submit']").click(function(){
			
			var state=$("input[type='submit']").val();
			$.ajax({
				//配置参数
				//请求方式
				type:'post',
				//需要发起的请求
				url:'updateRestaurantStatePL',
				//需要带什么数据过去
				data:{"state":1},
				//定义返回的数据类型
				dataType:"json",
				//请求成功时调用的函数
				success:function(data){
					if(state=="关闭餐馆"){
						$("div").text(
								"目前餐馆为关门状态"
								);
						$("input[type='submit']").val("开始营业");
					}else{
						$("div").text(
								"目前餐馆为营业状态"
								);
						$("input[type='submit']").val("关闭餐馆");
					}
					
				}
			});
		});
	});
</script>
<body>
	<div>
	
	</div>
	<br/>
	<input type="submit" value=""/>
</body>
</html>
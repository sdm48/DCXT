<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查询餐馆信息</title>
<link rel="stylesheet" type="text/css"
	href="../themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="../themes/color.css">
<script type="text/javascript"
	src="../js/jquery.min.js"></script>
<script type="text/javascript"
	src="../js/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
	$(function(){
		$.ajax({
			//配置参数
			//请求方式
			type:'post',
			//需要发起的请求
			url:'updateRestaurantPL',
			//需要带什么数据过去
			data:{},
			//定义返回的数据类型
			dataType:"json",
			//请求成功时调用的函数
			success:function(data){
				$("div").append(
						"<div>餐馆名字："+data.RestaurantPL.rname+"</div>"+
						"<div>餐馆联系电话："+data.RestaurantPL.rphone+"</div>"+
						"<div>餐馆详细地址："+data.RestaurantPL.raddress+"</div>"+
						"<div>餐馆创建时间："+data.RestaurantPL.createDate+"</div>"+
						"<div>餐馆星级："+data.RestaurantPL.star+"星级</div>"
						);
			}
		});
		
		//用户名输入框
		$("input[name='rname']").validatebox({    
		    required: true,    
		    validType: "餐馆名字"
		}); 
		//密码输入框
		$("input[name='rphone']").validatebox({    
		    required: true,    
		    validType: "餐馆联系电话"
		}); 
		$("input[name='raddress']").validatebox({    
		    required: true,    
		    validType: "餐馆详细地址"
		}); 
		//修改餐馆信息
		$("input[type='submit']").click(function(){
			var rname=$("input[name='rname']");
			var rphone=$("input[name='rphone']");
			var raddress=$("input[name='raddress']");
			$.ajax({
				//配置参数
				//请求方式
				type:'post',
				//需要发起的请求
				url:'updateRestaurantPL',
				//需要带什么数据过去
				data:{'rname':rname.val(),'rphone':rphone.val(),'raddress':raddress.val()},
				//定义返回的数据类型
				dataType:"json",
				//请求成功时调用的函数
				success:function(data){
					$.messager.confirm('确认',data.message,function(data){    
					if (data){
						rname.val("");
						rphone.val("");
						raddress.val("");
						location.reload();
						}
					})
				}
			});
		});
		
		
	});
</script>
<body>
		<div>
			
		</div>
		<br/>
		<br/>
		请输入修改内容：
		<br/>
		餐馆名字：<input name="rname" type="text" placeholder="请输入餐馆名字"/>
		<br/>
		餐馆联系电话：<input name="rphone" type="text" placeholder="请输入餐馆联系电话"/>
		<br/>
		餐馆详细地址：<input name="raddress" type="text" placeholder="请输入餐馆详细地址">
		<br/>
		<input type="submit" value="确定"/>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" 
    uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新增菜品</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="themes/color.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
	$(function () {
		$('#dg').datagrid({    
		    url:'showcp',
		    striped:true,
		    resizable:false,
		    columns:[[
				//{field:'Did',title:'菜品id',width:100},
		        {field:'dname',title:'菜品名',width:100},    
		        {field:'dnumber',title:'菜品数量',width:100},    
		        {field:'sname',title:'菜品类别',width:100,},
		        {field:'rprice',title:'菜品价格',width:100,}
		    ]],
		    pagination:true,
		    striped:true,
		    rownumbers:true,
		    toolbar: [{
				iconCls: 'icon-add',
				text:'新增菜品',
				handler: function(){
					$('#win').window('open');
					
				}
			}]
		});
		$("#addSure").click(function(){
			$.ajax({
				url : 'insertcp',
				data : {
					//did:$("#did").val(),
					dname : $("#dname").val(),
					dnum : $("#dnum").val(),
					sname : $("#sname").val(),
					rid : $("#rid").val(),
					rprice : $("#rprice").val(),
					isopen : $("#isopen").val(),
				},
				dataType : 'text',
				type : 'post',
				success : function(r){
					$('#win').window('close')
					$('#dg').datagrid('load')
					$("#did").val('')
					$("#dname").val('')
					$("#dnum").val('')
					$("#sname").val('')
					$("#rid").val('')
					$("#rprice").val('')
					$("#isopen").val('')
				}
				
			});
		})
		$('#win').window({
			title:'新增菜品',
		    width:950,    
		    height:200,    
		    modal:true,
		    collapsible:false,
		    minimizable:false,
		    maximizable:false,
		    draggable:false,
		    resizable:false
		});  
		$('#win').window('close');
	});
</script>
<body>
	<table id="dg">
		
	</table>
	<div id="win">
		<table id = "uptab" border="1px">
			<tr>
				<td>菜品名</td>
				<td>菜品数量</td>
				<td>菜品种类</td>
				<td>餐馆id</td>
				<td>菜品价格</td>
				<td>菜品能否被选中</td>
			</tr>
			<tr><td><input type='text' id='dname' /></td>
				<td><input type='text' id='dnum' /></td>
				<td><input type='text' id='sname' /></td>
				<td><input type='text' id='rid' /></td>
				<td><input type='text' id='rprice' /></td>
				<td><input type='text' id='isopen' /></td>
			</tr>
		</table>
		<button id="addSure">确定</button>
	</div> 		
</body>
</html>
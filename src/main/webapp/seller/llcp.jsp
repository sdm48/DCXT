<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" 
    uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>浏览菜品</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="themes/color.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
	$(function () {
		$('#dg').datagrid({    
		    url:'showcp',
		    striped:true,
		    resizable:false,
		    columns:[[
				{field:'did',title:'菜品id',width:100},
		        {field:'dname',title:'菜品名',width:100},    
		        {field:'dnumber',title:'菜品数量',width:100},    
		        {field:'sname',title:'菜品类别',width:100,},
		        {field:'rprice',title:'菜品价格',width:100,}
		    ]],
		    pagination:true,
		    striped:true,
		    rownumbers:true
		});
	});
</script>
<body>
	<table id="dg">
		
	</table>  
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查询餐馆信息</title>

<link rel="stylesheet" href="../css/bootstrap.min.css">
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="../js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="../themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="../themes/color.css">
<script type="text/javascript"
	src="../js/jquery.min.js"></script>
<script type="text/javascript"
	src="../js/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../js/easyui-lang-zh_CN.js"></script>
<style type="text/css">
table tr,td{
	border:1px solid black;
}
</style>

<script type="text/javascript">
	$(function(){
		//隐藏标签
		$("#year").hide();
		$("#month").hide();
		$("#day").hide();
		$("#week").hide();
		//当查询方式被改变时
		$("#select").change(function(){
			$.ajax({
				//配置参数
				//请求方式
				type:'post',
				//需要发起的请求
				url:'countRestaurantOrderPL',
				//需要带什么数据过去
				data:{"select":$("#select").val()},
				//定义返回的数据类型
				dataType:"json",
				//请求成功时调用的函数
				success:function(data){
					//定义变量
					var select=$("#select").val();
					var year=$("#year").val();
					var month=$("month").val();
					var day=$("#day").val();
					var week=$("#week").val();
					//清空年月日周的下拉菜单
					$("#year option").remove();
					$("#month option").remove();
					$("#day option").remove();
					$("#week option").remove();
					
					if(select==="按每年查询"){
						$("#year").show();
						$("#month").hide();
						$("#day").hide();
						$("#week").hide();
						
					}else if(select==="按每月查询"){
						$("#year").show();
						$("#month").show();
						$("#day").hide();
						$("#week").hide();
						
					}else if(select==="按每日查询"){
						$("#year").show();
						$("#month").show();
						$("#day").show();
						$("#week").hide();
					}else if(select==="按每周查询"){
						$("#year").show();
						$("#month").show();
						$("#day").hide();
						$("#week").show();
					}else{
						$("#year").hide();
						$("#month").hide();
						$("#day").hide();
						$("#week").hide();
					}
					
					var dt=new Date();
					var dyear=parseInt(data.year,10);
					var currentYear=parseInt(dt.getFullYear(),10);
					
					$("#year").append("<option>----请选择年----</option>");
					$("#month").append("<option>----请选择月----</option>");
					$("#day").append("<option>----请选择日----</option>");
					$("#week").append("<option>----请选择周----</option>");
					//年的下拉菜单加载
					for(;dyear<=currentYear;dyear++){
						$("#year").append(
								"<option>"+dyear+"年</option>"
								);
					}
					
					//清理表格
					$("table").empty();
					$("#total").text("");
					$("ul").empty();
				}
			});
		});
		
		//当年被改变时
		$("#year").change(function(){
			//获取用户选择的年
			var selectYear=parseInt($("#year").val(),10);
			selectYear=isNull(selectYear);
			$.ajax({
				//配置参数
				//请求方式
				type:'post',
				//需要发起的请求
				url:'countRestaurantOrderPL',
				//需要带什么数据过去
				data:{"year":selectYear},
				//定义返回的数据类型
				dataType:"json",
				//请求成功时调用的函数
				success:function(data){
					
					var dt=new Date();
					var dyear=parseInt(data.year,10);
					var currentYear=parseInt(dt.getFullYear(),10);
					
					var dmonth=parseInt(data.month,10);
					var currentMonth=parseInt(dt.getMonth(),10)+1;
					//清空月的下拉菜单
					$("#month option").remove();
					$("#month").append("<option>----请选择月----</option>");
					
					//清空日的下拉菜单
					$("#day option").remove();
					$("#day").append("<option>----请选择日----</option>");
					
					//清空周的下拉菜单
					$("#week option").remove();
					$("#week").append("<option>----请选择周----</option>");
					
					//月的下拉菜单加载
					if(selectYear===dyear){
						for(;dmonth<=12;dmonth++){
							$("#month").append(
									"<option>"+dmonth+"月</option>"
									);
						}
					}else if(selectYear===currentYear){
						for(var i=1;i<=currentMonth;i++){
							$("#month").append(
									"<option>"+i+"月</option>"
									);
						}
					}else if($("#year").val()==="----请选择年----"){
						
					}else{
						for(var i=1;i<=12;i++){
							$("#month").append(
									"<option>"+i+"月</option>"
									);
						}
					}
					
					$("table").empty();
					$("ul").empty();
					$("#total").text("");
					if($("#select").val()==="按每年查询"){
						
						table(data.order,data.users,data.dishsName,1);			
						$("#total").text("本年的销售额为"+data.total+"元");
						
					}
					
					
				}
			});
		});
		
		//当月被改变时
		$("#month").change(function(){
			//获取用户选择的年
			var selectYear=parseInt($("#year").val(),10);
			//获取用户选择的月
			var selectMonth=parseInt($("#month").val(),10);
			selectYear=isNull(selectYear);
			selectMonth=isNull(selectMonth);
			
			$.ajax({
				//配置参数
				//请求方式
				type:'post',
				//需要发起的请求
				url:'countRestaurantOrderPL',
				//需要带什么数据过去
				data:{"year":selectYear,"month":selectMonth,"select":$("#select").val()},
				//定义返回的数据类型
				dataType:"json",
				//请求成功时调用的函数
				success:function(data){
					//定义参数变量
					var dt=new Date();
					var currentMonth=parseInt(dt.getMonth(),10)+1;
					var currentYear=parseInt(dt.getFullYear(),10);
					var currentDay=parseInt(dt.getDate(),10);
					var dweekCount=parseInt(data.weekCount,10);
					
					if($("#select").val()==="按每周查询"){
						//清空周的下拉菜单
						$("#week option").remove();
						$("#week").append("<option>----请选择周----</option>");
						
						//周的下拉菜单加载
						for(var i=1;i<=dweekCount;i++){
							$("#week").append(
									"<option>"+i+"周</option>"
									);
						}
					}
					//清空日的下拉菜单
					$("#day option").remove();
					$("#day").append("<option>----请选择日----</option>");
					
					//日的下拉菜单加载
					if(selectYear===currentYear&&selectMonth===currentMonth){
						
						for(var i=1;i<=currentDay;i++){
							$("#day").append(
									"<option>"+i+"日</option>"
									);
						}
							
					}else{
					
						switch(selectMonth){
							case 1:
							case 3:
							case 5:
							case 7:
							case 8:
							case 10:
							case 12:
								for(var i=1;i<=31;i++){
									$("#day").append(
											"<option>"+i+"日</option>"
											);
								}
								break;
							case 2:
								if((selectYear%4==0 && selectYear%100!=0) || selectYear%400==0){
									for(var i=1;i<=29;i++){
										$("#day").append(
												"<option>"+i+"日</option>"
												);
									}
								}else{
									for(var i=1;i<=28;i++){
										$("#day").append(
												"<option>"+i+"日</option>"
												);
									}
								}
								break;
							case 4:
							case 6:
							case 9:
							case 11:
								for(var i=1;i<=30;i++){
									$("#day").append(
											"<option>"+i+"日</option>"
											);
								}
								break;
						}
					}
					
					$("table").empty();
					$("ul").empty();
					$("#total").text("");
					if($("#select").val()==="按每月查询"){
						
						table(data.order,data.users,data.dishsName,1);	
						$("#total").text("本月的销售额为"+data.total+"元");
						
					}
					
				}
			});
		});
		
		//当周被改变时
		$("#week").change(function(){
			//获取用户选择的年
			var selectYear=parseInt($("#year").val(),10);
			//获取用户选择的月
			var selectMonth=parseInt($("#month").val(),10);
			//获取用户选择的周
			var selectWeek=parseInt($("#week").val(),10);
			selectYear=isNull(selectYear);
			selectMonth=isNull(selectMonth);
			selectWeek=isNull(selectWeek);
			
			$.ajax({
				//配置参数
				//请求方式
				type:'post',
				//需要发起的请求
				url:'countRestaurantOrderPL',
				//需要带什么数据过去
				data:{"year":selectYear,"month":selectMonth,"week":selectWeek,"select":$("#select").val()},
				//定义返回的数据类型
				dataType:"json",
				//请求成功时调用的函数
				success:function(data){
					
					var dt=new Date();
					var dyear=parseInt(data.year,10);
					var currentYear=parseInt(dt.getFullYear(),10);
					
					var dmonth=parseInt(data.month,10);
					var currentMonth=parseInt(dt.getMonth(),10)+1;
					
					var dweekCount=parseInt(data.weekCount,10);
					
					//清空日的下拉菜单
					$("#day option").remove();
					$("#day").append("<option>----请选择日----</option>");
					
					
					$("table").empty();
					$("ul").empty();
					$("#total").text("");
					if($("#select").val()==="按每周查询"){
						
						table(data.order,data.users,data.dishsName,1);			
						$("#total").text("本周的销售额为"+data.total+"元");
						
					}
					
					
				}
			});
		});
		
		//当日被改变时
		$("#day").change(function(){
			//获取用户选择的年
			var selectYear=parseInt($("#year").val(),10);
			//获取用户选择的月
			var selectMonth=parseInt($("#month").val(),10);
			//获取用户选择的日
			var selectDay=parseInt($("#day").val(),10);
			selectYear=isNull(selectYear);
			selectMonth=isNull(selectMonth);
			selectDay=isNull(selectDay);
			$.ajax({
				//配置参数
				//请求方式
				type:'post',
				//需要发起的请求
				url:'countRestaurantOrderPL',
				//需要带什么数据过去
				data:{"year":selectYear,"month":selectMonth,"day":selectDay},
				//定义返回的数据类型
				dataType:"json",
				//请求成功时调用的函数
				success:function(data){
					
					$("table").empty();
					$("ul").empty();
					$("#total").text("");
					if($("#select").val()==="按每日查询"){
						
						table(data.order,data.users,data.dishsName,1);	
						$("#total").text("本日的销售额为"+data.total+"元");
						
					}
				}
			});
		});
		
		//通过传入的参数生成对应的表格
		function table(order,users,dishsName,start){
			
			$("ul").append(
					"<li>"+
					"<a href='#'>首页</a>"+
					"</li>"+
					"<li>"+
					"<a href='#'>上一页</a>"
					+"</li>"
					);
			
			
			var pageSize = 10;
			var scount=order.length;
			var totalPage =scount%pageSize == 0?Math.ceil(scount/pageSize):Math.ceil(scount/pageSize);
			var startPage=(start-1)*pageSize;
			var endPage=start*pageSize;
			if(start===totalPage){
				endPage=scount;
			}else if(totalPage===0){
				endPage=0;
			}
			
			for(var i=1;i<=totalPage;i++){
				$("ul").append(
						"<li>"+
						"<a href='#'>"+i+"</a>"+
						"</li>"	
				);
			}
			
			
			$("ul").append(
					"<li>"+
					"<a href='#'>下一页</a>"+
					"</li>"+
					"<li>"+
					"<a href='#'>尾页</a>"
					+"</li>"
					);
			
			$("a").click(function(){
				
				var text=$(this).text();
				if(text==="首页"){
					$("table").empty();
					$("ul").empty();
					table(order,users,dishsName,1);
				}else if(text==="尾页"){
					$("table").empty();
					$("ul").empty();
					table(order,users,dishsName,totalPage);
				}else if(text==="上一页"){
					if(start===1){
						
						$(this).attr("disabled",true);
						
					}else{
						$("table").empty();
						$("ul").empty();
						table(order,users,dishsName,start-1);
					}
					
				}else if(text==="下一页"){
					if(start===totalPage){
						
						$(this).attr("disabled",true);
						
					}else{
						$("table").empty();
						$("ul").empty();
						table(order,users,dishsName,start+1);
					}
					
				}else{
					$("table").empty();
					$("ul").empty();
					table(order,users,dishsName,parseInt(text,10));
					
				}
				
			});
			
			$("table").append(
					"<tr>"+
					"<td>订单序号</td>"+
					"<td>用户名字</td>"+
					"<td>购买菜品</td>"+
					"<td>购买数量</td>"+
					"<td>用户地址</td>"+
					"<td>用户评论</td>"+
					"<td>商家回复</td>"+
					"<td>订单总价</td>"+
					"<td>订单时间</td>"+
					"<td>评论时间</td>"+
					"<td>菜品评价</td>"+
					"</tr>"
					)
			
			
			for (var i=startPage;i<endPage;i++){
				for(var i=startPage;i<endPage;i++){
					for(var i=startPage;i<endPage;i++){
						var judgeNum=null;
						if(order[i].judgeNum===0){
							judgeNum="未评价";
						}else if(order[i].judgeNum===1){
							judgeNum="差评";
						}else{
							judgeNum="好评";
						}
						
						
						$("table").append(
								
								"<tr>"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+users[i].uname+"</td>"+
								"<td>"+dishsName[i]+"</td>"+
								"<td>"+order[i].onumber+"</td>"+
								"<td>"+order[i].uaddress+"</td>"+
								"<td>"+order[i].judge+"</td>"+
								"<td>"+order[i].reply+"</td>"+
								"<td>"+order[i].total+"</td>"+
								"<td>"+order[i].odata+"</td>"+
								"<td>"+order[i].date+"</td>"+
								"<td>"+judgeNum+"</td>"+
								"</tr>"
								);
					}
				}
			}
		}
		
		function isNull(s){
			if(isNaN(s)){
				return null;
			}else{
				return s;
			}
			
		}
		
	});
</script>
</head>
<body>
	<select id="select">
		<option>----请选择查询方式----</option>
		<option>按每年查询</option>
		<option>按每月查询</option>
		<option>按每周查询</option>
		<option>按每日查询</option>
	</select>
	<select id="year">
		
	</select>
	<select id="month">
		
	</select>
	<select id="day">
		
	</select>
	
	<select id="week">
		
	</select>
	
	<table>
		
	</table>
		
	<ul class="pagination" >
		
	</ul>
	
	<div id="total">
		
	</div>
</body>
</html>
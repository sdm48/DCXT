<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>确认订单</title>

<link rel="stylesheet" href="../css/bootstrap.min.css">
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="../js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="../themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="../themes/color.css">
<script type="text/javascript"
	src="../js/jquery.min.js"></script>
<script type="text/javascript"
	src="../js/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../js/easyui-lang-zh_CN.js"></script>
<style type="text/css">
table tr,td{
	border:1px solid black;
}
</style>

<script type="text/javascript">

	$(function(){
		
		$.ajax({
			//配置参数
			//请求方式
			type:'post',
			//需要发起的请求
			url:'updateOrderPL',
			//需要带什么数据过去
			data:{},
			//定义返回的数据类型
			dataType:"json",
			//请求成功时调用的函数
			success:function(data){
				
				$("table").empty();
				$("ul").empty();
				
				table(data.order,data.users,data.dishsName,1);
				
				selectAll();
				
			}
		});
		
		
		//通过传入的参数生成对应的表格
		function table(order,users,dishsName,start){
			
			$("ul").append(
					"<li>"+
					"<a href='#'>首页</a>"+
					"</li>"+
					"<li>"+
					"<a href='#'>上一页</a>"
					+"</li>"
					);
			
			
			var pageSize = 10;
			var scount=order.length;
			var totalPage =scount%pageSize == 0?Math.ceil(scount/pageSize):Math.ceil(scount/pageSize);
			var startPage=(start-1)*pageSize;
			var endPage=start*pageSize;
			if(start===totalPage){
				endPage=scount;
			}else if(totalPage===0){
				endPage=0;
			}
			
			for(var i=1;i<=totalPage;i++){
				$("ul").append(
						"<li>"+
						"<a href='#'>"+i+"</a>"+
						"</li>"	
				);
			}
			
			
			$("ul").append(
					"<li>"+
					"<a href='#'>下一页</a>"+
					"</li>"+
					"<li>"+
					"<a href='#'>尾页</a>"
					+"</li>"
					);
			
			$("a").click(function(){
				
				var text=$(this).text();
				if(text==="首页"){
					$("table").empty();
					$("ul").empty();
					table(order,users,dishsName,1);
					selectAll();
				}else if(text==="尾页"){
					$("table").empty();
					$("ul").empty();
					table(order,users,dishsName,totalPage);
					selectAll();
				}else if(text==="上一页"){
					if(start===1){
						
						$(this).attr("disabled",true);
						
					}else{
						$("table").empty();
						$("ul").empty();
						table(order,users,dishsName,start-1);
						selectAll();
					}
					
				}else if(text==="下一页"){
					if(start===totalPage){
						
						$(this).attr("disabled",true);
						
					}else{
						$("table").empty();
						$("ul").empty();
						table(order,users,dishsName,start+1);
						selectAll();
					}
					
				}else{
					$("table").empty();
					$("ul").empty();
					table(order,users,dishsName,parseInt(text,10));
					selectAll();
				}
				
			});
			
			$("table").append(
					"<tr>"+
					"<td><input type='checkbox'/></td>"+
					"<td>订单序号</td>"+
					"<td>用户名字</td>"+
					"<td>购买菜品</td>"+
					"<td>购买数量</td>"+
					"<td>用户地址</td>"+
					"<td>用户评论</td>"+
					"<td>商家回复</td>"+
					"<td>订单总价</td>"+
					"<td>订单时间</td>"+
					"<td>评论时间</td>"+
					"<td>菜品评价</td>"+
					"</tr>"
					)
			
			
			for (var i=startPage;i<endPage;i++){
				for(var i=startPage;i<endPage;i++){
					for(var i=startPage;i<endPage;i++){
						var judgeNum=null;
						if(order[i].judgeNum===0){
							judgeNum="未评价";
						}else if(order[i].judgeNum===1){
							judgeNum="差评";
						}else{
							judgeNum="好评";
						}
						
						
						$("table").append(
								
								"<tr>"+
								"<td><input type='checkbox'/></td>"+
								"<td style='display:none'>"+order[i].oid+"</td>"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+users[i].uname+"</td>"+
								"<td>"+dishsName[i]+"</td>"+
								"<td>"+order[i].onumber+"</td>"+
								"<td>"+order[i].uaddress+"</td>"+
								"<td>"+order[i].judge+"</td>"+
								"<td>"+order[i].reply+"</td>"+
								"<td>"+order[i].total+"</td>"+
								"<td>"+order[i].odata+"</td>"+
								"<td>"+order[i].date+"</td>"+
								"<td>"+judgeNum+"</td>"+
								"</tr>"
								);
					}
				}
			}
		}
		

		
		//给按钮绑定一个事件
		$("button").click(function(){
			//获取table下的input元素
			var input=$("table input[type='checkbox']:checked");
			//计数器
			var count=0;
			//判断当前项是否被选中
			if(input.prop("checked")){
				count++;
			}
			
			if(count==0){
				alert("请最少选择一个!");
			}else{
				var orderId="";
				$("table input[type='checkbox']:checked").each(function (i) {
					orderId+=$(this).parent().next().text()+",";
					
			    });
				order=orderId.substring(0, orderId.length-1);
				$.ajax({
					//配置参数
					//请求方式
					type:'post',
					//需要发起的请求
					url:'updateOrderPL',
					//需要带什么数据过去
					data:{"orderId":order},
					//定义返回的数据类型
					dataType:"json",
					//请求成功时调用的函数
					success:function(data){
						$("strong").text(data.message);
						
						$.ajax({
							//配置参数
							//请求方式
							type:'post',
							//需要发起的请求
							url:'updateOrderPL',
							//需要带什么数据过去
							data:{},
							//定义返回的数据类型
							dataType:"json",
							//请求成功时调用的函数
							success:function(data){
								
								$("table").empty();
								$("ul").empty();
								
								table(data.order,data.users,data.dishsName,1);
								
								selectAll();
								
							}
						});
						
						
					}
				});
			}
		});
		
		function selectAll(){
			//给表格的标题绑定一个全选事件
			$("table input:first").change(function(){
				//获取table下的所有input元素
				var input=$("table input");
				//判断表格标题是否为true
				if($(this).prop("checked")){
					input.each(function(){
						//直接全部变成true
						$(this).prop("checked",true);
					});
				}else{
					$.each(input,function(i,n){
						//直接全部变成false
						$(n).prop("checked",false);
					});
				}		
			});
		}
		
	});
	
</script>
</head>
<body>
		
	<table>
		
	</table>
		
	<ul class="pagination" >
		
	</ul><br/>
	<button>确定接单</button>
	<strong style="color: red"></strong>
</body>
</html>
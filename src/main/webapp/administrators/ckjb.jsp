<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="themes/color.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
$(function(){
	$('#tb').datagrid({    
	    url:'selecttip',
	    pagination:true,
	    fitColumns:true,
	    rownumbers:true,
	    pageNumber:1,
	    pageSize:4,
	    pageList:[4,8,12],
	    columns:[[    
	        {field:'tid',title:'举报id',width:100,hidden:true},    
	        {field:'oid',title:'订单id',width:100},
	        {field:'tcontent',title:'举报内容',width:100},
	        {field:'date',title:'举报时间',width:100},		       
	        
	    ]]    
	});  
})
</script>
<body>
	<div class="easyui-panel" id='div1'>
 		<table id='tb'  ></table>
	</div>  
</body>
</html>
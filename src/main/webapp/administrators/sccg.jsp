<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="themes/color.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
$(function(){
	$('#p').panel('close');
	$('#p1').panel('close');
	//点击删除按钮触发
	$('#btn1').linkbutton({
		onClick : function() {
			var rid = $('#ss').val()
			if(rid!=0){
				$.messager.confirm('确认对话框', '您想要删除该餐馆吗？', function(r){
					if (r){
						$.ajax({
							  url: 'deletearest',
							  type:'post',
							  dataType: 'text',
							  data: {"rid":rid},
							  success: function(r){
									
							  }
							});
					   alert("此餐馆已被禁用！")
					}
				});
			}else{
				alert("请输入一个餐馆id")
			}
			
		}
			
	});
	
	$('#btn2').linkbutton({
		onClick:function(){
			$('#p').panel('open'),
			$('#p1').panel('open');
			$('#tb').datagrid({    
			    url:'selectrestjinyong',
			    pagination:true,
			    fitColumns:true,
			    rownumbers:true,
			    pageNumber:1,
			    pageSize:2,
			    pageList:[2,4,6],
			    columns:[[  
			                {field:'checkbox',checkbox:true},
					        {field:'rid',title:'餐馆id',width:100},    
					        {field:'rname',title:'餐馆名称',width:100},
					        {field:'rphone',title:'餐馆电话',width:100},
					        {field:'raddress',title:'餐馆地址',width:100},		       
					        {field:'workdate',title:'餐馆注册时间',width:100},
					        {field:'star',title:'餐馆星级',width:100},
					        {field:'isopen',title:'餐馆开张状态(0是关闭,1是营业,2是封禁)',width:100}    
					    ]]    
			});  
		}
	});
	
	$('#btn3').linkbutton({
		onClick:function(){
			
			var row = $('#tb').datagrid('getChecked');
			var rid = row[0].rid;
			
			$.messager.confirm('确认对话框', '您确认要解封吗？', function(r){
				if (r){
					$.ajax({
						url: 'jiefengrest',
						  type:'post',
						  dataType: 'text',
						  data: {"rid":rid},
						  success: function(r){
							  $('#tb').datagrid('reload');
						  }
						});
				   alert("解封成功")
				}
			});
			
		}
	})
	
})
</script>
<body>
<<<<<<< HEAD
<p>请输入一个餐馆id：</p>
=======
	<p>请输入一个餐馆id</p>
>>>>>>> branch 'master' of https://git.oschina.net/sdm48/DCXT.git
	<input id="ss" ></input>
		<a id="btn1" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">确认删除</a><br/>
		<a id="btn2" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查看被禁用的餐馆</a>
		<div id="p" class="easyui-panel" >   
    		<table id="tb"></table>
		</div>
		<div id="p1" class="easyui-panel" > 
			<a id="btn3" href="#" class="easyui-linkbutton" >解封餐馆</a>  
		 </div> 


		  
</body>
</html>
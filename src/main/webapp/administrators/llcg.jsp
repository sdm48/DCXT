<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="themes/icon.css">
<script type="text/javascript"
	src="js/jquery.min.js"></script>
<script type="text/javascript"
	src="js/jquery.easyui.min.js"></script>
</head>
<script type="text/javascript">
     //jquery代码
     $(function(){
    	//为查询餐馆表设置属性
     	$('#selrest').datagrid({    
     	    url:'selres',    
     	    columns:[[  
     	        //{field:'checkbox',checkbox:true},      
     	        {field:'rid',title:'餐馆数据库中的id',width:100,hidden:true},    
     	        {field:'rname',title:'餐馆名',width:100},    
     	        {field:'rphone',title:'餐馆电话',width:100}, 
     	        {field:'raddress',title:'餐馆地址',width:100}, 
     	        {field:'uid',title:'餐馆老板的uid',width:100},
     	        {field:'workdate',title:'餐馆注册时间',width:100},
     	        {field:'star',title:'餐馆星级',width:100},
     	        {field:'isopen',title:'状态(1营业，0关门，2禁用)',width:100}
     	    ]],
     	    //显示行号列
     	    rownumbers:true,
     	    fitColumns:true,
     	    loadMsg:'请耐心等待',
     	    pagination:true,
     	    pageNumber:1,
     	    pageSize:4,
     	    pageList:[4,8,12]
     	});  
     })
    
</script>
<body>
  <!-- 查询所有餐馆基本信息显示的表格 -->
		<div id="contableres" class="easyui-panel">
		  <table id="selrest">
		  </table>
        </div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="themes/icon.css">
<script type="text/javascript"
	src="js/jquery.min.js"></script>
<script type="text/javascript"
	src="js/jquery.easyui.min.js"></script>
</head>
<script type="text/javascript">
    

     //js代码，要写在jquery代码外面
     //解封某账户
     function openUser(index){
    	 //选中解封账户那一行 
    	 $('#selclosedusert').datagrid('selectRow',index);  
    	 //获取这一行
    	 var row = $('#selclosedusert').datagrid('getSelected');
    	 //获取这个被封账户的uid,也是要解封账户的uid
    	 var openuid=row.uid;
    	 //获取这个被封账户被封之前的urole
    	 var urole=row.urole;
    	 //弹窗 确定要解封账户吗 
    	 $.messager.confirm('确认','您确认想要解封此账户吗？',function(r){    
 		    if (r){ 
 		       
 		        $.ajax({
   				  url: "openuser",
   				  type:'post',
   				  dataType: 'text',
   				  data: {"openuid":openuid,"urole":urole},
   				  success: function(req){
   					 alert('解封 成功');  
   	 		       //解封某个用户后，查询所有已封账户表重载 
   	  		        $('#selclosedusert').datagrid("load");
   				  }
   				});
 		    }    
		 });  
    	 
      } 
      
     
     //这里面的是jquery代码  
     $(function(){
    	 //此用户不存在  隐藏 
    	 $('#span1').hide();
    	 //此用户已被封禁 隐藏
    	 $('#span2').hide();
    	 //不能封禁管理员 隐藏 
    	 $('#span3').hide();
    	 //包含 将封用户表的pannel关闭  
    	 $('#contable').panel('close');
    	 //包含  查询所有已封用户表的 pannel关闭 
    	 $('#conselcloseduser').panel('close');


    	 //重写一个validatebox的验证方式，必须填正整数
    	 $.extend($.fn.validatebox.defaults.rules, {
    		 integer: {// 验证正整数 
                 validator: function (value) {
                	 return /^[+]?[1-9]+\d*$/i.test(value);
                 },
                 message: '请输入正整数'
             }
    	 });
    	 //验证框的属性设置
    	 $('#iuid').validatebox({    
    		    required: true,
    		    tipPosition:'left',
    		    missingMessage:'请输入用户id',
    		    validType: 'integer'   
    		});  
    	 
    	// 收索按钮点击时触发事件
    	 $('#btn').linkbutton({
    		 
    		 onClick:function(){
    			 //获取验证框是否通过验证
    			 var flag=$('#iuid').validatebox('isValid');
    			 //取得收索框 的uid值 
    			 var uid=$('#iuid').val();
    			 //为查询将封用户表设置发送的请求 "selclouser"
    			 var opts = $("#clousert").datagrid("options");
    			 opts.url = "selclouser";
    			//如果搜索框通过验证,发送ajax请求 
    			 if(flag){
	    			 $.ajax({
	    				  url: "selclouser",
	    				  type:'post',
	    				  dataType: 'json',
	    				  data: {"uid":uid},
	    				  success: function(r){
	    					if( r.total!="1"){
	    						$('#span1').show();
	    						$('#span2').hide();
	    						$('#span3').hide();
	    						$('#contable').panel('close');
	    					}else{
	    						if(r.rows[0].uid=="1"){
	    							$('#span3').show();
	    							$('#span1').hide();
	    							$('#span2').hide();
	    							$('#contable').panel('close');
	    						}else{
	    							if(r.rows[0].urole=="0"){
	    								$('#span1').hide();
	    								$('#span3').hide();
		    							$('#span2').show();
		    							$('#contable').panel('close');
		    						}else{
		    							$('#clousert').datagrid('reload');
			    						$('#contable').panel('open');
			    						$('#span1').hide();
			    						$('#span2').hide();
			    						$('#span3').hide();
		    						}
	    						}
	    						$('#span1').hide();
	    						
	    					}
	    				  }
	    				});
    			 }
    		 }
    	 })
    	 
    	 
    	 // 封禁用户按钮点击时触发事件
    	 $('#btnclo').linkbutton({
    		 
    		 onClick:function(){
    			 //获取文本域中输入的封号原因
    			 var cloreason=$('#cloarea').val();
    			 
    			 $.messager.confirm('确认','您确认想要封禁此账户吗？',function(r){    
    	    		    if (r){ 
    	    		    	
    	    		        
    	    		        //获取查询出来的将封账户的uid
    	    		        var clouid= $('#clousert').datagrid('getData').rows[0].uid
    	    		        //获取查询出来的将封账户的urole
    	    		        var clourole= $('#clousert').datagrid('getData').rows[0].urole
    	    		        //发送ajax请求，将要封账户id （clouid） 作为参数传到后端
    	    		        $.ajax({
    	      				  url: "clouser",
    	      				  type:'post',
    	      				  dataType: 'text',
    	      				  data: {"clouid":clouid,"clourole":clourole,"cloreason":cloreason},
    	      				  success: function(req){
    	      					 alert('封禁成功'); 
    	      					 $('#contable').panel('close');
    		    	    		 $('#iuid').val("");
    		    	    		 $('#selclosedusert').datagrid("load");
    	      				  }
    	      				});
	    	    		   
    	    		    }    
    	    		    $('#cloarea').val("");
    			 });  
    			 
    		 }
    	 })
    	 
    	 
    	
    	 //将封用户信息表属性配置
    	 $('#clousert').datagrid({    
	     	    url:'',  
	     	    
	     	   

	     	    columns:[[  
	     	        {field:'uid',title:'用户数据库中的id',width:100,hidden:true},    
	     	        {field:'uname',title:'用户登录名',width:100},    
	     	        {field:'upasw',title:'用户密码',width:100}, 
	     	        {field:'uphone',title:'用户手机',width:100}, 
	     	        {field:'urole',title:'用户角色(2代表老板，3代表顾客)',width:100}
	     	    ]],
	     	    //显示行号列
	     	    rownumbers:true,
	     	    fitColumns:true,
	     	    loadMsg:'请耐心等待',
	     	  
	     	});  
    	
    	  function loadData() {
    		    var options = $('#gridlistRanking').datagrid('options');
    		    options.url = '/api/AirQuality/PointRanking/getRanking';
    		    options.queryParams = { selectAreaLoc: selectAreaLoc, selectPointAvg: selectPointAvg };
    		    $('#gridlistRanking').datagrid(options);

    		}
    	
    	//设置左边的小功能选择树属性
    	 $('#clousertree').tree({
     		onClick: function(node){
     			//点此节点，隐藏封号功能,且显示查询被封账户
     			if(node.id==3){
     				$('#concloseuser').panel('close');
     				$('#conselcloseduser').panel('open');
     			}
     			//点此节点，显示封号功能，且隐藏查询被封账户
     			if(node.id==2){
     				$('#concloseuser').panel('open');
     				$('#conselcloseduser').panel('close');
     			}
     		}
     	});
    	
    	
    	 //为查询已封账户表设置属性
     	$('#selclosedusert').datagrid({    
     	    url:'selcloseduser',  
     	   autoRowHeight:false,
     	    columns:[[  
     	       // {field:'checkbox',checkbox:true},      
     	        {field:'cid',title:'封号操作的cid',width:100,hidden:true},    
     	        {field:'mid',title:'封号的管理员id',width:100},    
     	        {field:'uid',title:'被封用户的id',width:100}, 
     	        {field:'urole',title:'被封前角色(2为老板，3为顾客)',width:120}, 
     	        {field:'cdate',title:'封号操作的时间',width:100},
     	        {field:'ccontent',title:'封号的原因',width:100},
     	        //增加一个解封账户列
     	        {field:'operate',title:'解封账户',width:$(this).width()*0.1,  
     	           formatter:function(value, row, index){  
     	               var str = '<a href="javascript:;" name="opera"  onclick="openUser('+index+')"  class="easyui-linkbutton" ></a>';  
     	               return str;  
     	         }}  
     	    ]],
     	    
     	    //一些函数写在这里面，写外面没效果,作用域 。
     	    onLoadSuccess:function(data){ 
     	        $("a[name='opera']").linkbutton({text:'解除封禁',plain:true,iconCls:'icon-man'});    
     	    }, 
     	
     	    //点击行时不会选中该行 
     	    checkOnSelect:false,
     	    //显示行号列
     	    rownumbers:true,
     	    fitColumns:true,
     	    loadMsg:'请耐心等待',
     	    pagination:true,
     	    pageNumber:1,
     	    pageSize:4,
     	    pageList:[4,8,12]
     	});  
    	 
    	

     })
</script>
<!-- 创建布局 -->
<body class="easyui-layout">
    <div data-options="region:'west',border:false,collapsible:false" style="width:150px;">
      <!--树结构，封禁账户或查询已封账户的选择 -->
       <ul id="clousertree" class="easyui-tree">   
		    <li data-options="id:1,iconCls:'icon-cut'" >   
		        <span>封号管理</span>   
		        <ul>   
		            <li data-options="id:2,iconCls:'icon-cancel'">   
		                <span>封禁账户</span>   
		            </li>   
		            <li data-options="id:3,iconCls:'icon-search'">   
		                <span>查询已封账户</span>   
		            </li>   
		        </ul>   
		    </li>   
		   
		</ul>  
    </div>   
   
    <div data-options="region:'center',title:'封号功能',border:false" style="background:#eee;">
       
         <!-- 这个panel包含封号功能的操作页面 -->
         <div id="concloseuser" class="easyui-panel">
            <!-- 输入框中输入要封用户的id -->
			输入要封禁用户的UID：<input id="iuid" /><a id="btn" href="#" 
			class="easyui-linkbutton" data-options="iconCls:'icon-search'">确定</a>  
			<span id="span1">此用户不存在</span><span id="span2">该账号已被封禁</span>
			<span id="span3">不能封禁管理员</span>
			<!-- 查询出要封的某个用户展现的表 -->
			<div id="contable" class="easyui-panel">
			  <table id="clousert">
			  </table>
			     封号原因：<textarea id="cloarea" rows="3" cols="30"></textarea>
			  <a id="btnclo" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'">封禁此用户</a>  
            </div>
        </div>
        
        <!-- 这个panel包含查询被封的所有账户页面 -->
        <div id="conselcloseduser" class="easyui-panel">
            
		       <table id="selclosedusert">
		       </table>
            
        </div>
     </div>



</body>
</html>
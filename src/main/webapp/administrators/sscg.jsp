<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="themes/color.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
	$(function() {
		//隐藏面板
		$('#div1').panel('close');
		
		$('#tb').datagrid({    
		    url:'',
		    fitColumns:true,
		    columns:[[    
		        {field:'rid',title:'餐馆id',width:100},    
		        {field:'rname',title:'餐馆名称',width:100},
		        {field:'rphone',title:'餐馆电话',width:100},
		        {field:'raddress',title:'餐馆地址',width:100},		       
		        {field:'workdate',title:'餐馆注册时间',width:100},
		        {field:'star',title:'餐馆星级',width:100},
		        {field:'isopen',title:'餐馆开张状态',width:100}    
		    ]]    
		});  
		//点击搜索按钮触发事件
		$('#btn').linkbutton({
			onClick : function() {				
				var rid = $('#ss').val()
			
			 var opts = $("#tb").datagrid("options");
			 opts.url = "selectarest";
				if(rid!=0){
					
					$('#div1').panel('open');
					$.ajax({
						  url: 'selectarest',
						  type:'post',
						  dataType: 'text',
						  data: {"rid":rid},
						  success: function(r){
							  $('#tb').datagrid('reload');
						  }
						});
				}else{						
					alert("请输入一个餐馆id")			
				}
				
				
			}
				
		});
		
	})
</script>
<body>
<p>请输入一个餐馆id：</p>
	<input id="ss" ></input>
	<a id="btn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">搜索</a>  
 	<div class="easyui-panel" id='div1'>
 		<table id='tb'  ></table>
	</div>  


</body>
</html>
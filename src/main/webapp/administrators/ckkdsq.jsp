<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="themes/color.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
$(function(){
	$('#tb').datagrid({    
	    url:'selectapply',
	    pagination:true,
	    fitColumns:true,
	    rownumbers:true,
	    pageNumber:1,
	    pageSize:4,
	    pageList:[4,8,12],
	    columns:[[  
	        {field:'checkbox',title:'申请用户id',width:100,checkbox:true},
	        {field:'aid',title:'申请id',width:100,hidden:true},    
	        {field:'uid',title:'申请用户id',width:100},
	        {field:'aispass',title:'申请是否通过',width:100},
	        {field:'mid',title:'管理员id',width:100},
	        {field:'adate',title:'申请时间',width:100},
	        {field:'passdate',title:'通过时间',width:100},
	        
	    ]]    
	});
	//通过申请
	$('#btn1').linkbutton({
		onClick : function() {
			
			var selRows = $('#tb').datagrid('getChecked');
			//申请id
			//var aid = selRows[0].aid;
			//var aid1 = selRows[1].aid;
			var str=[];
			
			
			$.each(selRows,function (index,domEle){
				if(index==0){
					str.push(selRows[index].aid);
				}else{
					str.push(','+selRows[index].aid);
				}
				
				  

				});
			
			var aids=str.join('')
			
			$.messager.confirm('确认对话框', '您想要通过吗？', function(r){
				if (r){
					$.ajax({
						  url: 'passapply',
						  type:'post',
						  dataType: 'text',
						  data: {"aids":aids},
						  success: function(r){
							  $('#tb').datagrid('reload');
						  }
						});
				   alert("通过成功")
				}
			});
		}
			
	});
	//不通过申请
	$('#btn2').linkbutton({
		onClick : function() {
			
			var selRows = $('#tb').datagrid('getChecked');
			//bu申请id
			//var aid = selRows[0].aid;
			//var aid1 = selRows[1].aid;
			var str=[];
			
			
			$.each(selRows,function (index,domEle){
				if(index==0){
					str.push(selRows[index].aid);
				}else{
					str.push(','+selRows[index].aid);
				}
				
				  

				});
			
			var aids=str.join('')
				
			$.messager.confirm('确认对话框', '您想要不通过吗？', function(r){
				if (r){
					$.ajax({
						  url: 'notpassapply',
						  type:'post',
						  dataType: 'text',
						  data: {"aids":aids},
						  success: function(r){
							  $('#tb').datagrid('reload');
						  }
						});
				   alert("通过不成功")
				}
			});
		}
			
	});
})
</script>
<body>
	<div class="easyui-panel" id='div1'>
 		<table id='tb'  ></table>
	</div>
	<a id="btn1" href="#" class="easyui-linkbutton" >通过</a>
	<a id="btn2" href="#" class="easyui-linkbutton" >不通过</a>  
	  
	  
</body>
</html>
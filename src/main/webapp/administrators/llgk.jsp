<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="themes/icon.css">
<script type="text/javascript"
	src="js/jquery.min.js"></script>
<script type="text/javascript"
	src="js/jquery.easyui.min.js"></script>
</head>
<script type="text/javascript">
    $(function(){
    	
    	$('#contableb').panel('close');
    	
       //为查询顾客表设置属性
    	$('#selct').datagrid({    
    	    url:'selcus',    
    	    columns:[[  
    	        //{field:'checkbox',checkbox:true},      
    	        {field:'uid',title:'用户数据库中的id',width:100,hidden:true},    
    	        {field:'uname',title:'用户登录名',width:100},    
    	        {field:'upasw',title:'用户密码',width:100}, 
    	        {field:'uphone',title:'用户手机',width:100}, 
    	        {field:'urole',title:'用户角色(2代表老板，3代表顾客)',width:100}
    	    ]],
    	    //显示行号列
    	    rownumbers:true,
    	    fitColumns:true,
    	    loadMsg:'请耐心等待',
    	    pagination:true,
    	    pageNumber:1,
    	    pageSize:4,
    	    pageList:[4,8,12]
    	});  
       
       
    	//为查询老板表设置属性
    	$('#selbt').datagrid({    
    	    url:'selboss',    
    	    columns:[[  
    	        //{field:'checkbox',checkbox:true},      
    	        {field:'uid',title:'用户数据库中的id',width:100,hidden:true},    
    	        {field:'uname',title:'用户登录名',width:100},    
    	        {field:'upasw',title:'用户密码',width:100}, 
    	        {field:'uphone',title:'用户手机',width:100}, 
    	        {field:'urole',title:'用户角色(2代表老板，3代表顾客)',width:100}
    	    ]],
    	    //显示行号列
    	    rownumbers:true,
    	    fitColumns:true,
    	    loadMsg:'请耐心等待',
    	    pagination:true,
    	    pageNumber:1,
    	    pageSize:4,
    	    pageList:[4,8,12]
    	});  
    	
    	//设置左边的小功能选择树属性
    	$('#selcul').tree({
    		onClick: function(node){
    			//点此节点，隐藏顾客表,且显示老板表
    			if(node.id==3){
    				$('#contable').panel('close');
    				$('#contableb').panel('open');
    			}
    			//点此节点，显示顾客表，且隐藏老板表
    			if(node.id==2){
    				$('#contable').panel('open');
    				$('#contableb').panel('close');
    			}
    		}
    	});




    })
</script>
<!-- 创建布局 -->
<body class="easyui-layout">
    <div data-options="region:'west',border:false,collapsible:false" style="width:150px;">
      <!--树结构，查询顾客或老板的选择 -->
       <ul id="selcul" class="easyui-tree">   
		    <li data-options="id:1,iconCls:'icon-search'" >   
		        <span>查询对象</span>   
		        <ul>   
		            <li data-options="id:2,iconCls:'icon-man'">   
		                <span>查询顾客信息</span>   
		            </li>   
		            <li data-options="id:3,iconCls:'icon-man'">   
		                <span>查询老板信息</span>   
		            </li>   
		        </ul>   
		    </li>   
		   
		</ul>  
    </div>   
    <div data-options="region:'center',title:'用户信息列表',border:false" style="background:#eee;">
          <!-- 查询所有注册的顾客信息显示的表格 -->
		<div id="contable" class="easyui-panel">
		  <table id="selct">
		  </table>
        </div>
        
        <!-- 查询所有注册的老板信息显示的表格 -->
		<div id="contableb" class="easyui-panel">
		  <table id="selbt">
		  </table>
        </div>
    </div>   
  

  






  
</body>
</html>
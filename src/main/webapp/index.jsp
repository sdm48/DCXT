<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>首页</title>
		<link rel="stylesheet" type="text/css" href="js/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="themes/icon.css">
		<link rel="stylesheet" type="text/css" href="themes/color.css">
		<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
	</head>
		<script type="text/javascript">
			$(function(){
				$("#register").hide();
				$("#loginWindow").show();
				$('#registerWindow').window({    
					title : '界面窗口',
					collapsible : false,
					minimizable : false,
					maximizable : false,
					closable : false,
					draggable : true,
					resizable : false,
				    width:400,    
				    height:300,    
				    modal:true   
				});
				$($("input[type='text']").get(1)).blur(function(){
					var password =$($("input[type='text']").get(1)).val();
				})
				$("#regButton").click(function(){
					$("#register").show();
					$("#loginWindow").hide();
				});
				$("#returnLogin").click(function(){
					$("#register").hide();
					$("#loginWindow").show();
				});
				$("#registerButton").click(function(){
					$.ajax({
						url : 'register',
						data : {
							regName : $("input[name='uname']").val(),
							regPassword : $("input[name='upswd']").val(),
							regPasswordAgain : $("#registerPasswordAgain").val()
						},
						dataType : 'json',
						type : 'post',
						success : function(r){
							 if(r.code === 1){
									$.messager.confirm('确认','注册名或密码或再次输入密码未输入,注册失败!',function(r){
										$("input[name='uname']").val('');
										$("input[name='upswd']").val('');
										$("#registerPasswordAgain").val('');
									}); 
							} else if(r.code === 2) {
								$.messager.confirm('确认','两次输入的密码不一致,注册失败!',function(r){
									$("input[name='uname']").val('');
									$("input[name='upswd']").val('');
									$("#registerPasswordAgain").val('');
								});
							} else if(r.code === 3 ) {
								$("#loginWindow").show();
								$.messager.confirm('确认','注册成功!',function(r){
									if(r) {
										window.location.reload();
									}
									$("input[name='uname']").val('');
									$("input[name='upswd']").val('');
									$("#registerPasswordAgain").val('');
								});
								$("#register").hide();
							}
						}
					});
				});
				$("#loginButton").click(function(){
					$.ajax({
						url : 'login',
						data : {
							uname : $("#loginName").val(),
							upswd : $("#loginPassword").val(),
							uyanzheng : $("#loginYanZheng").val()
						},
						dataType : 'json',
						type : 'post',
						success : function(r){
							if(r.code == 1) {
								location.href = 'main.jsp';
							} else if(r.code == 2){
								$.messager.confirm('确认','验证码错误,登录失败!',function(r){
										window.location.reload();
								});
								$("#loginYanZheng").val('');
								$("#loginName").val('');
								$("#loginPassword").val('');
							} else {
								$.messager.confirm('确认','用户名或密码错误,登录失败!',function(r){
								});
								$("#loginYanZheng").val('');
								$("#loginName").val('');
								$("#loginPassword").val('');
								window.location.reload();
							}
						}
					});
				});
			})
		</script>
		<script type="text/javascript">
			function changeImg(){
				window.location.reload();
			}
		</script>
	<body>
		<div id="registerWindow" class="row form-group">
			<div style="margin-top: 40px;" align="center" id="register">
				<input name="uname" placeholder="请输入注册名" style="width: 150px; display: block;" class="form-control" type="text" style="display: block;"/>
				<input name="upswd" placeholder="请输入注册密码" style="width: 150px; margin-top:10px ; display: block;" class="form-control" type="text" style="display: block;"/>
				<input class="form-control" placeholder="请再次输入密码" style="width: 150px; margin-top:10px ; display: block;" id="registerPasswordAgain" type="text" style="display: block;"/>
				<button type="button" id="registerButton" class="btn btn-default" style="margin-top:10px ; display: block;">确认注册</button>
				<button type="button" id="returnLogin" class="btn btn-link" >返回到登陆界面</button>
			</div>
			<div id='loginWindow' style="margin-top: 40px;" align="center" id="login">
				<input id="loginName" placeholder="请输入用户名" class="form-control" type="text" style="width: 40%; margin-top:10px ;display: block;" />
				<input id="loginPassword" placeholder="请输入密码" class="form-control" type="password" style="width:40%; margin-top:10px; display: block;" />
				<img border=0 src="image.jsp" onclick="changeImg()" style="margin-bottom: 5px"/>
				<input id="loginYanZheng" placeholder="请输入验证码" class="form-control" type="text" style="width:30%; margin-top:10px; display: inline;" /><br/>
				<button type="button" id="loginButton" class="btn btn-default" style="margin-top: 10px">登录</button>
				<button type="button" id="regButton" class="btn btn-default" style="margin-top: 10px">注册</button>
			</div>
		</div>
	</body>
</html>
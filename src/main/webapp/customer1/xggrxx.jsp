<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<head>
		<link rel="stylesheet" href="js/bootstrap.min.css">
		<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>修改个人信息</title>
		</head>
		<script type="text/javascript">
	</script>
	
	<body>
		<form action="xggrxx" role="form" class="form-inline" style="margin-left: 35%">
			<span style='margin-right: 50px'>原名字：${sessionScope.customerly.getCname()}</span><input placeholder="修改后的名字" style="display: block;" name="Cname" class="form-control" type="text" />
			<span style='margin-right: 50px'>原地址：${sessionScope.customerly.getCaddress()}</span><input placeholder="修改后的地址" style="display: block;" name="Caddress" class="form-control" type="text" /> 
			<span style='margin-right: 50px'>原电话号码：${sessionScope.customerly.getCphone()}</span><input placeholder="修改后的电话" style="display: block;" name="Cphone" class="form-control" type="text" />
			<span style='margin-right: 50px'>原性别：${sessionScope.customerly.getCsex()}</span><input placeholder="修改后的性别" style="display: block;" name="Csex" class="form-control" type="text" />
			<span style='margin-right: 50px'>原密码：${sessionScope.customerly.getUpassword()}</span><input placeholder="修改后的密码" style="display: block;" name="Upassword" class="form-control" type="password" />
		<input type="submit" class="btn btn-default" value="确定"/>
		</form>
	</body>
</html>

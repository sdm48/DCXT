<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" href="js/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="themes/icon.css">
		<link rel="stylesheet" type="text/css" href="themes/color.css">
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
	</head>
		<script type="text/javascript">
			$(function(){
				$("#thisTr").hide();
				$.ajax({
					url : 'searchSname',
					data : {
					},
					dataType : 'json',
					type : 'post',
					success : function(r){
						$("#sel").append("<option>---请选择---</option>");
						for (var i=0; i<r.snames.length; i++) {
							$("#sel").append("<option>"+r.snames[i]+"</option>");
						}
						$("#sel").change(function(){
							if($("#sel").val() === '烧菜') {
								$.ajax({
									url : 'searchSname',
									data : {
										sname : $("#sel").val()
									},
									dataType : 'json',
									type : 'post',
									success : function(r){
										$("td").remove();
										$("tr").remove();
										var name=$("<td style='width:100px'>菜名</td>");
										var num=$("<td style='width:100px'>菜数量</td>");
										var sort=$("<td style='width:100px'>菜种类</td>");
										var restId=$("<td style='width:100px'>餐馆id</td>");
										var price=$("<td style='width:100px'>菜价格</td>");
										var tr = $("<tr></tr>");
										tr.append(name);
										tr.append(num);
										tr.append(sort);
										tr.append(restId);
										tr.append(price);
										$("table").append(tr);
										for (var i=0; i<r.dishsBySname.length; i++) {
											var tName = $("<td>"+r.dishsBySname[i].dName+"</td>");
											var tNumber = $("<td>"+r.dishsBySname[i].dNumber+"</td>");
											var tSname = $("<td>"+r.dishsBySname[i].sName+"</td>");
											var tRid = $("<td>"+r.dishsBySname[i].rid+"</td>");
											var tRprice = $("<td>"+r.dishsBySname[i].rPrice+"</td>");
											var tr = $("<tr></tr>");
											tr.append(tName);
											tr.append(tNumber);
											tr.append(tSname);
											tr.append(tRid);
											tr.append(tRprice);
											$("table").append(tr);
										}
									}
								});
							} else if($("#sel").val() === '蔬菜类') {
								$.ajax({
									url : 'searchSname',
									data : {
										sname : $("#sel").val()
									},
									dataType : 'json',
									type : 'post',
									success : function(r){
										$("td").remove();
										$("tr").remove();
										var name=$("<td>菜名</td>");
										var num=$("<td>菜数量</td>");
										var sort=$("<td>菜种类</td>");
										var restId=$("<td>餐馆id</td>");
										var price=$("<td>菜价格</td>");
										var tr = $("<tr></tr>");
										tr.append(name);
										tr.append(num);
										tr.append(sort);
										tr.append(restId);
										tr.append(price);
										$("table").append(tr);
										for (var i=0; i<r.dishsBySname.length; i++) {
											//var tCheckbox = $("<td style='width:100px'><input type='checkbox' id="+r.dishsBySname[i].did+" /></td>");
											var tName = $("<td>"+r.dishsBySname[i].dName+"</td>");
											var tNumber = $("<td>"+r.dishsBySname[i].dNumber+"</td>");
											var tSname = $("<td>"+r.dishsBySname[i].sName+"</td>");
											var tRid = $("<td>"+r.dishsBySname[i].rid+"</td>");
											var tRprice = $("<td>"+r.dishsBySname[i].rPrice+"</td>");
											var tr = $("<tr></tr>");
											var div = $("<div id='shucai'></div>");
											tr.append(tName);
											tr.append(tNumber);
											tr.append(tSname);
											tr.append(tRid);
											tr.append(tRprice);
											$("table").append(tr);
										}
									}
								});
							} else if($("#sel").val() === '青菜') {
								$.ajax({
									url : 'searchSname',
									data : {
										sname : $("#sel").val()
									},
									dataType : 'json',
									type : 'post',
									success : function(r){
										$("td").remove();
										$("tr").remove();
										var name=$("<td>菜名</td>");
										var num=$("<td>菜数量</td>");
										var sort=$("<td>菜种类</td>");
										var restId=$("<td>餐馆id</td>");
										var price=$("<td>菜价格</td>");
										var tr = $("<tr></tr>");
										tr.append(name);
										tr.append(num);
										tr.append(sort);
										tr.append(restId);
										tr.append(price);
										$("table").append(tr);
										for (var i=0; i<r.dishsBySname.length; i++) {
											var tName = $("<td>"+r.dishsBySname[i].dName+"</td>");
											var tNumber = $("<td>"+r.dishsBySname[i].dNumber+"</td>");
											var tSname = $("<td>"+r.dishsBySname[i].sName+"</td>");
											var tRid = $("<td>"+r.dishsBySname[i].rid+"</td>");
											var tRprice = $("<td>"+r.dishsBySname[i].rPrice+"</td>");
											var tr = $("<tr></tr>");
											var div = $("<div id='qingcai'></div>");
											tr.append(tName);
											tr.append(tNumber);
											tr.append(tSname);
											tr.append(tRid);
											tr.append(tRprice);
											$("table").append(tr);
										}
									}
								});
							} else if($("#sel").val() === '炒菜') {
							}
								$.ajax({
									url : 'searchSname',
									data : {
										sname : $("#sel").val()
									},
									dataType : 'json',
									type : 'post',
									success : function(r){
										$("td").remove();
										$("tr").remove();
										var name=$("<td>菜名</td>");
										var num=$("<td>菜数量</td>");
										var sort=$("<td>菜种类</td>");
										var restId=$("<td>餐馆id</td>");
										var price=$("<td>菜价格</td>");
										var tr = $("<tr></tr>");
										tr.append(name);
										tr.append(num);
										tr.append(sort);
										tr.append(restId);
										tr.append(price);
										$("table").append(tr);
										for (var i=0; i<r.dishsBySname.length; i++) {
											var tName = $("<td>"+r.dishsBySname[i].dName+"</td>");
											var tNumber = $("<td>"+r.dishsBySname[i].dNumber+"</td>");
											var tSname = $("<td>"+r.dishsBySname[i].sName+"</td>");
											var tRid = $("<td>"+r.dishsBySname[i].rid+"</td>");
											var tRprice = $("<td>"+r.dishsBySname[i].rPrice+"</td>");
											var tr = $("<tr></tr>");
											var div = $("<div id='qingcai'></div>");
											tr.append(tName);
											tr.append(tNumber);
											tr.append(tSname);
											tr.append(tRid);
											tr.append(tRprice);
											$("table").append(tr);
										}
									}
								});
						})
					}
				});
				$("#search").click(function(){
					$.ajax({
						url : 'searchSname',
						data : {
							searchName : $("input[type='text']").val()
						},
						dataType : 'json',
						type : 'post',
						success : function(r){
							console.log($("input[type='text']").val(""));
							$("tr").remove();
							var name=$("<td>菜名</td>");
							var num=$("<td>菜数量</td>");
							var sort=$("<td>菜种类</td>");
							var restId=$("<td>餐馆id</td>");
							var price=$("<td>菜价格</td>");
							var tr = $("<tr></tr>");
							tr.append(name);
							tr.append(num);
							tr.append(sort);
							tr.append(restId);
							tr.append(price);
							$("table").append(tr);
							for (var i=0; i<r.searchDishs.length; i++) {
								var tName = $("<td>"+r.searchDishs[i].dName+"</td>");
								var tNumber = $("<td>"+r.searchDishs[i].dNumber+"</td>");
								var tSname = $("<td>"+r.searchDishs[i].sName+"</td>");
								var tRid = $("<td>"+r.searchDishs[i].rid+"</td>");
								var tRprice = $("<td>"+r.searchDishs[i].rPrice+"</td>");
								var tr = $("<tr></tr>");
								var div = $("<div id='qingcai'></div>");
								tr.append(tName);
								tr.append(tNumber);
								tr.append(tSname);
								tr.append(tRid);
								tr.append(tRprice);
								$("table").append(tr);
							}
						}
					});
				});
			});
		</script>
	<body>
		<select id="sel" style="margin-right: 10%"></select>
		<input type="text" style="width: 200px; display: inline;" class="form-control" placeholder="请输入搜索的菜名" />
		<button id="search" class="btn btn-default">搜索</button>
		<table class="table table-bordered" style="margin-top: 20px" id="table"></table>
	</body>
</html>
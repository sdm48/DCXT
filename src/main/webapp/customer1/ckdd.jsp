<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" href="js/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="themes/icon.css">
		<link rel="stylesheet" type="text/css" href="themes/color.css">
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
	</head>
	<script type="text/javascript">
		$(function(){
			$("#inputJudge").hide();
			$("#sureJudge").hide();
			$("#selectJudgeNum").hide();
			$("#span").hide();
			$.ajax({
				url : 'showOrder',
				data : {
				},
				dataType : 'json',
				type : 'post',
				success : function(r){
					for (var i=0; i<r.length; i++) {
						if(r[i].judge != null) {
							var checkbox = $("<td></td>")
						} else {
							var checkbox = $("<td><input id='"+r[i].oid+"' type='checkbox' /></td>");
						}
						var oid = $("<td>").append(r[i].oid);//订单号
						var rName = $("<td>").append(r[i].rName);
						var number = $("<td>").append(r[i].onumber);
						var address = $("<td>").append(r[i].uaddress);
						var judgeText = $("<td>").append(r[i].judge);
						var orderDate = $("<td>").append(r[i].odate);
						var judgeDate = $("<td>").append(r[i].judgedate);
						//var judgeNum = $("<td>").append(r[i].judgenum);
						if(r[i].judgenum==2) {
							var judgeNumText = $("<td>").append('好评');
						} else if(r[i].judgenum==1) {
							var judgeNumText = $("<td>").append('差评');
						} else {
							var judgeNumText = $("<td>").append('');
						}
						var tr = $("<tr></tr>");
						tr.append(checkbox);
						tr.append(oid);
						tr.append(rName);
						tr.append(number);
						tr.append(address);
						tr.append(judgeText);
						tr.append(orderDate);
						tr.append(judgeDate);
						tr.append(judgeNumText);
						$("#table").append(tr);
					}
					$("#judgeButton").click(function(){
						var selectNum = 0;
						var checkboxs = $("input[type='checkbox']");
						for (var i=0; i<checkboxs.length; i++) {
							if(checkboxs.get(i).checked == true) {
								selectNum++;
							}
						}
						if(selectNum != 1) {
							alert("一次只能选一个评价");
						} else {
							var temp = 0;
							for (var i=0; i<checkboxs.length; i++) {
								if(checkboxs[i].checked == true) {
									temp = i;
									$('#win').window({    
									    width:300,    
									    height:200,    
									    modal:true   
									});
									$("#inputJudge").show();
									$("#sureJudge").show();
									$("#selectJudgeNum").show();
									$("#selectJudgeNum option[value='2']").attr('selected', 'selected');
									$("#span").show();
									/* $("#selectJudgeNum").change(function(){
										console.log($("#selectJudgeNum").val());
									}) */
									$("#sureJudge").click(function(){
										var temp2 = temp;
										$.ajax({
											url : 'showOrder',
											data : {
												selectNum : $("select").val(),
												oid : checkboxs[temp].id,
												judgeContent : $("#inputJudge").val()
											},
											dataType : 'json',
											type : 'post',
											success : function(r){
												console.log(r[0]);
												console.log(r[0].isJudge);
												if(r[0].isJudge==1) {
													$.messager.alert('确定','修改评价成功');
													$("input[type='checkbox']").attr('checked', false);
													$('#win').window('close');
												} else {
													$.messager.alert('确定','修改评价失败');
													$("input[type='checkbox']").attr('checked', false);
													$('#win').window('close');
												}
											}
										});
									});
								}
							}
						}
					});
				}
			});
		});
	</script>
	<body>
		<table class="table table-bordered" id="table" border="1">
			<tr>
				<td></td>
				<td>订单号</td>
				<td>餐馆名字</td>
				<td>订菜个数</td>
				<td>订单地址</td>
				<td>订单评价</td>
				<td>订单时间</td>
				<td>评价时间</td>
				<td>是否好评</td>
			</tr>
		</table>
		<button class="btn btn-default" id="judgeButton">确认订单</button>
		<div id="win">
			<textarea id="inputJudge" placeholder="评价内容" style="display:block; width: 100%;"></textarea>
			<select id='selectJudgeNum' style='margin-top: 10px; width: 20%;'>
				<option value="1">1</option>
				<option value="2">2</option>
			</select>
			<span id='span' style='margin-top: 10px; margin-left: 10px'>请选择2为好评,1为差评</span>
			<br/>
			<button id="sureJudge" class="btn btn-default" style="margin-top: 10px;">确定评价</button>
		</div>  
	</body>
</html>
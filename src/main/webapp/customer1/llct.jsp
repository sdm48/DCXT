<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<title>浏览餐厅</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="themes/color.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
$(function(){
	$('#dg').datagrid({    
	    url:'llctxxly',   
	    columns:[[ 
	        {field:'rid',title:'餐厅id',width:100},    
	        {field:'rname',title:'餐厅名字',width:100},    
	        {field:'rphone',title:'餐厅电话',width:100},
	        {field:'raddress',title:'餐厅地址',width:100},
	        {field:'workData',title:'开店时间',width:100},
	        {field:'isopen',title:'是否营业',width:100},
	        {field:'star',title:'星级',width:100} 
	    ]],
	    pagination:true,
	    singleSelect:true,
	    rownumbers:true,
	    checkOnSelect:true,
		pageSize:5,
		pageList:[5,10,15,20],
	onClickRow: function (index, row) {  //easyui封装好的时间（被单机行的索引，被单击行的值）
        //需要传递的值
        var rid = row["rid"];
		var rname=row["rname"];
        var url = "cgxxly?rid=" + rid+"&rname="+rname   
        //通过Ajax传值
        $.ajax({
            url: url,
            type: 'post',
            timeout: 100,

            Error: function () {
                alert(Error);
            },
            success: function () {
                window.location.href = url
            }
        });    
    }
	
	})
	
})
</script>
<body>
<table id="dg"></table> 
</body>

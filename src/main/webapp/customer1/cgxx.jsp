<%@ page language="java" contentType="text/html; charset=Utf-8"
    pageEncoding="Utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<title>${sessionScope.rname}店面</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="themes/color.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
</head>
</head>
<script type="text/javascript">
$(function(){
	$('#dg').datagrid({    
	    url:'Cgcply',   
	    columns:[[ 
	        {field:'ck',title:'选择菜品 ',checkbox:true,width:100},
	        {field:'did', title:'菜品唯一id',width:100,hidden:true},    
	        {field:'dname',title:'菜名',width:100},   
	        {field:'rprice',title:'菜价格',width:100},   
	        {field:'sname',title:'菜类',width:100}, 
	        {field:'dnumber',title:'剩余份数',width:100}
	    ]],
	    pagination:true,
	    singleSelect:true ,
	    checkOnSelect:true,
		pageSize:5,
		pageList:[5,10,15,20],//列出店家菜品进行分页 ;
		 toolbar: [{
			 iconCls: 'icon-edit',
				text:'加入购物车',
				handler: function(){
					  var sdata = $('#dg').datagrid('getSelections');
					  if(sdata.length!=0){
						var  s = sdata[0];
						 var rowIndex = $('#dg').datagrid('getRowIndex', s);
						 $("#rowIndex").textbox('setText',rowIndex);
						 $('#win').window('open'); 
						 $('#ff').form('load',s);
						 document.form.value.clear();
					  }else{
						   alert("请勾选菜品后再加哟")
						   
					  }
					}
		 
			},
		 ],
		})
		$('#win').window({    
		    width:600,    
		    height:400,    
		    modal:true   
		});  
		$('#win').window('close');  
		 $('#ff').form({    
			    url:'jrgwcly',    
			    success:function(data){ 
			    	$('#win').window('close'); 
			    	var  m=eval("("+data+")");
			        if(m.msg){
			        	$('#win').window('close'); 
			        	  $('#nub').datagrid('clearChecked');
			        	var did = $("#did").textbox('getText');
			        	var rowIndex = $("#rowIndex").textbox('getText');
			        	var lineNum = $("tr[datagrid-row-index="+rowIndex+"] .datagrid-cell-rownumber").text();
			        	$('#dg').datagrid('updateRow',{
			        		index:rowIndex,
			        		row: {
			        			sid:1,
			        			dname: dname,
			        			sage: '17',
			        			snum:'110'
			        		}
			        	});
			        	$("tr[datagrid-row-index="+rowIndex+"] .datagrid-cell-rownumber").text(lineNum);
			        	
			        	
			        } else{
			        	
			        } 
			    }    
			}); 
})

	</script>
<body>
${sessionScope.rname}店面
<table id="dg">
</table>
<div id="win">
				<form id="ff" method="post">
					<div  id="sid">
					菜品编码 <input class="easyui-textbox" name="did" />
					菜名 <input class="easyui-textbox" name="dname" />
					菜价<input class="easyui-textbox" name="rprice"/>
					需要份数<input class="easyui-textbox" name="nub"/> 
					</div>
					<br>
					剩余份数<input class="easyui-textbox" id="dnumber" name="dnumber" data-options="iconCls:'icon-man'" /> 
					<button class="easyui-linkbutton">提交</button>
				</form>  
			</div> 
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="themes/icon.css">
		<link rel="stylesheet" type="text/css" href="themes/color.css">
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
	</head>
		<script type="text/javascript">
			$(function(){
				$("#selectTable").hide();
				$("#sureOrder").hide();
				$('#table').datagrid({    
				    url:'selectVegetable',
				    fitColumns : true,
				    pagination : true,
				    pageSize : 5,
				    pageList : [5, 10, 15],
				    columns:[[    
				        {field:'did', checkbox:true, width:100},    
				        {field:'dName', title:'菜名', width:100},    
				        {field:'dNumber', title:'菜数量', width:100},    
				        {field:'sName', title:'菜种类', width:100},    
				        {field:'rid', title:'餐馆id', width:100},    
				        {field:'rPrice', title:'菜价格', width:100},    
				        {field:'isOPen', title:'是否能被选中', width:100}
				    ]]
				});
				$("#button").click(function(){
					var selectNum = 0;
					//获得全部复选框
					var checkboxs = $("input[type='checkbox']");
					for(var i=0; i<checkboxs.length; ++i) {
						//获得被选中的复选框
						if(checkboxs.get(i).checked == true) {
							selectNum++;
						}
					}
					if(selectNum != 1) {
						$.messager.alert('警告','一次只能选择一个菜'); 
						$("input[type='checkbox']").attr('checked', false);
					} else {
						for(var i=0; i<checkboxs.length; ++i) {
							//获得被选中的复选框
							if(checkboxs.get(i).checked == true) {
								$.ajax({
									url : 'selectVegetable',
									data : {
										did : checkboxs.get(i).value
									},
									dataType : 'json',
									type : 'post',
									success : function(r){
										$('#win').window({    
										    width : 400,    
										    height : 300,    
										    modal : true
										});
										$('#selectTable').ready(function(){
											$("#selectTable").show();
											$("#sureOrder").show();
											$("#newTr").remove();
											var tname = $("<td>").append(r.dName);
											var taddress = $("<td>").append(r.customer.caddress);
											var tsort = $("<td>").append(r.sName);
											var tdNumber = $("<td>").append(r.dNumber);
											var tcheckbox = $("<td><input style='width:50px' type='text' id='dishNum' value=1 /></td>")
											var tr = $("<tr id='newTr'></tr>");
											tr.append(tname);
											tr.append(taddress);
											tr.append(tsort);
											tr.append(tdNumber);
											tr.append(tcheckbox);
											$("#selectTable").append(tr);
										});
										$("#sureOrder").click(function(){
											$.ajax({
												url : 'selectVegetable',
												data : {
													restId : r.rid,
													dishNum : $("#dishNum").val(),
													dishId : r.did,
													rPrice : r.rPrice*$("#dishNum").val(),
												},
												dataType : 'json',
												type : 'post',
												success : function(x){
													if(x.isOrder == 1) {
														alert("下单成功");
														//$.messager.alert('确定','下单成功，等待商家是否接单');
														$("input[type='checkbox']").attr('checked', false);
														$('#win').window('close');
														window.location.reload();
													}else{
														$.messager.alert('警告','下单失败');
														$("input[type='checkbox']").attr('checked', false);
														$('#win').window('close');
													}
												}
											});
										})
									}
								});
							}
						}
					}
				});
			});
		</script>
	<body>
		<table id="table" class="easyui-datagrid"></table>
		<button id="button">选择</button>
		<div id="win">
			<table id="selectTable" border="1">
				<tr>
					<td>菜名</td>
					<td>地址</td>
					<td>菜种类</td>
					<td>菜剩余数量</td>
					<td>订菜个数</td>
				</tr>
			</table>
			<button id="sureOrder">确定</button>
		</div>
	</body>
</html>
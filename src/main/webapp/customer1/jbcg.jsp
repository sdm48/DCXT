<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="js/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="themes/icon.css">
		<link rel="stylesheet" type="text/css" href="themes/color.css">
		<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
	</head>
		<script type="text/javascript">
			$(function(){
				$("#tipText").hide();
				$("#sureTip").hide();
				$("#tipTable").hide();
				$.ajax({
					url : 'tipRestaurant',
					data : {
					},
					dataType : 'json',
					type : 'post',
					success : function(r){
						for(var i=0; i<r.length; ++i) {
							var checkbox = $("<td><input type='checkbox' id='"+r[i].oid+"' /></td>")
							var restName = $("<td>").append(r[i].restName);
							var oid = $("<td>").append(r[i].oid);
							var dishName = $("<td>").append(r[i].dishName);
							var totalPrice = $("<td>").append(r[i].totalPrice);
							var dishNum = $("<td>").append(r[i].dishNum);
							var star = $("<td>").append(r[i].star);
							var tr = $("<tr></tr>");
							tr.append(checkbox);
							tr.append(restName);
							tr.append(oid);
							tr.append(dishName);
							tr.append(totalPrice);
							tr.append(dishNum);
							tr.append(star);
							$("#table").append(tr);
						}
					}
				});
				$("#tip").click(function(){
					var checkNum  = 0;
					for (var i = 0; i < $("input[type='checkbox']").length; i++) {
						if($("input[type='checkbox']")[i].checked == true) {
							checkNum++;
						}
					}
					if(checkNum != 1) {
						$.messager.alert('警告','一次只能选择一个进行举报!');
						$("input[type='checkbox']").attr('checked', false);
					} else {
						var temp = 0;
						for (var i = 0; i < $("input[type='checkbox']").length; i++) {
							if($("input[type='checkbox']")[i].checked == true) {
								temp = i;
								$.ajax({
									url : 'tipRestaurant',
									data : {
										selectId : $("input[type='checkbox']")[i].id
									},
									dataType : 'json',
									type : 'post',
									success : function(r){
										$('#win').window({    
										    width:400,    
										    height:300,    
										    modal:true
										});
										$("#tipTable").ready(function(){
											$("#tipText").show();
											$("#tipTable").show();
											$("#sureTip").show();
											$("#newTr").remove();
											var name = $("<td>").append(r[temp].restName);
											var phone = $("<td>").append(r[temp].dishName);
											var address = $("<td>").append(r[temp].totalPrice);
											var uname = $("<td>").append(r[temp].dishNum);
											var star = $("<td>").append(r[temp].star);
											var tr = $("<tr>").attr('id', 'newTr');
											tr.append(name);
											tr.append(phone);
											tr.append(address);
											tr.append(uname);
											tr.append(star);
											$("#tipTable").append(tr);
											$("#newTr").remove();
										});
										$("#sureTip").click(function(){
											$.ajax({
												url : 'tipRestaurant',
												data : {
													restId : r[0].rid,
													oid :  $("input[type='checkbox']")[temp].id,
													tipText : $("#tipText").val() //把举报内容传到后台.
												},
												dataType : 'json',
												type : 'post',
												success : function(r){
													if(r[0].isTip == 1) {
														alert("举报成功");
														//$.messager.alert('确定','举报成功');
														$('#win').window('close');
														$("#tipText").val('');
														$("input[type='checkbox']").attr('checked', false);
														window.location.reload();
													} else {
														$.messager.alert('警告','举报失败');
														$('#win').window('close');
														$("#tipText").val('');
														$("input[type='checkbox']").attr('checked', false);
													}
												}
											});
										})
									}
								});
							}
						}
					}
				})
			});
		</script>
	<body>
		<table id="table" class="table table-bordered">
			<tr>
				<td></td>
				<td>餐馆名字</td>
				<td>订单id</td>
				<td>菜名</td>
				<td>菜总价</td>
				<td>菜个数</td>
				<td>餐馆星级</td>
			</tr>
		</table>
		<div id="win">
			<table id="tipTable" border="1">
				<tr>
					<td>餐馆名字</td>
					<td>菜名</td>
					<td>菜总价</td>
					<td>订菜数量</td>
					<td>餐馆星级</td>
				</tr>
			</table>
			<textarea id="tipText" placeholder="举报内容"></textarea>
			<br />
			<button id="sureTip" class="btn btn-default">确定</button>
		</div>
		<button id="tip" class="btn btn-default">举报</button>
	</body>
</html>
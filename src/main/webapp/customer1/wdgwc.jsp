<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的购物车</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="themes/color.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
$(function(){
	$('#dg').datagrid({    
	    url:'wdgwcly',    
	    columns:[[ 
	        {field:'ck',title:'确定要买 ',checkbox:true, width:100},
	        {field:'gid',title:'购物车唯一id',width:100},
	        {field:'dname',title:'菜名 ',width:100},    
	        {field:'rprice',title:'总价 ',width:100},
	        {field:'nbu',title:'菜的份数 ',width:100},
	    ]],
	    pagination:false,
	    striped:true,
	    singleSelect:true,
	    rownumbers:true,
	    checkOnSelect:true,
	    toolbar: [{
    	iconCls: 'icon-remove',
		text:'删除',
		handler: function(){
			var sdata = $('#dg').datagrid('getSelections');
			  if(sdata.length!=1){
				  $.messager.show({
						title:'我的消息',
						msg:'请一个一个删除。',
						timeout:100,
						showType:'slide'
					});
				  $('#dg').datagrid('clearChecked');
			  }else{
				  var s=sdata[0];
				var gid=s.gid
			
			       var url = "scgwc?gid=" + gid;
			        $.ajax({
			            url: url,
			            type: 'post',
			            timeout: 100,

			            Error: function () {
			                alert(Error);
			            },
			            success: function () {
			                window.location.href = url
			                alert("删除成功");
			            }
			        });
				   
			  }
			}
	},'-',{
    	iconCls: 'icon-add',
		text:'提交',
		handler: function(){
			var sdata = $('#dg').datagrid('getSelections');
			  if(sdata.length!=1){
				  $.messager.show({
						title:'我的消息',
						msg:'请勾选再提交 ',
						timeout:100,
						showType:'slide'
					});
				  $('#dg').datagrid('clearChecked');
			  }else{
				  var s=sdata[0];
				var gid=s.gid
				var nub=s.nbu
			       var url = "tjdd?gid=" + gid+"&nub="+nub;
			        $.ajax({
			            url: url,
			            type: 'post',
			            timeout: 100,
			            dataType : "text",
			            Error: function () {
			                alert(Error);
			            },
			            success: function (data) {
			                $('#dg').datagrid('load'); 
			            	 if(data==0){
			            		 alert("提交成功")
			            	
			            	 }else if(data==1){
			            		 alert("数量不够了哟 ")
			            	 }else{
			            		 alert("未知 ")
			            	 }
			            }
			           
			        });
			    
			  }
				
			}
	}
	    ],
		
	})
})

</script>
<body>
${sessionScope.uname}的购物车
<table id="dg">
</table>

</body>
</html>
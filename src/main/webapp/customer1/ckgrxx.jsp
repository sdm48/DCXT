<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查看信息页面</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="themes/color.css">
<script type="text/javascript"
	src="js/jquery.min.js"></script>
<script type="text/javascript"
	src="js/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
$(function(){
	$("#uid").hide();
	$('#dg').datagrid({    
	    url:'ckaction',    
	    columns:[[ 
	        {field:'uid',title:'顾客id ',width:100,hidden:true},    
	        {field:'cname',title:'用户名 ',width:100},    
	        {field:'csex',title:'用户性别 ',width:100},
	        {field:'cphone',title:'用户电话 ',width:100},
	        {field:'caddress',title:'用户地址 ',width:100} ,
	        {field:'oid',title:'订单号 ',width:100},
	    ]],
	    pagination:false,
	    striped:true,
	    loadMsg:'正在加载',
	    rownumbers:true,
	    toolbar: [{
		handler: function(){
		var sdata = $('#dg').datagrid('getSelections');
				
				}
		}],
	})
})

</script>
<body>
	
		
	<table id="dg"></table>  
 
</body>
</html>


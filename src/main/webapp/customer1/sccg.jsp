<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
    uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" href="js/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="themes/icon.css">
		<link rel="stylesheet" type="text/css" href="themes/color.css">
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="js/easyui-lang-zh_CN.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
	</head>
		<script type="text/javascript">
			$(function(){
				$.ajax({
					url : 'showRestaurant',
					data : {
						
					},
					dataType : 'json',
					type : 'post',
					success : function(r){
					}
				})
				$('#table').datagrid({
				    url:'showRestaurant',
				    fitColumns : true,
				    pagination : true,
				    pageSize : 5,
				    pageList : [5, 10, 15],
				    columns:[[
				        {field:'id', title:'', width:100},    
				        {field:'name', title:'餐馆名字', width:100},    
				        {field:'price', title:'餐馆电话', width:100},    
				        {field:'address', title:'餐馆地址', width:100},    
				        {field:'uid', title:'用户id', width:100},    
				        {field:'workdate', title:'工作时间', width:100},    
				        {field:'star', title:'星级', width:100},    
				        {field:'isopen', title:'是否开业', width:100}, 
				    ]]    
				});
				$("#button").click(function(){
					var selectNum = 0;
					var checkboxs = $("input[type='checkbox']");
					for(var i=0; i<checkboxs.length; ++i) {
						if(checkboxs.get(i).checked == false) {
							continue;
						} else {
							selectNum++;
						}
					}
					if(selectNum === 0) {
						alert(1)
					} else {
						for(var i=0; i<checkboxs.length; ++i) {
							if(checkboxs.get(i).checked == true) {
								var tempId = checkboxs.get(i).id;
								$.ajax({
									url : 'showRestaurant',
									data : {
										rid : tempId
									},
									dataType : 'json',
									type : 'post',
									success : function(r){
										if(r.isCollect == 1) {
											$.messager.alert('确定','收藏成功');
											$("input[type='checkbox']").attr('checked', false);
										} else {
											$.messager.alert('警告','收藏失败');
											$("input[type='checkbox']").attr('checked', false);
										}
									}
								});
							} else {
								continue;
							}
						}
					}
				});
			});
		</script>
	<body>
		<table id="table">
			<c:forEach items="${sessionScope.rests}" var="rest">
				<tr>
					<td><input id="${rest.rid}" type="checkbox"/></td>
					<td>${rest.rName}</td>
					<td>${rest.rPhone}</td>
					<td>${rest.rAddress}</td>
					<td>${rest.uid}</td>
					<td>${rest.workDate}</td>
					<td>${rest.star}</td>
					<td>${rest.isOpen}</td>
				</tr>
			</c:forEach>
		</table>
		<button id="button" class="btn btn-default">收藏</button>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>信息展示页面</title>
<link rel="stylesheet" type="text/css"
	href="themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="themes/color.css">
<script type="text/javascript"
	src="js/jquery.min.js"></script>
<script type="text/javascript"
	src="js/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="js/easyui-lang-zh_CN.js"></script>
</head>
<script type="text/javascript">
	$(function(){
		//树形菜单展示
		$('#tt').tree({    
		    url:'authAction',
		    closable:true,
    		selected:true,
		    queryParams:{
		    	pid:-1
		    },
		    onLoadSuccess:function(node,data){
		    	$("#tt").tree('expandAll')
		    },
		    onClick:function(node){
		    	var  flag = $("#tb").tabs('exists',node.text);
		    	if(node.text=="退出登录"){
		    		location.href = "${pageContext.request.contextPath}/index.jsp";
		    	}else{
			    	if(node.attributes.url){
			    		if(!flag){
				    		$("#tb").tabs('add',{				    						    
					    		title:node.text,
					    		content:"<iframe  width='100%' height='100%' frameborder='0' src='"+node.attributes.url+"'></iframe>",
					    		closable:true,
					    		selected:true,
					    	})	
				    	}else{
				    	$("#tb").tabs('select',node.text);		
				    	}
			    	}
		    	}
		    	
		    }
		   
		});  
		
	});

</script>
<body class="easyui-layout">
    <div data-options="region:'north',split:false" style="height:100px;"></div>   
    <div data-options="region:'west',title:'${sessionScope.uname},欢迎你',split:false,collapsible:false" style="width:200px;">
    	<ul id="tt"></ul> 
    </div>   
    <div data-options="region:'center',border:false" style="padding:5px;background:#eee;" >
    <!-- tabs框 -->
    <div id="tb" class="easyui-tabs" data-options="fit:true,border:false">   
	</div>
    </div>   
</body>
</html>